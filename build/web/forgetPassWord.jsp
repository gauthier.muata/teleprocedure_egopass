<%-- 
    Document   : forgetPassWord
    Created on : 1 oct. 2020, 15:57:51
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>E-Recettes:: DGRK</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicons -->

        <link href="asset/img/favicon.png" rel="icon">
        <link href="asset/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="asset/css/family.css" rel="stylesheet"/>
        <!--<link href="asset/css/family_ruda.css" rel="stylesheet"/> -->
        <!-- Bootstrap CSS File -->

        <link href="asset/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>

        <!-- Libraries CSS Files -->
        <link href="asset/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet"/>
        <link href="asset/lib/prettyphoto/css/prettyphoto.css" rel="stylesheet"/>
        <link href="asset/lib/hover/hoverex-all.css" rel="stylesheet">
        <link href="asset/lib/jetmenu/jetmenu.css" rel="stylesheet">
        <link href="asset/lib/owl-carousel/owl-carousel.css" rel="stylesheet">

        <!-- Bootstrap CSS File -->
        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>

        <!-- Main Stylesheet File -->
        <link href="asset/front/style.css" rel="stylesheet">
        <link href="asset/front/blue.css" rel="stylesheet">


    </head>
    <body>

        <%@include file="asset/include/header.html" %>

        <section class="post-wrapper-top">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <ul class="breadcrumb">
                        <li><a href="/teledeclaration">Accueil</a></li>
                        <li><a href="forget-password"></a>Mot de Passe oublié</li>
                    </ul>
                    <h2><i class="fa fa-key"></i>&nbsp;Mot de asse oublié !!</h2>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                    </div>
                    <!-- / end div .search-bar -->
                </div>
            </div>
        </section>

        <section class="section1">
            <div class="container clearfix">
                <div class="content col-lg-12 col-md-12 col-sm-12 clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <h4 class="title">
                            <span>Bienvenue !</span>
                        </h4>
                        <p>Vous avez oublié votre mot de passe ? <br>Entrez votre mail et un code vous sera envoyé pour réinitialiser votre mot de passe. </p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <h4 class="title">
                            <span>Réinitialisez votre mot de passe</span>
                        </h4>

                        <form class="resetPassword">
                            <div class="form-group divInputMail">
                                <label for="email">Votre adresse mail</label>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="email" class="form-control" name="inputEmail" placeholder="Entrez votre mail" id="inputEmail">
                                </div>
                                <span class="mail-error hidden">Veuillez saisir votre adresse mail s'il vous plait.</span>
                            </div>
                            <div class="form-group divBtnInit">
                                <button type="button" class="button" id="btnInitPassword">R&eacute;initialiser</button>
                                <span style="font-style: italic" id="img_loading" hidden>Traitement en cours...<img src="asset/img/loading.gif" width="42" alt=""/></span>
                            </div>
                            <div class="form-group alert alert-success alert-login divMessageText hidden">
                                Vous avez réussi un nouveau mail d'initialisation de votre mot de passe.
                            </div>
                        </form>

                        <form class="divResetnewPassword hidden">
                            <div class="form-group">
                                <label for="codeInit">Code d'initialisation</label>
                                <input type="text" class="form-control" name="codeInit" placeholder="Entrez le code d'initialisation" id="codeInit">
                            </div>
                            <div class="form-group">
                                <label for="password">Nouveau mot de Passe</label>
                                <input type="password" class="form-control" name="password" placeholder="Entrez votre nouveau mot de passe" id="newPasswordassword">
                            </div>
                            <div class="form-group">
                                <label for="confirmPassword">Confirmer le mot de Passe</label>
                                <input type="password" class="form-control" name="confirmPassword" placeholder="Confirmez votre nouveau mot de passe" id="confirmPassword">
                            </div>

                            <div class="form-group">
                                <button type="button" class="button" id="btnResetPassword">R&eacute;initialiser le mot de passe</button>
                            </div>

                            <div class="alert alert-danger error-alert" role="alert" style="display: none;">
                            </div>
                        </form>

                        <div class="alert alert-success alert-success-update hidden">
                            <p>Votre mot de passe a été modifié avec succès.</p>
                            <p>Vous pouvez maintenant vous connecter. <a href=""><b>Connectez-vous</b></a></p>
                        </div>
                    </div>
                    <!-- end login -->
                </div>
                <!-- end content -->
            </div>
            <!-- end container -->
        </section>

        <div class="modal fade" id="modalMsgErreur" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Message</h4>
                    </div>
                    <div class="modal-body">
                        <p>Votre connexion n'a pas abouti. Veuillez réessayer ou contacter l'administrateur.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fermer</button>
                    </div>
                </div>
            </div>
        </div>
        <%@include file="asset/include/footer.html" %>

        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "metal": "#c4c5d6",
                        "light": "#ffffff",
                        "accent": "#00c5dc",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995",
                        "focus": "#9816f4"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>

        <script src="asset/admin/js/plugins.bundle.js" type="text/javascript"></script>
        <script src="asset/admin/js/scripts.bundle.js" type="text/javascript"></script>
        <script src="asset/lib/jquery/jquery.min.js"></script>
        <script src="asset/lib/bootstrap/js/bootstrap.min.js"></script>

        <script src="asset/js/prettyphoto.js"></script>
        <script src="asset/js/isotope.min.js"></script>
        <script src="asset/js/hoverdir.js"></script>
        <script src="asset/js/hoverex.min.js"></script>
        <script src="asset/js/unveil-effects.js"></script>
        <script src="asset/js/owl-carousel.js"></script>
        <script src="asset/js/jetmenu.js"></script>
        <script src="asset/js/easypiechart.min.js"></script>

        <script src="asset/js/core.js" type="text/javascript"></script>

        <script src="asset/js/jquery.blockUI.js"></script>
        <script src="asset/alertify/alertify.min.js" type="text/javascript"></script>
        <script src="asset/js/utils.js" type="text/javascript"></script>

        <script src="asset/js/initParams.js" type="text/javascript"></script>

    </body>

</html>
