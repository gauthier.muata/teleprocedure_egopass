<%-- 
    Document   : impression_note
    Created on : 3 avr. 2021, 12:30:46
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Impression note</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--begin::Fonts -->
        <link href="asset/css/font.css" rel="stylesheet" type="text/css"/>
        <!--end::Fonts -->

        <!--begin::Global Theme Styles(used by all pages) -->
        <style>

            @page {
                size: A4;
                margin: 0;
            }

            body {
                width: 100%;
                height: 100%;
                margin: 0;
                padding: 0;
                background-color: #FAFAFA;
                font: 12pt "Tahoma";
            }
            * {
                box-sizing: border-box;
                -moz-box-sizing: border-box;
            }
            .page {
                width: 290mm;
                min-height: 350mm;
                padding: 20mm;
                margin: 10mm auto;
                border-radius: 5px;
                background: white;
                box-shadow: 0 0 5px rgba(0, 0, 0, 0.1);
            }
            .subpage {
                padding: 1cm;
                border: 4px #CCC solid;
                height: 310mm;
            }
            .button {
                background-color: #4CAF50; /* Green */
                border: none;
                color: white;
                padding: 16px 32px;
                text-align: center;
                text-decoration: none;
                display: inline-block;
                font-size: 16px;
                margin: 4px 2px;
                -webkit-transition-duration: 0.4s; /* Safari */
                transition-duration: 0.4s;
                cursor: pointer;
            }
            .button2 {
                background-color: white; 
                color: black; 
                border: 2px solid #008CBA;
            }

            .button2:hover {
                background-color: #008CBA;
                color: white;
            }

        </style>
    </head>

    <body>
        <br/><br/>
        <button id="btnPrint" style="margin-left:300px" class="button button2"><i class="fa fa-print"></i> &nbsp;&nbsp; Imprimer le document</button>
        <br/>
        <div class="page">
            <div class="subpage">
                <div id="document_container">

                </div>
            </div>
        </div>

        <!--end::Global Theme Bundle -->

        <script type="text/javascript" src="asset/lib/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="asset/alertify/alertify.min.js"></script>
        <script type="text/javascript" src="asset/lib/printThis.js"></script>
        <script type="text/javascript" src="asset/lib/html2pdf.bundle.min.js"></script>
        <script src="asset/js/core.js" type="text/javascript"></script>
        <script type="text/javascript" src="asset/js/utils.js"></script>
        <script type="text/javascript" src="asset/lib/document.js"></script>
        <script type="text/javascript">

            $(function () {

                $('#create_pdf').click(function (e) {
                    e.preventDefault();

                    var element = document.getElementById('document_container');

                    var opt = {
                        margin: 1,
                        filename: 'myfile.pdf',
                        image: {type: 'jpeg', quality: 0.98},
                        html2canvas: {scale: 2},
                        jsPDF: {unit: 'in', format: 'letter', orientation: 'portrait'}
                    };

                    var worker = html2pdf().from(element).set(opt).save();

                });

            });

        </script>
    </body> 
</html>