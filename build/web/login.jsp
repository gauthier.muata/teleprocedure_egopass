<%-- 
    Document   : login
    Created on : 04 aout 2021, 10:23:21
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Login</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Main Stylesheet File -->
        <link href="asset/front/style.css" rel="stylesheet" type="text/css"/>
        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>
        <link href="asset/css/classic.css" rel="stylesheet" type="text/css"/>

        <style>
            body {
                background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);
            }
            .dark_1{
                background-color: #2B2E31
            }
        </style>

    </head>
    <body>

        <%@include file="asset/include/header.html" %>

        <main class="main d-flex w-100">
            <div class="container d-flex flex-column">
                <div class="row h-10">
                    <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-100">
                        <div class="d-table-cell align-middle">

                            <div class="text-center mt-4">
                                <h1 class="h2">Bienvenue dans la T&eacute;l&eacute;-proc&eacute;dure de eGoPass</h1>
                                <hr>
                                <p class="lead">
                                    Connectez-vous
                                </p>
                            </div>

                            <div class="card" style="border-radius: 15px">
                                <div class="card-body">
                                    <div class="m-sm-2">
                                        <div class="text-center">
                                            <img src="asset/admin/img/avatar.jpg" alt="Avatar" class="img-fluid rounded-circle" width="72" height="72" />
                                        </div>
                                        <form>
                                            <div class="form-group">
                                                <label>Login ou email</label>
                                                <input class="form-control form-control-lg text-user-primary" type="text" name="email" placeholder="Entrez votre login ou votre email" />
                                                <span class="span_login alert alert-danger" hidden>Veuillez saisir votre login</span>
                                            </div>
                                            <div class="form-group">
                                                <label>Mot de passe</label>
                                                <input class="form-control form-control-lg text-user-pw-primary" type="password" name="password" placeholder="Entrez votre mot de passe" />
                                                <span class="span_password alert alert-danger" hidden>Veuillez saisir votre mot de passe</span>
                                                <div class="row text-muted">
                                                    <div class="col-6 text-left">
                                                        <small>
                                                            <a href=""><span style="color:#3f80a8" class="fa fa-question-circle fa-1x"></span>&nbsp;Mot de passe oublié ?</a>
                                                        </small>
                                                    </div>
                                                    <div class="col-6 text-right">
                                                        <small>
                                                            <span>Pas de compte ? <a href="register" style="color: red">Inscrivez-vous</a></span>
                                                        </small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="text-center mt-2">
                                                <button type="button" class="btn btn-lg btn-primary">Connexion</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>

        <footer class="footer">
            <div class="container-fluid">
                <div class="row text-muted">
                    <div class="col-6 text-left">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a class="text-muted" href="#" style="font-weight: bolder">Support</a>
                            </li>
                            <li class="list-inline-item">
                                <a class="text-muted" href="#">Help Center</a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-6 text-right">
                        <p class="mb-0">
                            &copy; 2021 - <a href="index.html" class="text-muted">Hologram</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>

    </body>

    <script src="asset/admin/js/app.js" type="text/javascript"></script>
    <script src="asset/lib/jquery/jquery.min.js"></script>
    <script src="asset/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="asset/alertify/alertify.min.js" type="text/javascript"></script>
    <script src="asset/js/jquery.blockUI.js"></script>
    <script src="asset/js/utils.js"></script>
    <script src="asset/js/core.js"></script>
    <script src="asset/js/login.js"></script>

</html>
