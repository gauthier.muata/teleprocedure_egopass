<%-- 
    Document   : modifier
    Created on : 22 oct. 2020, 09:34:34
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <title>Modifier mot de passe</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!--begin::Fonts -->
        <link href="asset/css/font.css" rel="stylesheet" type="text/css"/>
        <!--end::Fonts -->

        <!--begin::Page Vendors Styles(used by this page) -->
        <link href="asset/admin/css/fullcalendar.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Page Vendors Styles -->

        <!--begin::Global Theme Styles(used by all pages) -->
        <link href="asset/admin/css/plugins.bundle.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/style.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->

        <link href="asset/admin/css/light.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/light-menu.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy-aside.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/custom.css" rel="stylesheet" type="text/css">
        <!--end::Layout Skins -->

        <!-- Bootstrap CSS File -->
        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            /* Chart.js */
            /*
    * DOM element rendering detection
    * https://davidwalsh.name/detect-node-insertion
    */

            @keyframes chartjs-render-animation {
                from {
                    opacity: 0.99;
                }
                to {
                    opacity: 1;
                }
            }

            .chartjs-render-monitor {
                animation: chartjs-render-animation 0.001s;
            }
            /*
    * DOM element resizing detection
    * https://github.com/marcj/css-element-queries
    */

            .chartjs-size-monitor,
            .chartjs-size-monitor-expand,
            .chartjs-size-monitor-shrink {
                position: absolute;
                direction: ltr;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                overflow: hidden;
                pointer-events: none;
                visibility: hidden;
                z-index: -1;
            }

            .chartjs-size-monitor-expand > div {
                position: absolute;
                width: 1000000px;
                height: 1000000px;
                left: 0;
                top: 0;
            }

            .chartjs-size-monitor-shrink > div {
                position: absolute;
                width: 200%;
                height: 200%;
                left: 0;
                top: 0;
            }
        </style>
    </head>
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right 
          kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled 
          kt-subheader--transparent kt-aside--enabled kt-aside--fixed">

        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                <h4 class="text-light">Hologram</h4> 
            </div>
            <div class="kt-header-mobile__toolbar">

                <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
            </div>
        </div>


        <div class="kt-grid kt-grid--hor kt-grid--root">

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>

                <%@include file="asset/include/menu.html" %>

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " style="
                         justify-content: flex-end;
                         ">

                        <!-- begin:: Header Menu -->
                        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

                        <div class="kt-header__topbar">

                            <div class="kt-header__topbar-item kt-header__topbar-item--user">

                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">

                                    <div class="kt-header__topbar-user kt-rounded-">

                                        <span class="kt-header__topbar-welcome kt-hidden-mobile">Salut,</span>
                                        <span class="kt-header__topbar-username kt-hidden-mobile" id="userId"></span>
                                        <img alt="Pic" src="asset/admin/img/avatar.jpg" class="kt-rounded-">
                                        <span class="kt-badge kt-badge--username kt-badge--lg kt-badge--brand kt-hidden kt-badge--bold">S</span>
                                    </div>
                                </div>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-sm">
                                    <div class="kt-user-card kt-margin-b-40 kt-margin-b-30-tablet-and-mobile" style="background-image: url(/admin/img/head_bg_sm.jpg)">
                                        <div class="kt-user-card__wrapper">
                                            <div class="kt-user-card__pic">

                                                <img alt="Pic" src="asset/admin/img/avatar.jpg" class="kt-rounded-">
                                            </div>
                                            <div class="kt-user-card__details">
                                                <div class="kt-user-card__name" style="font-size: 1rem;"></div>
                                                <span class="email-user hidden"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <ul class="kt-nav kt-margin-b-10">
                                        <li class="kt-nav__item">
                                            <a href="profile" class="kt-nav__link">
                                                <span class="kt-nav__link-icon"><i class="flaticon2-calendar-3"></i></span>
                                                <span class="kt-nav__link-text">Mon profil</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__separator kt-nav__separator--fit"></li>

                                        <li class="kt-nav__custom kt-space-between">
                                            <a href="login" class="btn btn-label-brand btn-upper btn-sm btn-bold">Se déconnecter</a>
                                            <i class="flaticon2-information kt-label-font-color-2" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more..."></i>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        </div>
                        <!-- end:: Header Topbar -->
                    </div>

                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid ">
                            </div>
                        </div>


                        <!-- Begin Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                            <div class="kt-grid kt-grid--desktop kt-grid--ver kt-grid--ver-desktop kt-app">
                                <!--Begin:: App Aside Mobile Toggle-->
                                <button class="kt-app__aside-close" id="kt_profile_aside_close">
                                    <i class="la la-close"></i>
                                </button>
                                <!--End:: App Aside Mobile Toggle-->

                                <!--Begin:: App Aside-->
                                <div class="kt-grid__item kt-app__toggle kt-app__aside kt-app__aside--sm kt-app__aside--fit" id="kt_profile_aside">
                                    <!--Begin:: Portlet-->
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__body">
                                            <div class="kt-widget kt-widget--general-1">
                                                <div class="kt-media kt-media--brand kt-media--md kt-media--circle">
                                                    <img src="asset/admin/img/avatar.jpg" alt="image">
                                                </div>
                                                <div class="kt-widget__wrapper">
                                                    <div class="kt-widget__label">
                                                        <a href="#" class="kt-widget__title" id="usernameprofile"></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="kt-portlet__separator"></div>

                                        <div class="kt-portlet__body">
                                            <ul class="kt-nav kt-nav--bolder kt-nav--fit-ver kt-nav--v4" role="tablist">
                                                <li class="kt-nav__item">
                                                    <a class="kt-nav__link" href="profile" role="tab">
                                                        <span class="kt-nav__link-icon"><i class="flaticon2-user"></i></span>
                                                        <span class="kt-nav__link-text">Informations personnelles</span>
                                                    </a>
                                                </li>
                                                <li class="kt-nav__item active">
                                                    <a href="update-password" class="kt-nav__link">
                                                        <span class="kt-nav__link-icon"><i class="flaticon-lock"></i></span>
                                                        <span class="kt-nav__link-text">Modifier mot de passe</span>
                                                    </a>
                                                </li> 
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <!--End:: App Aside-->

                                <div class="kt-grid__item kt-grid__item--fluid kt-app__content">
                                    <div class="kt-portlet">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title"><i class="flaticon2-checking"></i>&nbsp;&nbsp;Changer votre mot de passe</h3>
                                            </div>
                                        </div>
                                        <div class="form-group form-group-last">
                                            <div class="alert alert-secondary">
                                                <div class="alert-icon"><i class="flaticon-lock kt-font-brand"></i></div>
                                                <div class="alert-text">
                                                    Vous pouvez modifier votre mot de passe ici.
                                                </div>
                                            </div>
                                        </div>
                                        <form class="kt-form kt-form--label-right" id="kt_profile_form">
                                            <div class="kt-portlet__body">
                                                <div class="kt-section kt-section--first">
                                                    <div class="kt-section__body">

                                                        <div class="form-group row">
                                                            <label class="col-xl-4 col-lg-4 col-form-label">Ancien mot de passe</label>
                                                            <div class="col-lg-7 col-xl-7">
                                                                <input type="password" id="lastPassword" class="form-control input-lg">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-4 col-lg-4 col-form-label">Nauveau mot de passe</label>
                                                            <div class="col-lg-7 col-xl-7">
                                                                <input type="password" id="newPassword" class="form-control input-lg">
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                            <label class="col-xl-4 col-lg-4 col-form-label">Confirmer le nouveau mot de passe</label>
                                                            <div class="col-lg-7 col-xl-7">
                                                                <input type="password" id="newPasswordConfirm" class="form-control input-lg">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                            <div class="kt-portlet__foot">
                                                <div class="kt-form__actions">
                                                    <div class="row">
                                                        <div class="col-lg-3 col-xl-3">
                                                        </div>
                                                        <div class="col-lg-9 col-xl-9">
                                                            <button type="button" class="btn btn-primary" id="btnUpdatePassword"><i class="flaticon2-writing"></i> Modifier</button>&nbsp;
                                                            <button type="reset" class="btn btn-secondary">Annuler</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <!--End:: App Content-->
                            </div>
                        </div>
                        <!-- End Content -->
                    </div>

                </div>
            </div>
        </div>

        <%@include file="asset/include/footerV2.html" %>

        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "metal": "#c4c5d6",
                        "light": "#ffffff",
                        "accent": "#00c5dc",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995",
                        "focus": "#9816f4"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>

        <script src="asset/admin/js/plugins.bundle.js" type="text/javascript"></script>
        <script src="asset/admin/js/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Global Theme Bundle -->

        <!--begin::Page Vendors(used by this page) -->
        <script src="asset/admin/js/fullcalendar.bundle.js" type="text/javascript"></script>

        <script src="asset/plugins/fancybox/js/jquery.fancybox.min.js" type="text/javascript"></script>

        <script src="asset/js/core.js" type="text/javascript"></script>
        <script src="asset/js/jquery.blockUI.js"></script>
        <script src="asset/alertify/alertify.min.js" type="text/javascript"></script>

        <script src="asset/js/utils.js" type="text/javascript"></script>     
        <script src="asset/admin/js/profile.js" type="text/javascript"></script>
    </body>
</html>
