var input_nom = $('#input_nom'),
        assujetti_set = $('.assujetti-set'),
        alert_demande = $('.alert-demande'),
        div_mail = $('.div_mail'),
        div_phone = $('.div_phone'),
        div_nom = $('.div_nom'),
        btn_search = $('.btn_search'),
        btn_demande_validation = $('.btn_demande_validation'),
        input_mail = $('#input_mail'),
        input_phone_number = $('#input_phone_number'),
        btnRecherche = $('#btnRecherche'),
        btnDemandeValidation = $('#btnDemandeValidation');

var codePersonne;

$(function () {

    btnRecherche.on('click', function () {
        if (input_nom.val() === '') {
            alertify.alert('Veuillez fournir votre nom ou raison sociale');
            return;
        }
        searchAssujetti(input_nom.val());
    });

    btnDemandeValidation.on('click', function () {
        if (input_mail.val() === '') {
            alertify.alert('Veuillez fournir votre adresse mail');
            return;
        }
        if (input_phone_number.val() === '') {
            alertify.alert('Veuillez fournir votre numéro de téléphone');
            return;
        }

        sendDemande();
    });

});

function searchAssujetti(nom) {

    $.ajax({
        url: baseUrl + 'v1/getAssujettiByName/' + nom + '',
        crossDomain: true,
        method: 'GET',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        if (data.code == '300') {
            showResponseError();
            return;
        } else if (data.code == '200') {
            alert_demande.html('Aucune donn&eacute;e n\'a &eacute;t&eacute; trouv&eacute;e pour cette recherche');
            alert_demande.removeClass('hidden');
            return;
        }
        setAssujettiData(data);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
    });
}

function sendDemande() {

    if (!validateEmail(input_mail.val())) {
        alertify.alert('L\adresse mail renseignée n\'est pas valide. Veuillez la ressaisir.');
        return;
    }

    var objet = {
        'code': codePersonne,
        'mail': input_mail.val(),
        'phone': input_phone_number.val()
    };

    $.ajax({
        url: baseUrl + 'v1/setDemandeInscription',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        headers: headers,
        data: JSON.stringify(objet),
        dataType: "JSON"
    }).done(function (data) {

        if (data == '-1') {
            showResponseError();
            return;
        } else if (data == '0') {
            alert_demande.html('Votre demande d\'inscription n\'a pas abouti. Veuillez r&eacute;essayer s\il vous plait.');
            alert_demande.removeClass('hidden');
            return;
        } else if (data == '2') {
            alertify.alert('L\adresse mail renseignée existe déjà pour un autre assujetti.');
            return;
        } else if (data == '4') {
            alertify.alert('Le numéro de téléphone renseigné existe déjà pour un autre assujetti.');
            return;
        } else if (data == '3') {
            alertify.alert('L\'adresse mail et le numéro de téléphone renseignés existent déjà pour un autre assujetti.');
            return;
        }

        alert_demande.html('Votre demande a &eacute;t&eacute; effectu&eacute;e avec succ&egrave;s. Vous allez &ecirc;tre notifi&eacute; une fois que celle-ci trait&eacute;e.');
        alert_demande.removeClass('hidden');
        alert_demande.attr('class', 'alert alert-info');
        btn_demande_validation.addClass('hidden');
        div_mail.addClass('hidden');
        div_phone.addClass('hidden');
        div_nom.addClass('hidden');

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
    });
}

function setAssujettiData(data) {

    var option = '', mail = '', name = '';
    option += '<div class="list-group-item active">';
    option += 'Liste des assujettis';
    option += '</div>';
    $.each(data, function (index, item) {
        mail !== '' ? mail = ' (' + item.mail + ')' : mail = '';
        name = item.prenom + ' ' + item.nom + ' ' + item.postnom;
        option += '<div class="list-group-item">' + name + mail +
                '&nbsp;&nbsp;<i class="alert-info glyphicon glyphicon-check" style="cursor: pointer" ' +
                'onclick="setDemandeValidation(\'' + item.code + '\',\'' + item.etat + '\',\'' + item.mail + '\',\'' +
                item.phone + '\',\'' + item.ntd + '\',\'' + name + '\')" title="S&eacute;lectionner"></i></div>';
    });
    if (data.length > 1) {
        assujetti_set.attr('style', 'height: 150px;overflow-y: scroll;');
    } else {
        assujetti_set.removeAttr('style');
    }
    assujetti_set.html(option);

}

function setDemandeValidation(code, etat, mail, phone, ntd, name) {

    if (etat == '1' || etat == '4') {
        setNTD(ntd);
        window.location = 'login';
    } else if (etat == '3') {
        alert_demande.html('Votre compte en t&eacute;l&eacute;-d&eacute;claration est en attente de validation et vous serez notifié une fois que cela est traité.');
        alert_demande.removeClass('hidden');
        assujetti_set.addClass('hidden');
        div_nom.addClass('hidden');
        btn_search.addClass('hidden');
        return;
    }

    codePersonne = code;

    div_mail.removeClass('hidden');
    div_phone.removeClass('hidden');
    alert_demande.removeClass('hidden');
    assujetti_set.addClass('hidden');
    btn_demande_validation.removeClass('hidden');
    btn_search.addClass('hidden');

    if (mail === '' && phone === '') {
        alert_demande.html('Veuillez entrer l\'adresse mail et le num&eacute;ro de t&eacute;l&eacute;phone pour ' + name);
    } else if (mail !== '' && phone === '') {
        alert_demande.html('Veuillez confirmer l\'adresse mail et entrer le num&eacute;ro de t&eacute;l&eacute;phone pour ' + name);
        input_mail.val(mail);
    } else if (mail === '' && phone !== '') {
        alert_demande.html('Veuillez entrer l\'adresse mail et confirmer le num&eacute;ro de t&eacute;l&eacute;phone pour ' + name);
        input_phone_number.val(phone);
    } else if (mail !== '' && phone !== '') {
        alert_demande.html('Veuillez confirmer l\'adresse mail et le num&eacute;ro de t&eacute;l&eacute;phone pour ' + name);
        input_mail.val(mail);
        input_phone_number.val(phone);
    }

}