/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var amount = 0,
        devise = '',
        reference = '',
        token = '',
        url;
var input_numeroCompte = $('#input_numeroCompte'),
        inputPin = $('#inputPin'),
        delCompte = $('#delCompte'),
        btnPayerMpata = $('#btnPayerMpata');

$(function () {
    
    checkIfConnexionOn();
    setMenu();

    amount = atob(getUrlParameter('id')).split('@')[0];
    devise = atob(getUrlParameter('id')).split('@')[1];
    token = atob(getUrlParameter('id')).split('@')[2];

    $('#input_montant').val(amount + ' ' + devise);
    $('#reference').html(atob(getUrlParameter('id')).split('@')[3]);

    btnPayerMpata.on('click', function () {
        setMpataAuthentification();
    });
    delCompte.on('click', function () {
        input_numeroCompte.val('');
    });
});

function setMpataAuthentification() {

    if (input_numeroCompte.val() === '') {
        Swal.fire({
            title: "Authentification",
            html: 'Veuillez saisir le num&eacute;ro de t&eacute;l&eacute;phone de votre compte s\'il vous plait',
            type: "warning",
            confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
        });
        return;
    }
    if (inputPin.val() === '') {
        Swal.fire({
            title: "Authentification",
            html: 'Veuillez saisir le code PIN de votre compte s\'il vous plait',
            type: "warning",
            confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
        });
        return;
    }

    $('#loading_img').show();

    var object = {
        "numero": input_numeroCompte.val(),
        "pin": inputPin.val()
    };

    $.ajax({
        url: baseUrl + 'v1/authenticationMpata',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {

        $('#loading_img').hide();

        if (data == '-1') {
            showErrorMessageOnRequest();
            return;
        }
        if (data == '0') {
            var message = 'Impossible de faire cette transaction.\nV\351rifiez les donn\351es de votre compte et r\351essayer';
            Swal.fire({
                title: "Paiement eMpata",
                html: message,
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            return;
        } else {
            Swal.fire({
                title: "Paiement eMpata",
                text: "Vous \352tes sur le point de payer pour cette commande. Etes-vous s\373r de faire cette transaction ?",
                type: "info",
                showCancelButton: !0,
                confirmButtonText: "Confirmer",
                cancelButtonText: "Annuler"
            }).then(function (isConfirm) {
                if (isConfirm.value) {
                    setMpataTransaction(data.token, token);
                }
            });
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
        KTApp.unblockPage();
    });
}

function setMpataTransaction(token, referenceToken) {

    devise === 'USD' ? montant = 1 : montant = 100;

    var object = {
        'adminId': 0,
        'amount': montant,
        'currency': devise,
        'date': '2020-11-04T11:13:36.805Z',
        'description': 'Paiement de la commande voucher Ref. ' + reference,
        'feesIn': true,
        'operationType': 3,
        'sender': input_numeroCompte.val(),
        'token': token
    };

    $.ajax({
        url: baseUrl + 'v1/transfertMpata',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {

        $('#loading_img').hide();

        if (data == '-1') {
            showErrorMessageOnRequest();
            return;
        }

        if (data[0].status == 'SUCCESS') {
            $('#modalPaiementMpata').modal('hide');
            var message = 'Le transfert d\'argent effectu\351 avec succès.';
            Swal.fire({
                title: "Paiement eMpata",
                html: message,
                type: "info",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            setTimeout(function () {
                window.location = 'paiement_success?reference=' + referenceToken;
            }, 2e3);
        } else if (data[0].status == '400') {
            var message = 'D\351sol\351! Votre solde est insuffisant pour effectuer cette op\351ration.';
            Swal.fire({
                title: "Paiement eMpata",
                html: message,
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            return;
        } else {
            var message = 'D\351sol\351! Votre transaction a \351chou\351. Veuillez contacter l\'administrateur.';
            Swal.fire({
                title: "Transfert",
                html: message,
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            return;
        }
    });
}