var tempBienList = [],
        commandeList = [],
        detailCmdList = [];

var commandeId, stamp_, img, table, idCarte, reference;
var btn_attribuer = $('#btn_attribuer'),
        bntRechercheAvancee = $('.bntRechercheAvancee'),
        btAdvancedSearch = $('.btAdvancedSearch'),
        rechercheAvancee = $('.rechercheAvancee'),
        cmb_etat = $('#cmb_etat'),
        dateDebut = $('#dateDebut'),
        dateFin = $('#dateFin');

$(function () {

    checkIfConnexionOn();
    setMenu();

    sendData('v1/getCommandesGoPass/' + userNif, 'GET', 'getCommandesGoPass', 'JSON', '');

    $('#cmb_etat').select2();

    bntRechercheAvancee.on('click', function (e) {
        e.preventDefault();
        rechercheAvancee.modal('show');
    });

    btAdvancedSearch.on('click', function (e) {
        e.preventDefault();
        advancedSearch();
    });

    dateDebut.val(getDateFormatYyMmDd(getDateToday()));
    dateFin.val(getDateFormatYyMmDd(getDateToday()));
    setEtatCommande();

});

function sendData(url, method, operation, type, data) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + url,
        crossDomain: true,
        method: method,
        contentType: 'application/json',
        data: data,
        headers: headers,
        dataType: type
    }).done(function (data) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        }

        switch (operation) {
            case "getCommandesGoPass":
                if (data === "0") {
                    if ($('#typeCommande').val() == '1') {
                        setTableDataCommande('');
                    }
                    return;
                }
                commandeList = data;

                if ($('#typeCommande').val() == '1') {
                    setTableDataCommande(data);
                }
                break;
            case "getDetailCommandeVoucher":
                if (data === "0") {
                    setTableDataDetailsCommande('');
                    return;
                }
                detailCmdList = data;
                setTableDataDetailsCommande(data);
                break;
            case "printNotePaiement":
                if (data === "0") {
                    Swal.fire({
                        title: "Note de Paiement",
                        text: 'Aucune note de paiement correspondant \340 cette reference : ' + reference,
                        type: "info",
                        confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
                    });
                    return;
                }

                window.location.reload();
                setDocumentContent(data);
                window.open('impression_note', '_blank');
                break;
            case "setPreuvePaiementCommande":
                if (data === "0") {
                    Swal.fire({
                        title: "Paiement",
                        html: "L'enregistrement de votre preuve de paiement n'a pas abouti. Veuillez réessayer.",
                        type: "info",
                        confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
                    });
                    return;
                }
                Swal.fire({
                    title: "Paiement", html: "L'enregistrement de votre preuve de paiement a été un succès.",
                    type: "success",
                    confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"});

                sendData('v1/getCommandesGoPass/' + userNif, 'GET', 'getCommandesGoPass', 'JSON', '');
                break;
        }
    });
}

function setTableDataCommande(data) {

    var numero = 0, montantUSD = 0, montantCDF = 0;
    var tableContent = '';
    tableContent += '<thead style="background-color:#5867dd;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="color:white">#</th>';
    tableContent += '<th style="text-align:left; color:white">MONTANT</th>';
    tableContent += '<th style="text-align:left; color:white">ETAT</th>';
    tableContent += '<th style="text-align:left; color:white">REFERENCE</th>';
    tableContent += '<th style="text-align:left; color:white">DETAILS</th>';
//    tableContent += '<th style="text-align:left; color:white">NOTE PAIEMENT</th>';
    tableContent += '<th style="text-align:left; color:white">PAYER</th>';
//    tableContent += '<th style="text-align:left; color:white">PREUVE PAIEMENT</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {
        numero += 1;
        data[i].devise === 'USD' ? montantUSD += data[i].total : montantCDF += data[i].total;
        var etat = '<td style="vertical-align:middle;color:red;" scope="col">EN ATTENTE DE VALIDATION</td>';
        var payBouton = '<td><button type="button" class="btn btn-primary" disabled title="Payer la commande">Payer</button></td>';
        var bouttonNote = '<td><button type="button" class="btn btn-primary" disabled title="Imprimer note de paiement manuel"><i class="flaticon2-print"></i></button></td>';
        var bouttonPreuvePaiement = '<td></td>';

        switch (data[i].etat) {
            case 1:
                etat = '<td style="vertical-align:middle;color:green;" scope="col">PAYEE</td>';
                if (data[i].compteBancaire !== '') {
                    bouttonNote = '<td><button type="button" class="btn btn-info" onclick="printNotePaiement(\'' + data[i].reference + '\')" title="Voire la note de paiement manuel"><i class="flaticon2-print"></i></button></td>';
                }
                payBouton = '<td></td>';
                if (data[i].preuvePaiement !== '') {
                    bouttonPreuvePaiement = '<td><button type="button" class="btn btn-secondary" data-toggle="popover-click" onclick="setImage(\'' + data[i].id + '\')" title="Visualiser"><i class="flaticon2-photograph"></i>Visualiser</button></td>';
                } else {
                    bouttonPreuvePaiement = '<td></td>';
                }
                break;
            case 2:
                etat = '<td style="vertical-align:middle;color:blue;" scope="col">VALIDEE</td>';
                payBouton = '<td><button type="button" class="btn btn-primary" onclick="setPayementData(\'' + data[i].id + '\')" title="Payer la commande">Payer</button></td>';
                if (data[i].compteBancaire !== '') {
                    bouttonNote = '<td><button type="button" class="btn btn-info" onclick="printNotePaiement(\'' + data[i].reference + '\')" title="Imprimer note de paiement manuel"><i class="flaticon2-print"></i></button></td>';
                }
                if (data[i].preuvePaiement !== '') {
                    bouttonPreuvePaiement = '<td><button type="button" class="btn btn-secondary" data-toggle="popover-click" onclick="setImage(\'' + data[i].id + '\')" title="Visualiser"><i class="flaticon2-photograph"></i>Visualiser</button></td>';
                } else {
                    bouttonPreuvePaiement = '<td scope="col"><input type="file" onclick="uploadPreuvePaiement(\'' + data[i].id + '\')" onchange="uploadStamp(this)" name="avatar" accept="image/png, image/jpeg"></td>';
                }
                break;
            case 4:
                etat = '<td style="vertical-align:middle;color:green;" scope="col">LIVREE</td>';
                bouttonNote = '<td><button type="button" class="btn btn-info" onclick="printNotePaiement(\'' + data[i].reference + '\')" title="Voir la note de paiement manuel"><i class="flaticon2-print"></i></button></td>';
                payBouton = '<td></td>';
                break;
            case 5:
                etat = '<td style="vertical-align:middle;color:green;" scope="col">EN ATTENTE DE LIVRAISON</td>';
                bouttonNote = '<td><button type="button" class="btn btn-info" onclick="printNotePaiement(\'' + data[i].reference + '\')" title="Voir la note de paiement manuel"><i class="flaticon2-print"></i></button></td>';
                payBouton = '<td></td>';
                break;
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;">' + numero + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + formatNumber(data[i].total, data[i].devise) + '</td>';
        tableContent += etat;
        tableContent += '<td style="vertical-align:middle;">' + data[i].reference + '</td>';
        tableContent += '<td><button type="button" class="btn btn-primary" onclick="getDetails(\'' + data[i].id + '\')" title="Voir les détails"><i class="flaticon2-soft-icons"></i></button></td>';//Voir détails
//        tableContent += bouttonNote;
        tableContent += payBouton;
//        tableContent += bouttonPreuvePaiement;
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';
    tableContent += '<tr>';
    tableContent += '<th colspan="1" rowspan="2" class="text-right label-general pr-3" style="color:red"> TOT.</th>';
    tableContent += '<th colspan="5"><span class="general_usd" style="color:green;font-size:14px"></span> </th>';
    tableContent += '</tr>';
    tableContent += '<tr>';
    tableContent += '<th colspan="5"><span class="general_cdf" style="color:blue;font-size:14px""></span> </th>';
    tableContent += '</tr>';
    tableContent += '</tfoot>';


    $('#table_cmd_voucher').html(tableContent);
    $('#table_cmd_voucher').DataTable({
        "order": [[1, "asc"]],
        destroy: true,
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...", zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible", search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
    $('.general_usd').html(formatNumber(montantUSD, 'USD'));
    $('.general_cdf').html(formatNumber(montantCDF, 'CDF'));

}

function uploadPreuvePaiement(id) {
    commandeId = id;
}

function uploadStamp(input) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            var stamp = e.target.result;
            stamp_ = stamp;

        };
        reader.readAsDataURL(input.files[0]);
    }

    var msg = 'Etes-vous s\373r de vouloir insérer une preuve de paiement pour cette commande';
    Swal.fire({
        title: "Paiement",
        html: msg, type: "info",
        showCancelButton: !0,
        confirmButtonText: "Confirmer",
        cancelButtonText: "Annuler"
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            savingPreuvePaiement(commandeId, stamp_);
        }

    });
}

function getDetails(idCommande) {
    sendData('v1/getDetailCommandeVoucher/' + idCommande, 'GET', 'getDetailCommandeVoucher', 'JSON', '');
}

function setTableDataDetailsCommande(data) {
    var numero = 0, totalPU = 0, quantite = 0, netPay = 0, devise = '';
    var tableContent = '';
    tableContent += '<thead style="background-color:#5867dd;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="color:white">#</th>';
    tableContent += '<th style="text-align:left; color:white">CATEGORIE GOPASS</th>';
    tableContent += '<th style="text-align:left; color:white">PRIX UNITAIRE</th>';
    tableContent += '<th style="text-align:left; color:white">QUANTITE</th>';
    tableContent += '<th style="text-align:left; color:white">NET A PAYER</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {
        numero += 1;
        totalPU += data[i].prix_unit;
        quantite += data[i].quantite;
        netPay += data[i].montant;
        devise = data[i].devise;

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;">' + numero + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + data[i].libTarif + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + formatNumber(data[i].prix_unit, data[i].devise) + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + data[i].quantite + '</td>';
        tableContent += '<td style="vertical-align:middle; color:blue">' + formatNumber(data[i].montant, data[i].devise) + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';
    tableContent += '<tr>';
    tableContent += '<th colspan="2" class="text-right label-general pr-3" style="color:red"> TOTAL GENERAL</th>';
    tableContent += '<th colspan="1"><span class="totalPU" style="color:blue;font-size:14px"></span> </th>';
    tableContent += '<th colspan="1"><span class="quantite" style="color:blue;font-size:14px"></span> </th>';
    tableContent += '<th colspan="1"><span class="netPay" style="color:blue;font-size:1.4em;background-color:#FFFF00"></span> </th>';
    tableContent += '</tr>';
    tableContent += '</tfoot>';

    $('#table_dcv').html(tableContent);
    $('#table_dcv').DataTable({
        "order": [[1, "asc"]],
        destroy: true,
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible",
            search: "Rechercher _INPUT_  ",
            paginate: {first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
    $('.totalPU').html(formatNumber(totalPU, devise));
    $('.quantite').html(quantite);
    $('.netPay').html(formatNumber(netPay, devise));
    $('.modalDetailsCommande').modal('show');
}

function printNotePaiement(reference) {

    var msg = 'Etes-vous s\373r de vouloir imprimer la note de paiement de r\351f\351rence : <label style="color:blue;font-weight:bold">' + reference + '</label> ?';

    Swal.fire({
        title: "Note de Paiement",
        html: msg,
        type: "info",
        showCancelButton: !0,
        confirmButtonText: "Confirmer",
        cancelButtonText: "Annuler"
    }).then(function (isConfirm) {
        if (isConfirm.value) {
            impressionNotePaiement(reference);
        }
    });
}

function setPayementData(id) {
    var objet = {
        "id": id,
        "commandeList": commandeList
    };
    payementData(objet);
}

function impressionNotePaiement(ref) {
    reference = ref;
    sendData('v1/printNotePaiement/' + ref + '/' + userNif, 'GET', 'printNotePaiement', 'text', '');
}

function savingPreuvePaiement(commandeId, base64) {
    var object = {
        'id': commandeId,
        'base64': base64
    };
    sendData('v1/setPreuvePaiementCommande', 'POST', 'setPreuvePaiementCommande', 'text', JSON.stringify(object));
}
function setImage(id) {

    $('[data-toggle="popover"]').popover();

    $.each(commandeList, function (index, item) {
        if (item.id == id) {
            var src = '<img src="' + item.preuvePaiement + '" width="100%" height="100%" />';
            $('[data-toggle="popover-click"]').popover({
                html: true,
                trigger: 'focus',
                placement: 'auto',
                content: function () {
                    return src;
                }
            });
        }
    });

}

function advancedSearch() {

    var listCommande = [];
    var objet;
    var etat, date1, date2;

    $.each(commandeList, function (index, item) {

        cmb_etat.val() === null ? etat = null : etat = item.etat;
        dateDebut.val() === '' ? date1 = -1 : date1 = compareDates(dateDebut.val(), item.date);
        dateFin.val() === '' ? date2 = 1 : date2 = compareDates(dateFin.val(), item.date);

        if ((date1 === -1 || date1 === 0)
                && (date2 === 1 || date2 === 0)
                && (cmb_etat.val() == etat)) {
            objet = {
                compteBancaire: item.compteBancaire,
                date: item.date,
                devise: item.devise,
                etat: item.etat,
                id: item.id,
                notePerception: item.notePerception,
                preuvePaiement: item.preuvePaiement,
                reference: item.reference, token: item.token,
                total: item.total
            };
            listCommande.push(objet);
        }
    });

    setTableDataCommande(listCommande);
    var paramEtat = cmb_etat.val(), date_debut = dateDebut.val(), date_fin = dateFin.val();

    var e = document.getElementById("cmb_etat");
    paramEtat === null ? paramEtat = 'tout' : paramEtat = e.options[e.selectedIndex].text;
    date_debut === '' ? date_debut = '' : date_debut = '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Du&nbsp;' + getDateFormatMnDdYy(dateDebut.val());
    date_fin === '' ? date_fin = '&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;Date fin:&nbsp;' + getDateToday() : date_fin = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Au&nbsp;' + getDateFormatMnDdYy(dateFin.val());

    $('.param_cmd').html("R&eacute;pertoire des commandes&nbsp;&nbsp;&nbsp;=>&nbsp;&nbsp;&nbsp;<span style=\"font-weight: normal\">Etat:&nbsp;" + paramEtat + date_debut + date_fin + "</span>");
    rechercheAvancee.modal('hide');
}

function setEtatCommande() {
    var option = '';
    option += '<option value="0" disabled selected>S&eacute;lectionner un &eacute;tat</option>';
    if (IS_PEAGE_LUALABA !== '1') {
        option += '<option value="3">En attente de validation</option>';
    }
    option += '<option value="5">En attente de livraison</option>';
    option += '<option value="4">Livr&eacute;e</option>';
    option += '<option value="1">Pay&eacute;e</option>';
    option += '<option value="2">Valid&eacute;e</option>';
    $('#cmb_etat').html(option);
}