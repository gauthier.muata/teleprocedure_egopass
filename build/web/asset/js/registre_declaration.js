/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var listRetraitDeclaration = [];

$(function () {

    checkIfConnexionOn();
    setMenu();

    loadRetraitDeclaration();
});

function loadRetraitDeclaration() {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Récupération des données..."
    });

    $.ajax({
        type: 'GET',
        url: baseUrl + '/v3/declaration/' + userNif,
        dataType: 'JSON',
        crossDomain: false,
        success: function (response)
        {
            setTimeout(function () {
                KTApp.unblockPage();

                if (response == '-1') {
                    showResponseError();
                    return;
                } else if (response == '-6') {
                    loadRetraitDeclarationTable('');
                } else {
                    listRetraitDeclaration = $.parseJSON(JSON.stringify(response));
                    loadRetraitDeclarationTable(listRetraitDeclaration);
                }
            });

        },
        error: function (jqXHR, textStatus, errorThrown) {
            KTApp.unblockPage();
            showResponseError();
        }

    });
}

function loadRetraitDeclarationTable(result) {

    var index = 0;
    var tableContent = '';
    tableContent += '<thead style="background-color:#5867dd;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="color:white">#</th>';
    tableContent += '<th style="text-align:left; color:white">DATE DECLARATION</th>';
    tableContent += '<th style="text-align:left; color:white">NUMERO</th>';
    tableContent += '<th style="color:white">ARTICLE BUDGEAIRE</th>';
    tableContent += '<th style="text-align:left; color:white">REQUERANT</th>';
    tableContent += '<th style="text-align:left; color:white">MONTANT D&Ucirc;</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < result.length; i++) {

        index += 1;
        tableContent += '<tr>';
        tableContent += '<td>' + index + '</td>';
        tableContent += '<td>' + result[i].dateRetrait + '</td>';
        tableContent += '<td>' + result[i].numero + '</td>';
        tableContent += '<td>' + result[i].articleBudgetaire + '</td>';
        tableContent += '<td>' + result[i].requerant + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + result[i].montantDu + ' ' + result[i].devise + '</td>';
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';
    $('#tableDeclaration').html(tableContent);
    $('#tableDeclaration').DataTable({
        "order": [[1, "asc"]],
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

}