var validNumber,
        initError = false,
        labelSelectedAdress = '',
        stringAdress = '',
        adresse,
        numero = '';

var tempAdresseList = [];

var inputTelephone = $('#inputTelephone'),
        formLogin = $('#loginform'),
        signupForm = $('#signup-form'),
        username = $('#username'),
        password = $('#password'),
        loading2 = $('#loading2_img'),
        csrfToken = $('#csrf_token'),
        loading_img = $('#loading_img'),
        inputLibelle = $('#inputLibelle'),
        inputNumber = $('#inputNumber'),
        btnRegister = $('.btn-register'),
        btnInscription = $('.btn-primary-register'),
        tableAdresse = $('#tableAdresse'),
        btnValiderAdresse = $('#btnValiderAdresse'),
        btnRechercherAdresse = $('#btnRechercherAdresse'),
        btn_add_address = $('.btn_add_address'),
        cmbTypePersonne = $('#cmbTypePersonne');

$(function () {

    var url = getUrlParameter('id');
    if (!isUndefined(url)) {
        $('.text-register-dark').html('Activation du compte');
        $('.form-register').addClass('hidden');
        $('.form-success-register').addClass('hidden');
        $('.form-active-register').removeClass('hidden');

        sendData(baseUrl + 'v1/activateAccount/' + url, 'activateAccount', 'GET', 'text', '', '', '');
        return;
    }

    $('.form-active-register').addClass('hidden');

    $('.lblNIf').html('Numéro d\'identification');
    $('#inputNif').attr('placeholder', 'Veuilez saisir votre numéro d\'identification');
    inputNumber.addClass('hidden');
    btnValiderAdresse.addClass('hidden');
    loading_img.hide();

    printAdresse('');

    inputLibelle.keypress(function (e) {
        if (e.keyCode === 13) {
            btnRechercherAdresse.trigger('click');
        }
    });

    btnValiderAdresse.click(function (e) {
        e.preventDefault();
        if (labelSelectedAdress === '') {
            alertify.alert('Veuillez d\'abord rechercher et s\351lectionner une adresse.');
            return;
        }
        if (inputNumber.val() == '') {
            alertify.alert('Veuillez d\'abord saisir un numéro de l\'adresse en cours de sélectionner.');
            return;
        }

        numero = ' n°' + inputNumber.val();
        if (stringAdress !== '') {
            stringAdress += '\n' + labelSelectedAdress + numero;
        } else {
            stringAdress = labelSelectedAdress + numero;
        }
        $('#registration_form_adresse').val(stringAdress);
        adresse.numero = inputNumber.val();
        tempAdresseList.push(adresse);
        inputNumber.val('');
        $('#labelSelectedAdresse').html('');
        $('#nouvelleAdresse').modal('hide');
        addAdressList();
    });

    btn_add_address.on('click', function () {

        if (tempAdresseList.length !== 0) {
            alertify.confirm('Voulez-vous ajouter une autre adresse ?', function () {
                $('#nouvelleAdresse').modal('show');
            });
        } else {
            $('#nouvelleAdresse').modal('show');
        }
    });

    btnRechercherAdresse.on('click', function () {
        loadAdresses();
    });

    btnInscription.click(function (e) {
        askIdentification();
    });

    setTypePersonne();

    $('.btn-dark-cancel').on('click', function (e) {
        e.preventDefault();
        window.location.reload();
    });

});

function addAdressList() {
    var item = '';
    tempAdresseList.length === 1 ? tempAdresseList[0].default = 'Oui' : '';

    for (var i = 0; i < tempAdresseList.length; i++) {
        if (tempAdresseList[i].default === 'Oui') {
            item += '<div class="list-group-item" style="background-color: #c1e2b3;color:black">' + tempAdresseList[i].chaine + ' n°' + tempAdresseList[i].numero +
                    '&nbsp;&nbsp;<i class="alert-danger fa fa-trash-o" style="cursor: pointer" onclick="deleteAddress(\'' + tempAdresseList[i].code + '\')" title="Supprimer"></i></div>';
        } else {
            item += '<div class="list-group-item">' + tempAdresseList[i].chaine + ' n°' + tempAdresseList[i].numero +
                    '&nbsp;&nbsp;<i class="alert-danger fa fa-trash-o" style="cursor: pointer" onclick="deleteAddress(\'' + tempAdresseList[i].code + '\')" title="Supprimer"></i>' +
                    '&nbsp;&nbsp;<i class="alert-success fa fa-check" style="cursor: pointer" onclick="setDefaultAddress(\'' + tempAdresseList[i].code + '\')" title="Adresse par d&eacute;faut"></i></div>';
        }
    }
    if (tempAdresseList.length > 1) {
        $('.adress-set').attr('style', 'height: 100px;overflow-y: scroll;');
    } else if (tempAdresseList.length === 1) {
        $('.adress-set').removeAttr('style');
    } else {
        item = '<div class="list-group-item adresse-error">Ajouter une adresse</div>';
        $('.adress-set').removeAttr('style');
    }
    $('.adress-set').html(item);
}

function deleteAddress(code) {

    alertify.confirm('Etes-vous s\373r de vouloir supprimer cette adresse ?', function () {
        for (var i = 0; i < tempAdresseList.length; i++) {
            if (code == tempAdresseList[i].code) {
                tempAdresseList.splice(i, 1);
                addAdressList();
            }
        }
    });
}

function setDefaultAddress(code) {
    for (var i = 0; i < tempAdresseList.length; i++) {
        tempAdresseList[i].default = '';
        if (code === tempAdresseList[i].code) {
            tempAdresseList[i].default = 'Oui';
        }
    }
    addAdressList();
}

function register() {

    if (inputTelephone.length) {
        var input = document.querySelector('#registration_form_telephone');
        var iti = intlTelInput(input, {
            initialCountry: "cd",
            utilsScript: "intl-tel-input/build/js/utils.js"
        });
        inputTelephone.on('blur', function (event) {
            var num = iti.getNumber().substring(1);
            validNumber = iti.isValidNumber();
            if (!validNumber) {
                inputTelephone.parents('.form-group').addClass('has-error');
                if (!initError) {
                    var error = "<div class='error-message error-text'>Ce numéro de téléphone est invalide.</div>";
                    inputTelephone.parents('.form-group').append(error);
                    initError = true;
                }

                btnRegister.attr('disabled', 'true').css('cursor', 'not-allowed');
                $('#registration_form_isvalid').val('invalid');
            } else {
                inputTelephone.parents('.form-group').removeClass('has-error');
                btnRegister.removeAttr('disabled').css('cursor', 'pointer');
                $('#registration_form_isvalid').val('valid');
            }

            $(this).val(num);
        });
    }
}

cmbTypePersonne.on('change', function (event) {

    var value = $(this).val();
    $('.type-personne-error').addClass('hidden');
    if (value == '03') {
        $('.lblNIf').html('Nif');
        $('.morale-field').removeClass('hidden');
        $('.prenom-field').addClass('hidden');
        $('#inputNif').attr('placeholder', 'Veuilez saisir votre nif');
        $('#inputNom').attr('placeholder', 'Veuilez saisir votre raison social');
        $('#inputPostnom').attr('placeholder', 'Veuilez saisir votre sigle');
        $('label[for=inputNom]').html('Raison Social');
        $('label[for=inputPostnom]').html('Sigle');
    } else {
        $('.lblNIf').html('Numéro d\'identification');
        $('.morale-field').addClass('hidden');
        $('.prenom-field').removeClass('hidden');
        $('#inputNif').attr('placeholder', 'Veuilez saisir votre numéro d\'identification');
        $('#inputNom').attr('placeholder', 'Veuilez saisir votre nom');
        $('#inputPostnom').attr('placeholder', 'Veuilez saisir votre postnom');
        $('#inputIdNat').removeAttr('required');
        $('#inputRccm').removeAttr('required');
        $('label[for=inputNom]').html('Nom');
        $('label[for=inputPostnom]').html('Postnom');
    }
});

function askIdentification() {

    $('.type-personne-error').addClass('hidden');
    $('.nif-error').addClass('hidden');
    $('.idnat-error').addClass('hidden');
    $('.rccm-error').addClass('hidden');
    $('.nom-error').addClass('hidden');
    $('.email-error').addClass('hidden');
    $('.phone-error').addClass('hidden');
    $('.adresse-error').attr('style', 'color:#696E74');

    if (cmbTypePersonne.val() === '0' || cmbTypePersonne.val() === null) {
        $('.type-personne-error').removeClass('hidden');
        loading2.addClass('hidden');
        return;
    }

    if (cmbTypePersonne.val() === '03') {
        $('.lblNIf').html('Nif');
        if ($('#inputRccm').val() === '') {
            $('.rccm-error').removeClass('hidden');
            loading2.addClass('hidden');
            return;
        }
    }
    if ($('#inputNom').val() === '') {
        $('.nom-error').removeClass('hidden');
        loading2.addClass('hidden');
        return;
    }
    if ($('#inputEmail').val() === '') {
        $('.email-error').html('Veuillez saisir votre adresse mail.');
        $('.email-error').removeClass('hidden');
        $('.email-error').attr('style', 'color:red');
        loading2.addClass('hidden');
        return;
    }

    if (!validateEmail($('#inputEmail').val())) {
        $('.email-error').html('Votre adresse mail est invalide.');
        $('.email-error').removeClass('hidden');
        loading2.addClass('hidden');
        return;
    }

    if (tempAdresseList.length === 0) {
        $('.adresse-error').html('Veuillez ajouter une adresse.');
        $('.adresse-error').attr('style', 'color:red');
        loading2.addClass('hidden');
        return;
    }

    alertify.confirm('Etes-vous s\373r de bien vouloir cr\351er ce compte ?', function () {

        loading2.addClass('hidden');
        signupForm.addClass('opacity-less');

        var params = {};

        params.nom = $('#inputNom').val(),
                params.postnom = $('#inputPostnom').val(),
                params.prenom = $('#inputPrenom').val(),
                params.nif = $('#inputNif').val(),
                params.forme = cmbTypePersonne.val(),
                params.mail = $('#inputEmail').val(),
                params.telephone = $('#inputTelephone').val(),
                params.idnat = '',
                params.rccm = $('#inputRccm').val(),
                params.adresse = JSON.stringify(tempAdresseList);

        $.ajax({
            url: baseUrl + 'v1/savecustumer',
            type: 'POST',
            dataType: "JSON",
            data: JSON.stringify(params),
            crossDomain: true,
            headers: headers,
            success: function (result) {
                loading2.addClass('hidden');
                if (result.code == 100) {
                    $('.form-register').addClass('hidden');
                    $('.form-success-register').removeClass('hidden');
                } else if (result.code == 200) {
                    alertify.alert(result.message);
                } else {
                    alertify.alert("Votre inscription n'a pas abouti. Veuillez réessayer ou contacter l'administrateur");
                }
            },
            error: function (result) {
                loading2.addClass('hidden');
                alertify.alert("Votre inscription n'a pas abouti. Veuillez réessayer ou contacter l'administrateur");
            }
        });
    });
}

function sendData(url, operation, method, type, data, param, message) {

    $('.fa-circle').removeAttr('hidden');
    $('.spinner-border-sm').attr('hidden', true);

    $.ajax({
        url: url,
        type: method,
        crossDomain: true,
        data: data,
        headers: headers,
        dataType: type
    }).done(function (data) {

        $('.check-square').removeClass('hidden');
        $('.spinner-border-sm').attr('hidden', true);

        if (data == "-1") {
            errorMessage();
            return;
        }

        switch (operation) {
            case 'activateAccount':
                callBackActivateAccount(data);
                break;
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        errorMessage();
    }).always(function () {
        $('.fa-circle').removeAttr('hidden');
        $('.spinner-border-sm').attr('hidden', true);
    });

}

function callBackActivateAccount(data) {

    $('.text-register-dark1').addClass('text-left');
    $('.text-register-dark').addClass('hidden');

    if (data == "0") {
        $('.text-response').addClass('text-danger');
        $('.failed-activate').removeClass('hidden');
        $('.success-activate').addClass('hidden');
        $('.message-text-activated').html('L\'activation de votre compte n\'a pas abouti. Veuillez <a href="" onclick="reload()">r\351esayer.</a>');
        return;
    } else if (data == "2") {
        $('.text-response').addClass('text-danger');
        $('.failed-activate').removeClass('hidden');
        $('.success-activate').addClass('hidden');
        $('.message-text-activated').html('Nous avons du mal à retrouver votre compte. Veuillez contacter l\'administrateur.');
        return;
    }

    $('.text-response').addClass('text-success');
    $('.failed-activate').addClass('hidden');
    $('.success-activate').removeClass('hidden');
    $('.message-text-activated').html('Votre compte a \351t\351 activ\351 avec succès. <a href="login">Connectez-vous</a>');
}

function reload() {
    window.location.reload();
}

function loadAdresses() {

    loading_img.show();

    $.ajax({
        method: 'GET',
        url: baseUrl + 'v1/getAdresses/' + inputLibelle.val(),
        crossDomain: true,
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {
        loading_img.hide();
        if (data.length > 0) {
            var adresseList = $.parseJSON(JSON.stringify(data));
            inputNumber.removeClass('hidden');
            btnValiderAdresse.removeClass('hidden');
            printAdresse(adresseList);
        } else {
            printAdresse('');
        }
    }).fail(function () {
        loading_img.hide();
        showResponseError();
    }).always(function () {
    });
}

function printAdresse(adresseList) {

    var header = '<thead><tr>';
    header += '<th scope="col"> PROVINCE </th>';
    header += '<th scope="col"> VILLE </th>';
    header += '<th scope="col"> COMMUNE </th>';
    header += '<th scope="col"> QUARTIER </th>';
    header += '<th scope="col"> AVENUE </th>';
    header += '<th hidden="true" scope="col"> Code avenue </th>';
    header += '</tr></thead>';
    var body = '<tbody id="tbodyAdresses">';
    for (var i = 0; i < adresseList.length; i++) {
        if (adresseList[i].province === undefined || adresseList[i].province == 'undefined'
                || adresseList[i].province === null || adresseList[i].province == 'null') {
            continue;
        }
        body += '<tr>';
        body += '<td>' + adresseList[i].province.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].ville.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].commune.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].quartier.toUpperCase() + '</td>';
        body += '<td>' + adresseList[i].avenue.toUpperCase() + '</td>';
        body += '<td hidden="true">' + adresseList[i].codeAvenue + '</td>';
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    tableAdresse.html(tableContent);
    var dtAdresse = tableAdresse.DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Rechercher par N° document _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: false,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 7,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    $('#tableAdresse tbody').on('click', 'tr', function () {
        var data = dtAdresse.row(this).data();
        adresse = new Object();
        adresse.code = data[5];
        adresse.chaine = data[0].toUpperCase() + '--> V/' + data[1].toUpperCase() + ' - C/' + data[2].toUpperCase() + ' - Q/' + data[3].toUpperCase() + ' - Av. ' + data[4].toUpperCase();
        adresse.numero = '';
        adresse.default = '';
        adresse.codeAP = '';
        adresse.etat = '1';
        $('#labelSelectedAdresse').html(adresse.chaine);
        labelSelectedAdress = adresse.chaine;
    });
}

function setTypePersonne() {

    var option = '<option value="0" selected disabled>S&eacute;lectionnez le type de personne</option>';
    option += '<option value="03">Personne morale</option>';
    option += '<option value="04">Personne physique</option>';
    cmbTypePersonne.html(option);
    cmbTypePersonne.select2();

}