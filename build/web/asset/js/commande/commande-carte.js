/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var input_qte = $('#input_qte'),
        btnCommandeCarte = $('#btnCommandeCarte');

var netPayCard = 0;
var commandeList = [];

$(function () {
    
    checkIfConnexionOn();
    setMenu();

    $('.prix_carte').html('<div class="list-group-item" style="color:blue">' + CARD_PRICE + ' USD</div>');

    input_qte.on('keypress', function (event) {
        var regex = new RegExp("^[0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
//            btnAddCommande
            event.preventDefault();
            return false;
        }
//        netPayCard = CARD_PRICE * input_qte.val();
//        alert(input_qte.val())
//        $('.montant_net').html('<div class="list-group-item" style="color:blue">' + netPayCard + '$</div>');
//        $('.div_montant_net').removeClass('hidden');
    });

    $('#input_qte').on('change', function (event) {
        netPayCard = CARD_PRICE * $('#input_qte').val();
        $('.div_montant_net').removeClass('hidden');
        $('.montant_net').html('<div class="list-group-item" style="color:blue">' + netPayCard + ' USD</div>');
    });

    btnCommandeCarte.on('click', function () {
        alertify.confirm('Etes-vous sûr de vouloir soumettre cette commande ?', function () {
            saveCommandeCarte();
        });
    });
    getListComandesCartes();

});

function saveCommandeCarte() {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    var object = {
        'nif': userNif,
        'prix': CARD_PRICE,
        'quantite': input_qte.val(),
        'total': CARD_PRICE * input_qte.val(),
        'devise': "USD"
    };

    $.ajax({
        url: baseUrl + 'v1/saveCommandeCarte',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        } else if (data == "0") {
            Swal.fire({
                title: "Commande",
                text: "L'enregistrement de votre commande n'a pas a abouti. Veuillez réessayer.",
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            return;
        } else {
            Swal.fire({
                title: "Commande",
                html: "Votre commande de référence <span style=\"color:red; font-weight:bold\">" + data.reference + "</span> a été enregistrée avec succès. Cependant, elle reste <a href='registre-commande'>en attente de validation</a>.",
                type: "success",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            setTimeout(function () {
                window.location.reload();
            }, 3500);
            getListComandesCartes();
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessageOnRequest();
        KTApp.unblockPage();
    });
}

function getListComandesCartes() {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + 'v1/getListComandesCartes/' + userNif + '',
        crossDomain: true,
        method: 'GET',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        } else if (data == "0") {
            setTableDataCommandeCartes('');
            return;
        }
        commandeList = data;
        console.log(data)
        setTableDataCommandeCartes(data);


    }).fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessageOnRequest();
    }).always(function () {
        KTApp.unblockPage();
    });
}

function setTableDataCommandeCartes(data) {

    var numero = 0;
    var tableContent = '';
    tableContent += '<thead style="background-color:#5867dd;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="color:white">#</th>';
    tableContent += '<th style="text-align:left; color:white">REFERENCE</th>';
    tableContent += '<th style="text-align:left; color:white">PRIX UNITAIRE</th>';
    tableContent += '<th style="text-align:left; color:white">QUANTITE</th>';
    tableContent += '<th style="text-align:left; color:white">NET A PAYER</th>';
    tableContent += '<th style="text-align:left; color:white">ETAT</th>';
    tableContent += '<th style="text-align:left; color:white"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {
        numero += 1;
        var etat = '<td style="vertical-align:middle;color:red;" scope="col">En attente de validation</td>';
        var payBouton = '<td><button type="button" class="btn btn-primary" disabled title="Payer la commande">Payer</button></td>';

        if (data[i].etat == 2) {
            etat = '<td style="vertical-align:middle;color:blue;" scope="col">Validé</td>';
            payBouton = '<td><button type="button" class="btn btn-primary" onclick="setPayementData(\'' + data[i].id + '\')" title="Payer la commande">Payer</button></td>';
        } else if (data[i].etat == 1) {
            etat = '<td style="vertical-align:middle;color:green;" scope="col">Payé</td>';
            payBouton = '<td style="width:10%" scope="col"></td>';
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;">' + numero + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + data[i].reference + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + formatNumber(data[i].prix_unit, data[i].devise) + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + data[i].quantite + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + formatNumber(data[i].total, data[i].devise) + '</td>';
        tableContent += etat;
        tableContent += payBouton;
        tableContent += '</tr>';
    }

    tableContent += '</tbody>';
    $('#table_cmd_carte').html(tableContent);
    $('#table_cmd_carte').DataTable({
        "order": [[1, "asc"]],
        destroy: true,
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

}

function setPayementData(id) {
    var objet = {
        "id": id,
        "commandeList": commandeList
    };
    payementData(objet);
}