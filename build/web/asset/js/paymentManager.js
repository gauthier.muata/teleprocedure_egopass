/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var listCmd = [];
var idCmd,
        amount = 0,
        devise = '',
        reference = '',
        token = '',
        xyz;

function payementData(objet) {
    
    checkIfConnexionOn();
    setMenu();

    idCmd = objet.id;
    listCmd = objet.commandeList;

    $.each(listCmd, function (index, item) {
        if (item.id == idCmd) {
            amount = item.total;
            devise = item.devise;
            reference = item.reference;
            token = item.token;
        }
    });

    xyz = amount + '@' + devise + '@' + token + '@' + reference;

    $.ajax({
        type: 'GET',
        url: 'asset/admin/js/methodesPaiement.json',
        dataType: 'JSON',
        async: false,
        success: function (data) {
            paiement(data);
        },
        error: function () {
        }
    });
}

function paiement(mode_paiement) {
    var data = '';
    data += '<div class="alert alert-primary" style="font-size:14px;">';
    data += '<strong id="notificationSelectedMethod">Veuillez choisir le mode de paiement qui vous convient.</strong>';
    data += '</div>';

    data += '<div class="paymentWrap" id="divModePayment">';
    data += '<div class="btn-group paymentBtnGroup btn-group-justified" data-toggle="buttons">';

    for (var i = 0; i < mode_paiement.length; i++) {

        if (!mode_paiement[i].visible) {
            continue;
        }

        data += '<label class="btn paymentMethod">';
        data += '<div id="selected_' + mode_paiement[i].id + '" class="method cash" value="' + mode_paiement[i].id + '" onclick="selectPayementmethod(' + mode_paiement[i].id + ')" class="method cash">' + mode_paiement[i].name + '<br/><img width="50px" height="50px" src="' + mode_paiement[i].icon + '" /></div>';
        data += '<input type="radio" id="method" value="' + mode_paiement[i].id + '"></label>';
    }

    $('#idPaiement').html(data);
    $('#modalMoyenPaiement').modal('show');
}

function selectPayementmethod(id) {

    switch (id) {
        case 100:
            $('#modalMoyenPaiement').modal('hide');
            payWithMaxiCash(reference, amount);
            break;
        case 200:
            window.location = 'empata-paiement?id=' + btoa(xyz);
            break;
        case 300:
            $('#modalMoyenPaiement').modal('hide');
            pay(urlId, montant);
            break;
        case 400:
            $('#modalMoyenPaiement').modal('hide');
            pay(urlId, montant);
            break;
        case 500:
            $('#modalMoyenPaiement').modal('hide');
            pay(urlId, montant);
            break;
        case 600:
            $('#modalMoyenPaiement').modal('hide');
            showAlertMsgValidation();
            break;
    }

}

function showAlertMsgValidation() {
    var message = '';
    message += '<div class="text-center">';
    message += '<form class="form-verification">';
    message += '<div class="form-group mb-0">';
    message += '<input type="text" class="form-control" id="code_validation">';
    message += '<div class="invalid-feedback">Veuillez entrer votre numéro Pepele Mobile</div>';
    message += '</div>';
    message += '<div class="text-center mt-4">';
    message += '<button class="mt-4 btn btn-brand kt-btn kt-btn--wide btn-validate-verification">Valider</button>';
    message += '<button class="mt-4 btn btn-danger kt-btn kt-btn--wide btn-cancel-verification">Annuler</button>';
    message += '</div>';
    message += '</form>';
    message += '</div>';

    Swal.fire({
        title: "Entrez votre numéro Pepele Mobile",
        html: message,
        type: "info",
        showConfirmButton: false,
        allowOutsideClick: false
    });

    $('.btn-validate-verification').on('click', function (event) {
        event.preventDefault();

        var phone_number = $('#code_validation').val();

        if (phone_number != '') {

            Swal.close();
            $('.form-verification .form-group').removeClass('validated');

            payWithPepeleMobile(amount, devise, token, phone_number);
            loadPayementData();

        } else {
            $('.form-verification .form-group').addClass('validated');
            $('#code_validation').val('');
        }
    });

    $('.btn-cancel-verification').on('click', function (event) {
        event.preventDefault();
        Swal.close();
    });
}

function payWithPepeleMobile(amount, devise, token, mobile) {

    var getTypeUrl = token.substr(10, 3);

    setReference(getTypeUrl);

    if (devise == 'USD') {
        amount = amount * 100;
    }

    var dataPayement = {
        'amount': amount,
        'devise': devise,
        'reference': token,
        'referenceLabel': reference,
        'description': 'Ma commande des vouchers',
        'phonenumber': mobile,
        'accepturl': host + '/teledeclaration/paiement_success',
        'cancelurl': host + '/teledeclaration/paiement_failed',
        'declineurl': host + '/teledeclaration/paiement_failed'

    };

    payPepele(dataPayement);
}

function payWithMaxiCash(reference, montant) {

    var dataPayement = {
        'PayType': 'MaxiCash',
        'Amount': montant * 100,
        'Currency': 'MaxiDollar',
        'MerchantID': '0c15cf4a03f64d4793ce9aa956a29d42',
        'MerchantPassword': '11e8b003ff394e1d8d935ecdcfa0b9b0',
        'Reference': reference,
        'Language': 'fr',
        'Telephone': '00243821416216',
        'Email': 'moussa.toure@hologram.cd',
        'accepturl': host + '/teledeclaration/paiement_success',
        'cancelurl': host + '/teledeclaration/paiement_failed',
        'declineurl': host + '/teledeclaration/paiement_failed',
        'notifyurl': host + '/teledeclaration/paiement_failed'
    };

    openPopupPage(dataPayement);
}

function payPepele(data) {
    var param = JSON.parse(JSON.stringify(data));
//    OpenWindowWithPost("https://hautkatanga-gouv.net/pay/pepele.php", "width=1000, height=600, left=100, top=100, resizable=yes, scrollbars=yes", "", param);
    OpenWindowWithPost("http://74.208.121.66/pepele/online/paiement_online.php", "width=1000, height=600, left=100, top=100, resizable=yes, scrollbars=yes", "", param);
//    OpenWindowWithPost("http://demo.hologram.cd/pepele/online/paiement_online.php", "width=1000, height=600, left=100, top=100, resizable=yes, scrollbars=yes", "", param);
}

function openPopupPage(data) {
    var param = JSON.parse(JSON.stringify(data));

    OpenWindowWithPost2("https://api.maxicashapp.com/PayEntryPost", param);
}

function OpenWindowWithPost(url, windowoption, name, params) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", url);
    form.setAttribute("target", name);
    for (var i in params) {
        if (params.hasOwnProperty(i)) {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = i;
            input.value = params[i];
            form.appendChild(input);
        }
    }
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function OpenWindowWithPost2(url, params) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", url);
    form.setAttribute("target", name);
    for (var i in params) {
        if (params.hasOwnProperty(i)) {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = i;
            input.value = params[i];
            form.appendChild(input);
        }
    }
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}