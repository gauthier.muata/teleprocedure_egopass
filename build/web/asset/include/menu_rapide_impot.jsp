<%-- 
    Document   : menu_rapide_impot
    Created on : 27 mars 2021, 12:59:48
    Author     : Juslin TSHIAMUA
--%>

<div class="kt-portlet__head-toolbar">
    <div class="kt-portlet__head-wrapper">
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-bold btn-upper btn-font-sm" data-toggle="dropdown" aria-haspopup="true">
                <i class="flaticon2-soft-icons"></i>Faire une d&eacute;claration      
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <ul class="kt-nav">
                    <li class="kt-nav__section kt-nav__section--first">
                        <span class="kt-nav__section-text">Actions rapides</span>
                    </li>
                    <li class="kt-nav__item">
                        <a href="vignette-1" class="kt-nav__link t_vignette">
                            <i class="kt-nav__link-icon flaticon-truck"></i>
                            <span class="kt-nav__link-text">Vignette</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="impot-foncier-1" class="kt-nav__link t_if">
                            <i class="kt-nav__link-icon flaticon-buildings"></i>
                            <span class="kt-nav__link-text">Imp&ocirc;t foncier</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="impot-revenu-locatif" class="kt-nav__link t_irl">
                            <i class="kt-nav__link-icon flaticon-buildings"></i>
                            <span class="kt-nav__link-text">Imp&ocirc;t sur le revenu locatif</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="retenue-locative" class="kt-nav__link t_rl">
                            <i class="kt-nav__link-icon flaticon-home"></i>
                            <span class="kt-nav__link-text">La Retenue Locative</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="impot-concession-miniere" class="kt-nav__link t_icm">
                            <i class="kt-nav__link-icon flaticon-background"></i>
                            <span class="kt-nav__link-text">Imp&ocirc;t sur la concession mini&egrave;re</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
