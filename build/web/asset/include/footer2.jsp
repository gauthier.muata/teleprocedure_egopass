<footer class="footer">
    <div class="container-fluid">
        <div class="row text-muted">
            <div class="col-6 text-left">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <a class="text-muted" href="#" style="font-weight: bolder">Support</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="text-muted" href="#">&copy; 2021</a>
                    </li>
                </ul>
            </div>
            <div class="col-6 text-right">
                <p class="mb-0">
                    Designed by <a href="index.html" class="text-muted">Hologram</a>
                </p>
            </div>
        </div>
    </div>
</footer>