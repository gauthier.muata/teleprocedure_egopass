<div class="modal fade" id="nouvelleAdresse" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-scrollable" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">R&eacute;pertoire des adresses</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">�</span>
                </button>
            </div>
            <div class="modal-body m-3">
                <div class="form-group">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Saisir l'entit&eacute;..." id="inputLibelle">
                        <span class="input-group-append">
                            <button class="btn btn-primary" type="button" id="btnRechercherAdresse"><i class="fa fa-search"></i>&nbsp;Rechercher</button>
                        </span>
                    </div>
                </div>
                <img src="asset/img/loading.gif" class="center-block" alt="" id="loading_img" width="72px" height="72px" />
                <table class="table table-striped" style="width:100%" id="tableAdresse"></table>
            </div>
            <div class="modal-footer">
                <div class="form-group" style="margin-top: 12px">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Num&eacute;ro..." id="inputNumber">
                        <span class="input-group-append">
                            <button class="btn btn-primary" type="button" id="btnValiderAdresse"><i class="fa fa-check"></i>&nbsp;Valider</button>
                        </span>
                    </div>
                </div>
                <button type="button" class="btn btn-dark" data-dismiss="modal">Fermer</button>
            </div>
        </div>
    </div>
</div>