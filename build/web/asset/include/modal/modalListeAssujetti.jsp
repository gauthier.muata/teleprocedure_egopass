<div class="modal fade bd-example-modal-lg modal-liste-assujetti" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Rechercher un assujetti</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="recherche-assujetti">
                    <div class="form-row mb-2">
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="nom" name="nom" placeholder="Nom">
                        </div>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="prenom" name="prenom" placeholder="Pr�nom">
                        </div>
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="postnom" name="postnom" placeholder="Postnom">
                        </div>
                    </div>
                    <div class="form-row mb-2">
                        <div class="col-lg-4">
                            <input type="text" class="form-control" id="telephone" name="telephone" placeholder="Numero de Telephone">
                        </div>
                        <div class="col-lg-4">
                            <div class="input-group date">
                                <input type="text" class="form-control" id="dateNaissance" name="dateNaissance" placeholder="Date de Naissance">
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="la la-calendar"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <select name="modesearch" id="modesearch" class="form-control">
                                <option value=""> S�lectionner la personne </option>
                                <option value="1">Personne Physique</option>
                                <option value="2">Personne Morale</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <button class="btn btn-block btn-primary btngetAssujetti"> 
                            <i class="flaticon-search"></i> Rechercher
                        </button>
                    </div>
                </form>
                <div class="liste-assujetti" id="kt_datatable">
                    <div class="alert alert-outline-danger fade hidden" role="alert">
                        <div class="alert-icon"><i class="flaticon-questions-circular-button"></i></div>
                        <div class="alert-text"></div>
                        <div class="alert-close">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true"><i class="la la-close"></i></span>
                            </button>
                        </div>
                    </div>
                    <table class="table table-assujetti hidden">
                        <thead>
                            <tr>
                                <th>Nom Complet</th>
                                <th>E-Mail</th>
                                <th>Numero de Telephone</th>
                                <th>Type de Personne</th>
                                <th>Actions</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <!--
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
            -->
        </div>
    </div>
</div>
