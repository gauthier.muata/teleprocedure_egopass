<%-- 
    Document   : modalRechercheProprietaire
    Created on : 15 mars 2021, 16:42:00
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="modal fade bd-example-modal-lg rechercheAssujetti" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Propri&eacute;taires</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--label-right">
                    <div class="kt-portlet__corps">
                        <div class="form-group row">
                            <div class="col-lg-6">
                                <input type="text" class="form-control" id="input-value-searchAssujetti" 
                                placeholder="Saisir votre crit&egrave;re de recherche ici"/>
                            </div>
                            <div class="col-lg-3">
                                <button type="button" class="btn btn-block btn-primary btn-search-proprietaire"><i class="la la-search"></i>Rechercher</button>
                            </div>
                        </div>
                        <div class="kt-wizard-v3__content panier" data-ktwizard-type="step-content" data-ktwizard-state="current">
                            <div class="kt-form__section kt-form__section--first">
                                <table id="table_assujetti" class="table table-striped table-bordered" style="width:100%"></table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btnSelectAssujeti" style="display: none">
                    <i class="fa fa-check-circle"></i> &nbsp;S&eacute;lectionner
                </button>
                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
            </div>
        </div>
    </div>
</div>
