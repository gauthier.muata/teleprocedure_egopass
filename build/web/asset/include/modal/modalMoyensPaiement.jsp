<%-- 
    Document   : modalMoyensPaiement
    Created on : 17 f�v 2021, 11:46:53
    Author     : JUSLIN TSHIAMUA
--%>

<div class="modal fade bd-example-modal-lg modal-liste-assujetti" id="modalMoyenPaiement" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Moyens de paiement</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="idPaiement">
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
                <button type="button" class="btn btn-primary hidden">Payer</button>
            </div>

        </div>
    </div>
</div>
