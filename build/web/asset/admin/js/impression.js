/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var userData,
        username,
        urlId,
        montant,
        devise,
        id;
var input_numeroCompte = $('#input_numeroCompte'),
        input_montant = $('#input_montant'),
        inputPin = $('#inputPin'),
        btnPayerMpata = $('#btnPayerMpata');

$(function () {

    userData = JSON.parse(getUserData());

    if (userData == null) {
        window.location = 'error-404';
    }

    urlId = getUrlParameter('id');

    if (!isUndefined(urlId)) {

        setTimeout(function () {
            getDeclarationData(urlId);
        }, 500);

    }

    loadPayementData();

    $('.btn-paiement-facture').click(function (e) {
        $('#modalMoyenPaiement').modal('show');
    });

    btnPayerMpata.click(function (e) {
        if (devise === 'CDF') {
            montant = parseFloat(montant) / TAUX;
        }
        setMpataAuthentification();
    });
});

function getDeclarationData(code) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });
    $.ajax({
        url: baseUrl + 'v2/declaration/' + code,
        crossDomain: true,
        method: 'GET',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {
        KTApp.unblockPage();
        data = JSON.parse(JSON.stringify(data));

        if (data.numero === code) {
            setDataDeclaration(data);
        } else {
            showResponseError();
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
        KTApp.unblockPage();
    });
}

function setDataDeclaration(decData) {
    jQuery('#qr_code').qrcode({
        text: baseUrl + "/teledeclaration/declaration-impression?id=" + urlId
    });
    var bien = decData.details[0];
    $('.code').html(decData.numero);
    $('.titre').html(bien.periode);
    montant = bien.montant;
    devise = bien.devise;
    $('.montant').html(bien.montant + ' ' + devise);
    $('.total').html(bien.montant + ' ' + devise);
//    $('.taux').html(decData.taux);
    $('.date').html(decData.dateRetrait);
    $('.dec').html(decData.articleGenerique);
    $('#user_prenom').html(decData.requerant);
    $('#userId').html(decData.requerant);
    $('#user_adresse').html(decData.adresseRequerant);
    $('#user_phone').html(decData.telephone);
}

//function loadPayementData() {
//
//    $.ajax({
//        type: 'POST',
//        url: 'asset/admin/js/methodesPaiement.json',
//        dataType: 'JSON',
//        async: false,
//        beforeSend: function () {
//
//        }, data: {
//        },
//        success: function (data) {
//            paiement(data);
//        },
//        error: function () {
//        }
//    });
//}

function paiement(mode_paiement) {
    var data = '';
    data += '<div class="alert alert-warning" style="font-size:14px; color: #5867dd">';
    data += '<strong id="notificationSelectedMethod">Veuillez choisir le mode de paiement qui vous convient.</strong>';
    data += '</div>';

    data += '<div class="paymentWrap" id="divModePayment">';
    data += '<div class="btn-group paymentBtnGroup btn-group-justified" data-toggle="buttons">';

    for (var i = 0; i < mode_paiement.length; i++) {

        if (!mode_paiement[i].visible) {
            continue;
        }

        data += '<label class="btn paymentMethod">';
        data += '<div id="selected_' + mode_paiement[i].id + '" class="method cash" value="' + mode_paiement[i].id + '" onclick="selectPayementmethod(' + mode_paiement[i].id + ')" class="method cash">' + mode_paiement[i].name + '<br/><img width="50px" height="50px" src="' + mode_paiement[i].icon + '" /></div>';
        data += '<input type="radio" id="method" value="' + mode_paiement[i].id + '"></label>';
    }

    $('#idPaiement').html(data);
}

function selectPayementmethod(id) {

    urlId = getCodeDeclaration();
    montant = getTotalPayDeclaration();
//    montant = 1;
    switch (id) {
        case 100:
            $('#modalMoyenPaiement').modal('hide');
            pay(urlId, montant);
            break;
        case 200:
            input_montant.val(montant + ' ' + getDevisePayDeclaration());
            $('#modalMoyenPaiement').modal('hide');
            $('#modalPaiementMpata').modal('show');
            break;
        case 300:
            $('#modalMoyenPaiement').modal('hide');
            pay(urlId, montant);
            break;
        case 400:
            $('#modalMoyenPaiement').modal('hide');
            pay(urlId, montant);
            break;
        case 500:
            $('#modalMoyenPaiement').modal('hide');
            pay(urlId, montant);
            break;
    }
}

function setMpataAuthentification() {

    if (input_numeroCompte.val() === '') {
        Swal.fire({
            title: "Authentification",
            html: 'Veuillez saisir le num&eacute;ro de t&eacute;l&eacute;phone de votre compte s\'il vous plait',
            type: "warning",
            confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
        });
        return;
    }
    if (inputPin.val() === '') {
        Swal.fire({
            title: "Authentification",
            html: 'Veuillez saisir le code PIN de votre compte s\'il vous plait',
            type: "warning",
            confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
        });
        return;
    }

    $('#loading_img').show();

    var object = {
        "numero": input_numeroCompte.val(),
        "pin": inputPin.val()
    };

    $.ajax({
        url: baseUrl + 'v1/authenticationMpata',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {

        $('#loading_img').hide();

        if (data == '-1') {
            showErrorMessageOnRequest();
            return;
        }
        if (data == '0') {
            var message = 'Impossible de faire cette transaction.\nVérifiez les donn\351es de votre compte et r\351essayer';
            Swal.fire({
                title: "Paiement eMpata",
                html: message,
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            return;
        } else {
            Swal.fire({
                title: "Paiement eMpata",
                text: "Vous \352tes sur le point de payer pour cette d\351claration. Etes-vous s\373r de faire cette transaction ?",
                type: "info",
                showCancelButton: !0,
                confirmButtonText: "Confirmer",
                cancelButtonText: "Annuler"
            }).then(function (isConfirm) {
                if (isConfirm.value) {
                    setMpataTransaction(data.token);
                }
            });
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
        KTApp.unblockPage();
    });
}

function setMpataTransaction(token) {

    montant = 1;

    var object = {
        'adminId': 0,
        'amount': montant,
        'currency': devise,
        'date': '2020-11-04T11:13:36.805Z',
        'description': 'Paiement de la d\351claration n°' + urlId,
        'feesIn': true,
        'operationType': 3,
        'sender': input_numeroCompte.val(),
        'token': token
    };

    $.ajax({
        url: baseUrl + 'v1/transfertMpata',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {

        $('#loading_img').hide();

        if (data == '-1') {
            showErrorMessageOnRequest();
            return;
        }

        if (data[0].status == 'SUCCESS') {
            $('#modalPaiementMpata').modal('hide');
            var message = 'Le transfert d\'argent effectu\351 avec succès.';
            Swal.fire({
                title: "Paiement eMpata",
                html: message,
                type: "info",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            setTimeout(function () {
                window.location = 'dashboard';
            }, 2e3);
        } else if (data[0].status == '400') {
            var message = 'D\351sol\351! Votre solde est insuffisant pour effectuer cette op\351ration.';
            Swal.fire({
                title: "Paiement eMpata",
                html: message,
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
        } else {
            var message = 'D\351sol\351! Votre transaction a \351chou\351. Veuillez contacter l\'administrateur.';
            Swal.fire({
                title: "Transfert",
                html: message,
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
        }
    });
}