var host = window.location.origin;

function pay(code, montant) {

    var dataPayement = {
        'PayType': 'MaxiCash',
        'Amount': montant * 100,
        'Currency': 'MaxiDollar',
        'MerchantID': '9c56057bcd50463ab8cb7843678037b3',
        'MerchantPassword': '780f41a1f2a94221983f8493a740078a',
        'Reference': code,
        'Language': 'fr',
        'Telephone': '00243821416216',
        'Email': 'moussa.toure@hologram.cd',
        'accepturl': host + '/teledeclaration/paiement-maxicash',
        'cancelurl': host + '/teledeclaration/paiement-maxicash_failed',
        'declineurl': host + '/teledeclaration/paiement-maxicash_failed',
        'notifyurl': host + '/teledeclaration/paiement-maxicash_failed'
    };

    openPopupPage(dataPayement);
}

function openPopupPage(data) {
    var param = JSON.parse(JSON.stringify(data));

    OpenWindowWithPost("https://api-testbed.maxicashapp.com/PayEntryPost", param);
}

function OpenWindowWithPost(url, params) {
    var form = document.createElement("form");
    form.setAttribute("method", "post");
    form.setAttribute("action", url);
    form.setAttribute("target", name);
    for (var i in params) {
        if (params.hasOwnProperty(i)) {
            var input = document.createElement('input');
            input.type = 'hidden';
            input.name = i;
            input.value = params[i];
            form.appendChild(input);
        }
    }
    document.body.appendChild(form);
    form.submit();
    document.body.removeChild(form);
}

function getResponsePaiement() {

    var reference = getUrlParameter('reference');

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });
    $.ajax({
        url: baseUrl + 'bank/v1/payment',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {
        KTApp.unblockPage();
        data = JSON.parse(JSON.stringify(data));

        if (data.numero === code) {
            setDataDeclaration(data);
        } else {
            showResponseError();
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
        KTApp.unblockPage();
    });

}
