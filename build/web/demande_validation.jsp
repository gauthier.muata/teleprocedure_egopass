<%-- 
    Document   : login
    Created on : 28 sept. 2020, 10:23:21
    Author     : WILLY
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>E-Recettes T&eacute;l&eacute;-proc&eacute;dure</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicons -->
        <link href="asset/img/favicon.png" rel="icon">
        <link href="asset/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="asset/css/family.css" rel="stylesheet" type="text/css"/>

        <!-- Bootstrap CSS File -->
        
        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <!-- Libraries CSS Files -->
        <link href="asset/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/prettyphoto/css/prettyphoto.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/hover/hoverex-all.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/jetmenu/jetmenu.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/owl-carousel/owl-carousel.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/intl-tel-input/build/css/intlTelInput.css" rel="stylesheet" type="text/css"/>

        <!-- Main Stylesheet File -->
        <link href="asset/front/style.css" rel="stylesheet" type="text/css"/>
        <link href="asset/front/blue.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>

        <%@include file="asset/include/header.html" %>

        <!-- end post-wrapper-top -->

        <section class="section1">
            <div class="container clearfix">
                <div class="content col-lg-12 col-md-12 col-sm-12 clearfix">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <h4 class="title">
                            <span>Demande d'inscription &agrave; la plateforme T&eacute;l&eacute;-proc&eacute;dure</span>
                        </h4>
                        <p>Vous êtes d&eacute;j&agrave; un assujetti de la r&eacute;gie. Ceci vous donne la possibilit&eacute; de faire une demande d'inscription à la plateforme de la <span style="color: blue">T&eacute;l&eacute;-proc&eacute;dure</span>.</p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <h4 class="title">
                            <span>Veuillez vous rechercher par nom ou raison sociale</span>
                        </h4>

                        <form id="loginform" name="loginform">
                            <div class="form-group div_nom">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                    <input type="text" value="" class="form-control" name="input_nom" 
                                           placeholder="Entrez votre nom ou raison sociale" id="input_nom">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="list-group assujetti-set">
                                </div>
                            </div>

                            <div class="form-group alert alert-danger alert-demande hidden"></div>

                            <div class="form-group div_mail hidden">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-envelope"></i></span>
                                    <input type="email" class="form-control" name="input_mail" 
                                           placeholder="Entrez votre adresse mail" id="input_mail">
                                </div>
                            </div>

                            <div class="form-group div_phone hidden">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-earphone"></i></span>
                                    <input type="tel" value="" class="form-control" name="T&eacute;l&eacute;phone" 
                                           placeholder="Entrez votre num&eacute;ro de t&eacute;l&eacute;phone" id="input_phone_number">
                                </div>
                            </div>

                            <div class="form-group btn_search">
                                <button type="button" id="btnRecherche" class="button"> <i class="fa fa-search"></i>Rechercher</button>                               
                            </div>
                            <div class="form-group btn_demande_validation hidden">
                                <button type="button" id="btnDemandeValidation" class="button"> <i class="glyphicon glyphicon-ok"></i>Envoyer votre demande</button>                               
                            </div>

                            <div class="alert alert-danger error-alert" role="alert" style="display: none;">
                            </div>
                        </form>
                    </div>
                    <!-- end login -->
                </div>
                <!-- end content -->
            </div>
            <!-- end container -->
        </section>
        <!-- end section -->

        <%@include file="asset/include/footer.html" %>

    </body>

    <script src="asset/lib/jquery/jquery.min.js"></script>
    <script src="asset/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="asset/js/validate.js"></script>
    <script src="asset/js/prettyphoto.js"></script>
    <script src="asset/js/isotope.min.js"></script>
    <script src="asset/js/hoverdir.js"></script>
    <script src="asset/js/hoverex.min.js"></script>
    <script src="asset/js/unveil-effects.js"></script>
    <script src="asset/js/owl-carousel.js"></script>
    <script src="asset/js/jetmenu.js"></script>
    <script src="asset/js/animate-enhanced.min.js"></script>
    <script src="asset/js/jigowatt.js"></script>
    <script src="asset/js/easypiechart.min.js"></script>
    <script src="asset/alertify/alertify.min.js" type="text/javascript"></script>
    <script src="asset/js/jquery.blockUI.js"></script>
    <script src="asset/js/utils.js"></script>
    <script src="asset/js/core.js"></script>
    <script src="asset/js/main.js"></script>
    <script src="asset/js/demande_validation.js"></script>

</html>
