<%-- 
    Document   : eMpataPaiement
    Created on : 26 mai 2021, 10:41:42
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>R&eacute;pertoire des commandes des vouchers</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--begin::Fonts -->
        <link href="asset/css/font.css" rel="stylesheet" type="text/css"/>

        <!--begin::Global Theme Styles(used by all pages) -->
        <link href="asset/admin/css/plugins.bundle.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/style.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->

        <link href="asset/admin/css/light.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/light-menu.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy-aside.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/custom.css" rel="stylesheet" type="text/css">

        <style>
            body {
                background: #ddd;
                min-height: 100vh;
                vertical-align: middle;
                display: flex
            }

            .card {
                margin: auto;
                width: 600px;
                padding: 3rem 3.5rem;
                box-shadow: 0 6px 20px 0 rgba(0, 0, 0, 0.19)
            }

            .mt-50 {
                margin-top: 50px
            }

            .mb-50 {
                margin-bottom: 50px
            }

            @media(max-width:767px) {
                .card {
                    width: 90%;
                    padding: 1.5rem
                }
            }

            @media(height:1366px) {
                .card {
                    width: 90%;
                    padding: 8vh
                }
            }

            .card-title {
                font-weight: 700;
                font-size: 2.5em
            }

            .nav {
                display: flex
            }

            .nav ul {
                list-style-type: none;
                display: flex;
                padding-inline-start: unset;
                margin-bottom: 6vh
            }

            .nav li {
                padding: 1rem
            }

            .nav li a {
                color: black;
                text-decoration: none
            }

            .active {
                border-bottom: 2px solid #646c9a;
                font-weight: bold
            }

            input {
                border: none;
                outline: none;
                font-size: 1rem;
                font-weight: 600;
                color: #000;
                width: 100%;
                min-width: unset;
                background-color: transparent;
                border-color: transparent;
                margin: 0
            }

            form a {
                color: grey;
                text-decoration: none;
                font-size: 0.87rem;
                font-weight: bold
            }

            form a:hover {
                color: grey;
                text-decoration: none
            }

            form .row {
                margin: 0;
                overflow: hidden
            }

            form .row-1 {
                border: 1px solid rgba(0, 0, 0, 0.137);
                padding: 0.5rem;
                outline: none;
                width: 100%;
                min-width: unset;
                border-radius: 5px;
                background-color: rgba(221, 228, 236, 0.301);
                border-color: rgba(221, 228, 236, 0.459);
                margin: 2vh 0;
                overflow: hidden
            }

            form .row-2 {
                border: none;
                outline: none;
                background-color: transparent;
                margin: 0;
                padding: 0 0.8rem
            }

            form .row .row-2 {
                border: none;
                outline: none;
                background-color: transparent;
                margin: 0;
                padding: 0 0.8rem
            }

            form .row .col-2,
            .col-7,
            .col-3 {
                display: flex;
                align-items: center;
                text-align: center;
                padding: 0 1vh
            }

            form .row .col-2 {
                padding-right: 0
            }

            #card-header {
                font-weight: bold;
                font-size: 0.9rem
            }

            #card-inner {
                font-size: 0.7rem;
                color: gray
            }

            .three .col-7 {
                padding-left: 0
            }

            .three {
                overflow: hidden;
                justify-content: space-between
            }

            .three .col-2 {
                border: 1px solid rgba(0, 0, 0, 0.137);
                padding: 0.5rem;
                outline: none;
                width: 100%;
                min-width: unset;
                border-radius: 5px;
                background-color: rgba(221, 228, 236, 0.301);
                border-color: rgba(221, 228, 236, 0.459);
                margin: 2vh 0;
                width: fit-content;
                overflow: hidden
            }

            .three .col-2 input {
                font-size: 0.7rem;
                margin-left: 1vh
            }

            .btn {
                width: 100%;
                /*background-color: rgb(65, 202, 127);
                border-color: rgb(65, 202, 127);*/
                color: white;
                justify-content: center;
                padding: 2vh 0;
                margin-top: 3vh
            }

            .btn:focus {
                box-shadow: none;
                outline: none;
                box-shadow: none;
                color: white;
                -webkit-box-shadow: none;
                -webkit-user-select: none;
                transition: none
            }

            .btn:hover {
                color: white
            }

            input:focus::-webkit-input-placeholder {
                color: transparent
            }

            input:focus:-moz-placeholder {
                color: transparent
            }

            input:focus::-moz-placeholder {
                color: transparent
            }

            input:focus:-ms-input-placeholder {
                color: transparent
            }
            .color-reference{
                color: #F44336
            }
        </style>

    </head>

    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed" style="">
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed">
            <div class="kt-header-mobile__logo">
                <h4 class="text-light">HOLOGRAM</h4> 
            </div>
            <div class="kt-header-mobile__toolbar">

                <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
            </div>
        </div>

        <div class="kt-grid kt-grid--hor kt-grid--root">

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <button class="kt-aside-close " id="kt_aside_close_btn">
                    <i class="la la-close"></i>
                </button>

                <%@include file="asset/include/menu.html" %>

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    <%@include file="asset/include/headerV2.html" %>


                    <div class="row">
                        <div class="col-lg-12">

                            <div class="card mt-50 mb-50 paypal">
                                <div class="card-title mx-auto"> <img src="asset/admin/img/mpata.png" style="width: 72px; height: 72px;" alt="mpata">eMpata </div>
                                <div class="nav">
                                    <ul class="mx-auto">
                                        <li class="active"><span>PAIEMENT DE LA COMMANDE (Ref. </span><span class="color-reference" id="reference"></span>)</li>
                                    </ul>
                                </div>
                                <form>
                                    <span id="card-header">Numéro du compte:</span>
                                    <div class="row row-1">
                                        <div class="col-2"><img class="img-fluid" src="asset/admin/img/mpata.png" width="40px" height="40px" /></div>
                                        <div class="col-7"> <input type="text" id="input_numeroCompte" placeholder="**** **** **** 3193"> </div>
                                        <div class="col-3 d-flex justify-content-center"><i class="fa fa-eraser" id="delCompte" style="cursor: pointer"></i></div>
                                    </div>
                                    <span id="card-header">Code PIN:</span>
                                    <div class="row-1">
                                        <div class="row row-2"> <span id="card-inner">Votre PIN d'accès au compte</span> </div>
                                        <div class="row row-2"> <input type="password" id="inputPin" placeholder="******"> </div>
                                    </div>
                                    <span id="card-header">Montant à payer:</span>
                                    <div class="row-1">
                                        <div class="row row-2"> <input type="text" id="input_montant" placeholder="0" disabled> </div>
                                    </div>
                                    <button type="button" class="btn btn-primary d-flex mx-auto" id="btnPayerMpata"><b>Payer</b></button>
                                </form>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>

        <%@include file="asset/include/footerV2.html" %>

        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "metal": "#c4c5d6",
                        "light": "#ffffff",
                        "accent": "#00c5dc",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995",
                        "focus": "#9816f4"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>
        <script src="asset/admin/js/plugins.bundle.js" type="text/javascript"></script>
        <script src="asset/admin/js/scripts.bundle.js" type="text/javascript"></script>

        <script src="asset/admin/js/jquery.dataTables.min.js" type="text/javascript"></script>

        <script src="asset/js/jquery.blockUI.js"></script>
        <script src="asset/alertify/alertify.min.js" type="text/javascript"></script>

        <script src="asset/js/core.js" type="text/javascript"></script>
        <script src="asset/js/utils.js" type="text/javascript"></script>
        <script src="asset/js/payment-eMpata.js" type="text/javascript"></script>

    </body>
</html>