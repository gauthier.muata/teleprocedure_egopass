<%-- 
    Document   : dashboard
    Created on : 13 avr. 2021, 11:19:32
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tableau de bord
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--begin::Fonts -->
        <link href="asset/css/font.css" rel="stylesheet" type="text/css"/>
        <!--end::Fonts -->

        <!--begin::Page Vendors Styles(used by this page) -->
        <link href="asset/admin/css/fullcalendar.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Page Vendors Styles -->

        <!--begin::Global Theme Styles(used by all pages) -->
        <link href="asset/admin/css/plugins.bundle.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/style.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->

        <link href="asset/admin/css/light.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/light-menu.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy-aside.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/custom.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="asset/plugins/fancybox/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css">
        <!--end::Layout Skins -->

        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            /* Chart.js */
            /*
    * DOM element rendering detection
    * https://davidwalsh.name/detect-node-insertion
    */

            @keyframes chartjs-render-animation {
                from {
                    opacity: 0.99;
                }
                to {
                    opacity: 1;
                }
            }

            .chartjs-render-monitor {
                animation: chartjs-render-animation 0.001s;
            }
            /*
    * DOM element resizing detection
    * https://github.com/marcj/css-element-queries
    */

            .chartjs-size-monitor,
            .chartjs-size-monitor-expand,
            .chartjs-size-monitor-shrink {
                position: absolute;
                direction: ltr;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                overflow: hidden;
                pointer-events: none;
                visibility: hidden;
                z-index: -1;
            }

            .chartjs-size-monitor-expand > div {
                position: absolute;
                width: 1000000px;
                height: 1000000px;
                left: 0;
                top: 0;
            }

            .chartjs-size-monitor-shrink > div {
                position: absolute;
                width: 200%;
                height: 200%;
                left: 0;
                top: 0;
            }
        </style>
    </head>
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed" style="">
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                <h4 class="text-light">eGoPass</h4> 
            </div>
            <div class="kt-header-mobile__toolbar">

                <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
            </div>
        </div>

        <div class="kt-grid kt-grid--hor kt-grid--root">

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>

                <%@include file="asset/include/menu.html" %>

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    <%@include file="asset/include/headerV2.html" %>

                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid "></div>
                        </div>

                        <!-- Begin Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="kt-portlet" id="kt_blockui_1_content">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title"><i class="flaticon2-chart"></i>&nbsp;&nbsp;Tableau de bord - eGoPass</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <form class="kt-form kt-form--label-right" id="form-dashbord-peage">
                                        <div class="kt-portlet__corps">
                                            <!--begin::Row-->
                                            <div class="row">
                                                <div class="col-xl-6">
                                                    <!--begin::Base Table Widget 1-->
                                                    <div class="card card-custom card-stretch gutter-b">
                                                        <!--begin::Header-->
                                                        <div class="card-header border-0 pt-5">
                                                            <h3 class="card-title align-items-start flex-column">
                                                                <span class="card-label font-weight-bolder text-dark">Tableau de bord - </span>
                                                                <span class="text-muted mt-3 font-weight-bold font-size-sm">GoPass</span>
                                                            </h3>
                                                            <div class="card-toolbar">
                                                                <ul class="nav nav-pills nav-pills-sm nav-dark-75">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link py-2 px-4 btn_filtre" data-toggle="tab" href="" onclick="hideDateDiv(0)">D&eacute;finir</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link py-2 px-4" data-toggle="tab" href="" onclick="hideDateDiv(30)">Ce mois</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link py-2 px-4" data-toggle="tab" href="" onclick="hideDateDiv(7)">Cette semaine</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link py-2 px-4 active" data-toggle="tab" href="" onclick="hideDateDiv(1)">Aujourd'hui</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                            <div class="card-toolbar mt-n3 div_date hidden">
                                                                <div class="form-group row">
                                                                    <label for="dateDebut" class="col-lg-1 col-form-label">Du</label>
                                                                    <div class="col-lg-4 input-group date">
                                                                        <input type="date" class="form-control" id="dateDebut">
                                                                    </div>
                                                                    <label for="dateFin" class="col-lg-1 col-form-label">Au</label>
                                                                    <div class="col-lg-4 input-group date">
                                                                        <input type="date" class="form-control" id="dateFin">
                                                                    </div>
                                                                    <div class="col-lg-2">
                                                                        <button class="form-control btn-outline-primary btnFilter" type="button" name="">Filtrer</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!--end::Header-->
                                                        <!--begin::Body-->
                                                        <div class="card-body pt-2 pb-0 mt-n3">
                                                            <div class="tab-content mt-5" id="myTabTables1">
                                                                <!--begin::Tap pane-->
                                                                <div class="tab-pane fade show active" id="kt_tab_pane_1_3" role="tabpanel" aria-labelledby="kt_tab_pane_1_3">

                                                                    <!--begin::Table-->
                                                                    <div class="table-responsive">
                                                                        <table class="table table-borderless table-vertical-center">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="p-0 w-50px"></th>
                                                                                    <th class="p-0 min-w-200px"></th>
                                                                                    <th class="p-0 min-w-100px"></th>
                                                                                    <th class="p-0 min-w-40px"></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td class="pl-0 py-5"></td>
                                                                                    <td class="pl-0">
                                                                                        <label class="text-danger text-hover-primary mb-1 font-size-lg font-weight-bold">GOPASS</label>
                                                                                        <span class="text-muted font-weight-bold d-block">Total go-pass command&eacute;s</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="d-flex flex-column w-100 mr-2">
                                                                                            <div class="d-flex align-items-center justify-content-between mb-2">
                                                                                                <span class="text-muted mr-2 font-size-sm font-weight-bold" id="totalGopass">0</span>
                                                                                                <span class="text-muted font-size-sm font-weight-bold"></span>
                                                                                            </div>
                                                                                            <div class="progress progress-xs w-100" id="progress_vouchers"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="text-right pr-0">
                                                                                        <a href="" class="btn btn-icon btn-light btn-sm">
                                                                                            <span class="svg-icon svg-icon-md svg-icon-success">
                                                                                            </span>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr
                                                                                <tr>
                                                                                    <th class="pl-0 py-5"></th>
                                                                                    <td class="pl-0">
                                                                                        <hr style="margin-top:  0"/>
                                                                                        <a href="" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">GOPASS</a>
                                                                                        <span class="text-muted font-weight-bold d-block">Total go-pass blancs</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <hr style="margin-top:  0"/>
                                                                                        <div class="d-flex flex-column w-100 mr-2">
                                                                                            <div class="d-flex align-items-center justify-content-between mb-2">
                                                                                                <span class="text-muted mr-2 font-size-sm font-weight-bold" id="totalGopassBlancs">0</span>
                                                                                                <span class="text-muted font-size-sm font-weight-bold">Progression</span>
                                                                                            </div>
                                                                                            <div class="progress progress-xs w-100" id="progress_vouchersUsed"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="text-right pr-0">
                                                                                        <a href="" class="btn btn-icon btn-light btn-sm">
                                                                                            <span class="svg-icon svg-icon-md svg-icon-success">
                                                                                            </span>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="pl-0 py-5"></th>
                                                                                    <td class="pl-0">
                                                                                        <a href="" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">GO-PASS</a>
                                                                                        <span class="text-muted font-weight-bold d-block">Total go-pass activ&eacute;s</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="d-flex flex-column w-100 mr-2">
                                                                                            <div class="d-flex align-items-center justify-content-between mb-2">
                                                                                                <span class="text-muted mr-2 font-size-sm font-weight-bold" id="totalGopassActives">0</span>
                                                                                                <span class="text-muted font-size-sm font-weight-bold">Progression</span>
                                                                                            </div>
                                                                                            <div class="progress progress-xs w-100" id="progress_vouchersUnused"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="text-right pr-0">
                                                                                        <a href="" class="btn btn-icon btn-light btn-sm">
                                                                                            <span class="svg-icon svg-icon-md svg-icon-success">
                                                                                            </span>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="pl-0 py-5"></th>
                                                                                    <td class="pl-0">
                                                                                        <a href="" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">GOPASS</a>
                                                                                        <span class="text-muted font-weight-bold d-block">Total gopass utilis&eacute;s</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="d-flex flex-column w-100 mr-2">
                                                                                            <div class="d-flex align-items-center justify-content-between mb-2">
                                                                                                <span class="text-muted mr-2 font-size-sm font-weight-bold" id="totalGopassUtilises">0</span>
                                                                                                <span class="text-muted font-size-sm font-weight-bold">Progression</span>
                                                                                            </div>
                                                                                            <div class="progress progress-xs w-100" id="progress_vouchersUnused"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="text-right pr-0">
                                                                                        <a href="" class="btn btn-icon btn-light btn-sm">
                                                                                            <span class="svg-icon svg-icon-md svg-icon-success">
                                                                                            </span>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!--end::Table-->
                                                                </div>
                                                                <!--end::Tap pane-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Base Table Widget 1-->
                                                </div>
                                                <div class="col-xl-6">
                                                    <!--begin::Base Table Widget 1-->
                                                    <div class="card card-custom card-stretch gutter-b">
                                                        <!--begin::Body-->
                                                        <div class="card-body pt-2 pb-0 mt-n3">
                                                            <div class="tab-content mt-5" id="myTabTables1">
                                                                <!--begin::Tap pane-->
                                                                <div class="tab-pane fade show active" id="kt_tab_pane_1_3" role="tabpanel" aria-labelledby="kt_tab_pane_1_3">
                                                                    <!--begin::Table-->
                                                                    <div class="table-responsive">
                                                                        <table class="table table-borderless table-vertical-center">
                                                                            <thead>
                                                                                <tr>
                                                                                    <th class="p-0 w-50px"></th>
                                                                                    <th class="p-0 min-w-200px"></th>
                                                                                    <th class="p-0 min-w-100px"></th>
                                                                                    <th class="p-0 min-w-40px"></th>
                                                                                </tr>
                                                                            </thead>
                                                                            <tbody>
                                                                                <tr class="hidden">
                                                                                    <td class="pl-0 py-5"></td>
                                                                                    <td class="pl-0">
                                                                                        <label class="text-danger text-hover-primary mb-1 font-size-lg font-weight-bold">CARTES GO-PASS</label>
                                                                                        <span class="text-muted font-weight-bold d-block">Total cartes command&eacute;es</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="d-flex flex-column w-100 mr-2">
                                                                                            <div class="d-flex align-items-center justify-content-between mb-2">
                                                                                                <span class="text-muted mr-2 font-size-sm font-weight-bold" id="totalCommandeCarte">0</span>
                                                                                                <span class="text-muted font-size-sm font-weight-bold"></span>
                                                                                            </div>
                                                                                            <div class="progress progress-xs w-100" id="progress_vouchers"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="text-right pr-0">
                                                                                        <a href="" class="btn btn-icon btn-light btn-sm">
                                                                                            <span class="svg-icon svg-icon-md svg-icon-success">
                                                                                            </span>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="pl-0 py-5"></th>
                                                                                    <td class="pl-0">
                                                                                        <label class="text-danger text-hover-primary mb-1 font-size-lg font-weight-bold">CARTES GO-PASS</label>
                                                                                        <span class="text-muted font-weight-bold d-block">Total cartes valid&eacute;es</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="d-flex flex-column w-100 mr-2">
                                                                                            <div class="d-flex align-items-center justify-content-between mb-2">
                                                                                                <span class="text-muted mr-2 font-size-sm font-weight-bold" id="totalCarteGopass">0</span>
                                                                                                <span class="text-muted font-size-sm font-weight-bold"></span>
                                                                                            </div>
                                                                                            <div class="progress progress-xs w-100" id="progress_vouchers"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="text-right pr-0">
                                                                                        <a href="" class="btn btn-icon btn-light btn-sm">
                                                                                            <span class="svg-icon svg-icon-md svg-icon-success">
                                                                                            </span>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <th class="pl-0 py-5"></th>
                                                                                    <td class="pl-0">
                                                                                        <hr style="margin-top:  0"/>
                                                                                        <a href="" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">CARTES</a>
                                                                                        <span class="text-muted font-weight-bold d-block">Total cartes affect&eacute;es</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <hr style="margin-top: 0"/>
                                                                                        <div class="d-flex flex-column w-100 mr-2">
                                                                                            <div class="d-flex align-items-center justify-content-between mb-2">
                                                                                                <span class="text-muted mr-2 font-size-sm font-weight-bold" id="totalCartesAffected">0</span>
                                                                                                <span class="text-muted font-size-sm font-weight-bold">Progression</span>
                                                                                            </div>
                                                                                            <div class="progress progress-xs w-100" id="progress_cartesAffected"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="text-right pr-0">
                                                                                        <a href="" class="btn btn-icon btn-light btn-sm">
                                                                                            <span class="svg-icon svg-icon-md svg-icon-success">
                                                                                            </span>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <th class="pl-0 py-5"></th>
                                                                                    <td class="pl-0">
                                                                                        <a href="" class="text-dark font-weight-bolder text-hover-primary mb-1 font-size-lg">CARTES</a>
                                                                                        <span class="text-muted font-weight-bold d-block">Total cartes non affect&eacute;es</span>
                                                                                    </td>
                                                                                    <td>
                                                                                        <div class="d-flex flex-column w-100 mr-2">
                                                                                            <div class="d-flex align-items-center justify-content-between mb-2">
                                                                                                <span class="text-muted mr-2 font-size-sm font-weight-bold" id="totalCartesNotAffected">0</span>
                                                                                                <span class="text-muted font-size-sm font-weight-bold">Progression</span>
                                                                                            </div>
                                                                                            <div class="progress progress-xs w-100" id="progress_cartesNotAffected"></div>
                                                                                        </div>
                                                                                    </td>
                                                                                    <td class="text-right pr-0">
                                                                                        <a href="" class="btn btn-icon btn-light btn-sm">
                                                                                            <span class="svg-icon svg-icon-md svg-icon-success">
                                                                                            </span>
                                                                                        </a>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                    <!--end::Table-->
                                                                </div>
                                                                <!--end::Tap pane-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--end::Base Table Widget 1-->
                                                </div>
                                            </div>
                                            <!--end::Row-->
                                            <!--begin::Row-->
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Content -->
            </div>

        </div>

        <%@include file="asset/include/footerV2.html" %>

        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "metal": "#c4c5d6",
                        "light": "#ffffff",
                        "accent": "#00c5dc",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995",
                        "focus": "#9816f4"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>
        <script src="asset/admin/js/plugins.bundle.js" type="text/javascript"></script>
        <script src="asset/admin/js/scripts.bundle.js" type="text/javascript"></script>

        <!--end::Page Vendors -->
        <script src="asset/admin/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="asset/admin/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

        <script src="asset/plugins/fancybox/js/jquery.fancybox.min.js" type="text/javascript"></script>

        <script src="asset/js/jquery.blockUI.js"></script>
        <script src="asset/alertify/alertify.min.js" type="text/javascript"></script>

        <script src="asset/js/core.js" type="text/javascript"></script>
        <script src="asset/js/utils.js" type="text/javascript"></script>
        <script src="asset/js/dashboard-gopass.js" type="text/javascript"></script>

    </body>

</html>
