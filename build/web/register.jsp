<%-- 
    Document   : register
    Created on : 05 aout 2021, 10:23:49
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>E-Recettes T&eacute;l&eacute;-proc&eacute;dure</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <link href="asset/front/style.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/intl-tel-input/build/css/intlTelInput.css" rel="stylesheet" type="text/css"/>
        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>
        <link href="asset/css/classic.css" rel="stylesheet" type="text/css"/>

        <style>
            body {
                background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);
            }
            .dark_1{
                background-color: #000
            }
        </style>

    </head>
    <body>

        <%@include file="asset/include/header.html" %>

        <main class="main d-flex w-100">
            <div class="container d-flex flex-column">
                <div class="row h-10">
                    <div class="col-sm-10 col-md-8 col-lg-8 mx-auto d-table h-100">
                        <div class="d-table-cell align-middle">

                            <div class="text-center mt-4">
                                <h1 class="h2 text-register-dark">Inscrivez-vous</h1>
                            </div>

                            <div class="card-body">
                                <form id="validation-form" novalidate="novalidate">
                                    <div role="alert" class="form-register">
                                        <button type="button" class="close" data-dismiss="alert">
                                        </button>
                                        <div class="alert-message" style="padding-bottom: 2rem">
                                            <div>
                                                <div class="form-group">
                                                    <div class="alert alert-dark" role="alert">
                                                        <div class="alert-icon">
                                                            <i data-feather="grid"></i>
                                                        </div>
                                                        <div class="alert-message" style="padding: .95rem">
                                                            Formulaire d'inscription
                                                            <img src="asset/img/loading.gif" class="center-block hidden" alt="" id="loading2_img" width="32x" height="32px" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="cmbTypePersonne" class="col-lg-3 col-form-label text-right">Type Personne</label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" name="Type personne" id="cmbTypePersonne">
                                                            <option value="">S&eacute;lectionner le type de personne</option>
                                                        </select>
                                                        <small>
                                                            <span class="type-personne-error hidden text-danger">
                                                                Veuillez s&eacute;lectioner le type personne
                                                            </span>
                                                        </small>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputNif" class="col-lg-3 col-form-label lblNIf text-right"></label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" name="Nif" placeholder="Veuilez saisir votre nif" id="inputNif">
                                                        <small>
                                                            <span class="nif-error hidden text-danger">
                                                                Veuillez saisir le nif
                                                            </span>
                                                        </small>
                                                    </div>
                                                </div>
                                                <div class="morale-field hidden">
                                                    <div class="form-group row">
                                                        <label for="inputIdNat" class="col-lg-3 col-form-label text-right">Id Nat</label>
                                                        <div class="col-lg-8">
                                                            <input type="text" class="form-control" name="Idnat" placeholder="Veuilez saisir votre IdNat" id="inputIdNat">
                                                            <small>
                                                                <span class="idnat-error hidden text-danger">
                                                                    Veuillez saisir le Id Nat
                                                                </span>
                                                            </small>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="inputRccm" class="col-lg-3 col-form-label text-right">Rccm</label>
                                                        <div class="col-lg-8">
                                                            <input type="text" class="form-control" name="Rccm" placeholder="Veuilez saisir votre Rccm" id="inputRccm">
                                                            <small>
                                                                <span class="rccm-error hidden text-danger">
                                                                    Veuillez saisir le Rccm
                                                                </span>
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputNom" class="col-lg-3 col-form-label text-right">Nom</label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" name="Nom" placeholder="Veuilez saisir votre nom" id="inputNom">
                                                        <small>
                                                            <span class="nom-error hidden text-danger">
                                                                Veuillez saisir le nom
                                                            </span>
                                                        </small>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputPostnom" class="col-lg-3 col-form-label text-right">Postnom</label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" name="Postnom" placeholder="Veuilez saisir votre postnom" id="inputPostnom">
                                                        <small>
                                                            <span class="postnom-error hidden text-danger">
                                                                Veuillez saisir le postnom
                                                            </span>
                                                        </small>
                                                    </div>
                                                </div>
                                                <div class="prenom-field">
                                                    <div class="form-group row">
                                                        <label for="inputPrenom" class="col-lg-3 col-form-label text-right">Pr&eacute;nom</label>
                                                        <div class="col-lg-8">
                                                            <input type="text" class="form-control" name="Pr&eacute;nom" placeholder="Veuilez saisir votre pr&eacute;nom" id="inputPrenom">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputEmail" class="col-lg-3 col-form-label text-right">Email</label>
                                                    <div class="col-lg-8">
                                                        <input type="text" class="form-control" name="Email" placeholder="Veuilez saisir votre adresse mail" id="inputEmail">
                                                        <small>
                                                            <span class="email-error hidden text-danger">
                                                                Veuillez saisir l'adresse mail
                                                            </span>
                                                        </small>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="inputTelephone" class="col-lg-3 col-form-label text-right">T&eacute;l&eacute;phone</label>
                                                    <div class="col-lg-8">
                                                        <div class="iti iti--allow-dropdown">
                                                            <input type="text" class="form-control" name="T&e&eacute;l&e&eacute;phone" placeholder="Veuilez saisir votre numéro de t&eacute;l&eacute;phone" id="inputTelephone" autocomplete="off" data-intl-tel-input-id="0">
                                                            <small><span class="phone-error hidden text-danger">
                                                                    Veuillez saisir le num&eacute;ro de t&eacute;l&eacute;phone
                                                                </span>
                                                            </small>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row" style="margin-top: .5rem">
                                                    <label for="adress-set" class="col-lg-3 col-form-label text-right">Adresse</label>
                                                    <div class="col-lg-6">
                                                        <div class="list-group adress-set">
                                                            <div class="list-group-item adresse-error">
                                                                Ajouter une adresse
                                                            </div>
                                                        </div>
                                                        <small>
                                                            <span class="text-success font-weight-bold">
                                                                L'adresse par défaut est mise en surbrillance
                                                            </span>
                                                        </small>

                                                    </div>
                                                    <div class="col-lg-2">
                                                        <button type="button" class="btn btn-primary btn_add_address">
                                                            <i class="align-middle" data-feather="map-pin"></i>&nbsp;Adresse
                                                        </button>

                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row btn-list">
                                                <div class="col-xl-6 btn-list text-left">
                                                    <small><span>Voux avez d&eacute;j&agrave; un compte ? <a href="login" class="text-danger">Cliquez ici</a></span></small>
                                                </div>
                                                <div class="col-xl-6 btn-list text-right">
                                                    <button class="btn btn-primary btn-primary-register" type="button">Enregistrer</button>
                                                    <button class="btn btn-dark btn-dark-cancel" type="button">Annuler</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="alert" class="form-success-register hidden">
                                        <button type="button" class="close" data-dismiss="alert">
                                        </button>
                                        <div class="alert-message" style="padding-bottom: 2rem">
                                            <div>
                                                <div class="form-group">
                                                    <div class="alert alert-success" role="alert">
                                                        <div class="alert-icon">
                                                            <i data-feather="check-square"></i>
                                                            <div class="spinner-border spinner-border-sm text-danger mr-2" role="status" hidden="true">
                                                                <span class="sr-only">Loading...</span>
                                                            </div>
                                                        </div>
                                                        <div class="alert-message">
                                                            Votre inscription &agrave; la t&eacute;l&eacute;-proc&eacute;dure eGoPass a r&eacute;ussi avec succ&egrave;s.
                                                            Nous vous avons envoy&eacute; un mail de confirmation de votre inscription.
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="alert" class="form-active-register">
                                        <button type="button" class="close" data-dismiss="alert">
                                        </button>
                                        <div class="alert-message">
                                            <div>
                                                <div class="form-group">
                                                    <div class="text-center mt-4">
                                                        <h1 class="h2 text-register-dark1">Activation du compte</h1>
                                                    </div>
                                                    <div class="alert alert-dark" role="alert">
                                                        <div class="alert-icon text-response">
                                                            <i class="success-activate" data-feather="check-square"></i>
                                                            <i class="failed-activate" data-feather="x-square"></i>
                                                        </div>
                                                        <div class="alert-message message-text-activated" style="padding: .95rem">
                                                            Activation de votre compte en cours...
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </main>

        <%@include file="asset/include/footer2.jsp" %>
        <%@include file="asset/include/modal/modalNewAdresse.jsp" %>

        <script src="asset/js/intlTelInput.js" type="text/javascript"></script>
        <script>
            var input = document.querySelector("#inputTelephone");
            window.intlTelInput(input, {
                preferredCountries: ['cd'],
                utilsScript: "asset/js/intl-tel-input/build/js/utils.js"
            });
        </script>


        <script src="asset/lib/jquery/jquery.min.js"></script>
        <script src="asset/lib/bootstrap/js/bootstrap.min.js"></script>
        <script src="asset/admin/js/app.js" type="text/javascript"></script>
        <script src="asset/alertify/alertify.min.js" type="text/javascript"></script>
        <script src="asset/js/jquery.blockUI.js"></script>
        <script src="asset/js/utils.js"></script>
        <script src="asset/js/core.js"></script>

        <script src="asset/js/front-main.js" type="text/javascript"></script>

    </body>
</html>
