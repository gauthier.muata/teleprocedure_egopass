<%-- 
    Document   : registre-commande
    Created on : 26 mars 2021, 14:49:16
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Activation des go-pass blancs</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--begin::Fonts -->
        <link href="asset/css/font.css" rel="stylesheet" type="text/css"/>
        <!--end::Fonts -->

        <!--begin::Page Vendors Styles(used by this page) -->
        <link href="asset/admin/css/fullcalendar.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Page Vendors Styles -->

        <!--begin::Global Theme Styles(used by all pages) -->
        <link href="asset/admin/css/plugins.bundle.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/style.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->

        <link href="asset/admin/css/light.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/light-menu.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy-aside.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/custom.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/paiement.css" rel="stylesheet" type="text/css"/>
        <!--end::Layout Skins -->

        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>




        <style type="text/css">
            /* Chart.js */
            /*
    * DOM element rendering detection
    * https://davidwalsh.name/detect-node-insertion
    */

            @keyframes chartjs-render-animation {
                from {
                    opacity: 0.99;
                }
                to {
                    opacity: 1;
                }
            }

            .chartjs-render-monitor {
                animation: chartjs-render-animation 0.001s;
            }
            /*
    * DOM element resizing detection
    * https://github.com/marcj/css-element-queries
    */

            .chartjs-size-monitor,
            .chartjs-size-monitor-expand,
            .chartjs-size-monitor-shrink {
                position: absolute;
                direction: ltr;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                overflow: hidden;
                pointer-events: none;
                visibility: hidden;
                z-index: -1;
            }

            .chartjs-size-monitor-expand > div {
                position: absolute;
                width: 1000000px;
                height: 1000000px;
                left: 0;
                top: 0;
            }

            .chartjs-size-monitor-shrink > div {
                position: absolute;
                width: 200%;
                height: 200%;
                left: 0;
                top: 0;
            }
        </style>
    </head>
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed" style="">
        <br>
        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
            <br>
            <div class="row">
                <div class="col-lg-12">
                    <div class="kt-portlet" id="kt_blockui_1_content">
                        <div class="kt-portlet__head">
                            <br>
                            <div class="alert-text param_cmd" style="font-weight: bold; font-size: 16px">
                                <br>
                                Go-Pass Tracking
                            </div>

                        </div>
                        <!--begin::Form-->

                        <div class="kt-portlet__corps">
                            <input type="hidden" name="level" id="typeCommande" value="1">
                            <div class="form-group form-group-last">
                                <br>
                                <div class="alert alert-secondary" role="alert">
                                    <div class="alert-icon"><i class="flaticon-clipboard kt-font-brand"></i></div>
                                    <div id="RefGopass" class="alert-text param_cmd" style="font-weight: bold; font-size: 16px">

                                    </div>
                                </div>
                            </div>
                            <div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                <div class="kt-form__section kt-form__section--first table-responsive-md">
                                    <div class="row">
                                        <div  class="icon col-xs-6 col-md-6" > 
                                            <div class="card-header">
                                                <b>Recherche du Go-Passe</b>
                                            </div>
                                            <div class="card shadow">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-12">
                                                            <label><b>Code du Go-Pass</b></label>
                                                            <input type="text" class="form-control required" name="userName" id="CodeGoPass" placeholder="Ex: CP-AA-650061"  >
                                                            <br>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div  class="icon col-xs-6 col-md-6" > 
                                            <div class="card-header">
                                                <b>Information du Go-Passe</b>
                                            </div>
                                            <div class="card shadow">
                                                <div class="card-body">
                                                    <div class="row">
                                                        <div class="col-lg-12" >
                                                            <label id="DetailsGoPassInfo"><b></b></label>
                                                            <br>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div  class="icon col-xs-6 col-md-6" > 
                                            <div class="card-header">
                                                <b></b>
                                            </div>
                                            <div class="card shadow">
                                                <div class="card-body">
                                                    <button class="btn btn-primary" id="btnCheckGoPass" style="float:right" >
                                                        <i class="fa fa-search"></i> Rechercher le GoPass
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                        <div  class="icon col-xs-6 col-md-6" > 
                                            <div class="card-header">
                                                <b></b>
                                            </div>
                                            <div class="card shadow">
                                                <div class="card-body">
                                                    <button class="btn btn-primary" id="btnPrintGoPass" style="float:right" >
                                                        <i class="fa fa-print"></i> Afficher le GoPass
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
        <!--end::Form-->
    </div>

</div>

</div>
</div>
</div>

<%@include file="asset/include/footerV2.html" %>
<%@include file="asset/include/modal/modalprintGopass.jsp" %>


<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "metal": "#c4c5d6",
                "light": "#ffffff",
                "accent": "#00c5dc",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995",
                "focus": "#9816f4"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
<script src="asset/admin/js/plugins.bundle.js" type="text/javascript"></script>
<script src="asset/admin/js/scripts.bundle.js" type="text/javascript"></script>
<script src="asset/vendor/js/propper.min.js" type="text/javascript"></script>

<script src="asset/admin/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="asset/admin/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

<script src="asset/js/jquery.blockUI.js"></script>

<script src="asset/alertify/alertify.min.js" type="text/javascript"></script>
<script src="asset/lib/sweetalert.js" type="text/javascript"></script>
<script src="asset/js/core.js" type="text/javascript"></script>
<script src="asset/js/utils.js" type="text/javascript"></script>
<script src="asset/js/export/jspdf.min.js" type="text/javascript"></script>
<script src="asset/js/export/jspdf.plugin.autotable.js" type="text/javascript"></script>
<script src="asset/js/gopass-tracking.js" type="text/javascript"></script>
<script src="asset/js/qr/jquery-qrcode.js" type="text/javascript"></script>
</body>
</html>
