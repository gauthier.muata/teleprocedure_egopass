<%-- 
    Document   : registre-commande
    Created on : 26 mars 2021, 14:49:16
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Activation des go-pass blancs</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--begin::Fonts -->
        <link href="asset/css/font.css" rel="stylesheet" type="text/css"/>
        <!--end::Fonts -->

        <!--begin::Page Vendors Styles(used by this page) -->
        <link href="asset/admin/css/fullcalendar.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Page Vendors Styles -->

        <!--begin::Global Theme Styles(used by all pages) -->
        <link href="asset/admin/css/plugins.bundle.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/style.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->

        <link href="asset/admin/css/light.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/light-menu.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy-aside.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/custom.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/paiement.css" rel="stylesheet" type="text/css"/>
        <!--end::Layout Skins -->

        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>




        <style type="text/css">
            /* Chart.js */
            /*
    * DOM element rendering detection
    * https://davidwalsh.name/detect-node-insertion
    */

            @keyframes chartjs-render-animation {
                from {
                    opacity: 0.99;
                }
                to {
                    opacity: 1;
                }
            }

            .chartjs-render-monitor {
                animation: chartjs-render-animation 0.001s;
            }
            /*
    * DOM element resizing detection
    * https://github.com/marcj/css-element-queries
    */

            .chartjs-size-monitor,
            .chartjs-size-monitor-expand,
            .chartjs-size-monitor-shrink {
                position: absolute;
                direction: ltr;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                overflow: hidden;
                pointer-events: none;
                visibility: hidden;
                z-index: -1;
            }

            .chartjs-size-monitor-expand > div {
                position: absolute;
                width: 1000000px;
                height: 1000000px;
                left: 0;
                top: 0;
            }

            .chartjs-size-monitor-shrink > div {
                position: absolute;
                width: 200%;
                height: 200%;
                left: 0;
                top: 0;
            }
        </style>
    </head>
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed" style="">
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed">
            <div class="kt-header-mobile__logo">
                <h4 class="text-light">HOLOGRAM</h4> 
            </div>
            <div class="kt-header-mobile__toolbar">

                <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
            </div>
        </div>

        <div class="kt-grid kt-grid--hor kt-grid--root">

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <button class="kt-aside-close " id="kt_aside_close_btn">
                    <i class="la la-close"></i>
                </button>

                <%@include file="asset/include/menu.html" %>

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    <%@include file="asset/include/headerV2.html" %>

                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid "></div>
                        </div>

                        <!-- Begin Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="kt-portlet" id="kt_blockui_1_content">
                                        <div class="kt-portlet__head">
                                            <div class="alert-text param_cmd" style="font-weight: bold; font-size: 16px">
                                                <br>
                                                Activation du Go-Pass
                                            </div>

                                        </div>
                                        <!--begin::Form-->
                                        <form class="kt-form kt-form--label-right" id="form-repertoire-commande">
                                            <div class="kt-portlet__corps">
                                                <input type="hidden" name="level" id="typeCommande" value="1">
                                                <div class="form-group form-group-last">
                                                    <br>
                                                    <div class="alert alert-secondary" role="alert">
                                                        <div class="alert-icon"><i class="flaticon-clipboard kt-font-brand"></i></div>
                                                        <div id="RefGopass" class="alert-text param_cmd" style="font-weight: bold; font-size: 16px">
                                                            Go-Passe Référence : 795WXSF485
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                    <div class="kt-form__section kt-form__section--first table-responsive-md">

                                                        <form class="kt-form kt-form--label-right">


                                                            <div class="row">
                                                                <div  class="icon col-xs-6 col-md-6" > 
                                                                    <div class="card-header">
                                                                        <b>1. Aéroport de Départ (Uniquement de la RDC)</b>
                                                                    </div>
                                                                    <div class="card shadow">

                                                                        <div class="col-lg-12">
                                                                            <div class="card-body">
                                                                                <label><b>Aeroport</b></label>
                                                                                <select class="form-control" name="Type GoPass" id="cmb_aeroport_provenance">
                                                                                    
                                                                                </select>
                                                                                <br>
                                                                                <p id="DetNational"></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="icon col-xs-6 col-md-6" > 

                                                                    <div class="card-header">
                                                                        <b>2. Aéroport de Destination</b>
                                                                    </div>
                                                                    <div class="card shadow">

                                                                        <div class="col-lg-12">
                                                                            <div class="card-body">
                                                                                <label><b>Aéroport</b></label>
                                                                                <select class="form-control" name="Type GoPass" id="cmb_aeroport_destination">
                                                                                    <option value="0" selected disabled>S&eacute;lectionner un type</option>
                                                                                    <option value="1">C</option>
                                                                                    <option value="2">Y</option>
                                                                                </select>
                                                                                <br>
                                                                                <p id="DetInternational"></p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div  class="icon col-xs-6 col-md-6" > 
                                                                    <br>
                                                                    <div class="card-header">
                                                                        <b>3. Information sur la compagnie</b>
                                                                    </div>
                                                                    <div class="card shadow" >

                                                                        <div class="card-body">
                                                                            <div class="row">
                                                                                <div class="col-lg-7">
                                                                                    <label><b>Nom de la compagnie</b></label>
                                                                                    <select class="form-control" name="Type GoPass" id="cmb_compagnie">
                                                                                    </select>
                                                                                </div>
                                                                                <div class="col-lg-5">
                                                                                    <label><b>N° Vol</b></label>
                                                                                    <input type="text" class="form-control required" id="NumeroVol"  >
                                                                                </div>
                                                                                <div class="col-lg-12">
                                                                                    <label><b style="color:white">.</b></label>
                                                                                    <div class="card card-pills">
                                                                                        <div class="card-body block-el" id="Logo">
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div  class="icon col-xs-6 col-md-6" > 
                                                                    <br>
                                                                    <div class="card-header">
                                                                        <b>4. Information sur le vol</b>
                                                                    </div>
                                                                    <div class="card shadow" >

                                                                        <div class="card-body">
                                                                            <div class="row">
                                                                                <div class="col-lg-12">
                                                                                    <label><b>Date du Vol</b></label>
                                                                                    <input type="date" class="form-control required" id="DateVol"  >
                                                                                </div>
                                                                                <div class="col-lg-12">
                                                                                    <br>
                                                                                    <label><b>Heure du Vol</b></label>
                                                                                    <input type="time" class="form-control required" id="HeureVol"  >
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <div class="icon col-xs-12 col-md-12" > 
                                                                    <br>
                                                                    <div class="card-header">
                                                                        <b>5. Information sur le passager</b>
                                                                    </div>
                                                                    <div class="card shadow">
                                                                        <div class="card-body">
                                                                            <div class="row">
                                                                                <div class="col-lg-6">

                                                                                    <label for="userName"><b>Prénom</b></label>
                                                                                    <input type="text" class="form-control required" name="userName" id="Prenom" placeholder="Ex: Christian"  >
                                                                                    <br>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <label><b>Nom</b></label>
                                                                                    <input type="text" class="form-control required" name="userName" id="Nom" placeholder="Ex: LUKETA"  >
                                                                                    <br>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <label><b>PostNom</b></label>
                                                                                    <input type="text" class="form-control required" name="userName" id="Postnom" placeholder="Ex: KANYINDA"  >
                                                                                    <br>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <label><b>Sexe</b></label>
                                                                                    <select class="form-control" name="Type GoPass" id="cmb_Sexe">
                                                                                        <option value="0" selected disabled>S&eacute;lectionner le sexe</option>
                                                                                        <option value="Feminin">Feminin</option>
                                                                                        <option value="Masculin">Masculin</option>
                                                                                        <option value="Masculin">Enfant</option>
                                                                                        <option value="Masculin">Child</option>
                                                                                    </select>
                                                                                    <br>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>



                                                <div class="row">
                                                    <div class="icon col-xs-12 col-md-12" >
                                                        <br>
                                                        <div class="card">

                                                            <div class="card-body">
                                                                <button class="btn btn-primary" id="btnActiveGoPass" style="float:right" >
                                                                    <i class="fa fa-check-circle"></i> Activer le GoPass
                                                                </button>
                                                                <br>
                                                            </div>
                                                            <br>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>



                                        </form>
                                    </div>
                                </div>
                            </div>
                            </form>
                            <!--end::Form-->
                        </div>
                    </div>
                    <br>
                </div>
            </div>
            <!-- End Content -->
        </div>
    </div>
</div>
</div>

<%@include file="asset/include/footerV2.html" %>
<%@include file="asset/include/modal/modalprintGopass.jsp" %>


<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "metal": "#c4c5d6",
                "light": "#ffffff",
                "accent": "#00c5dc",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995",
                "focus": "#9816f4"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>
<script src="asset/admin/js/plugins.bundle.js" type="text/javascript"></script>
<script src="asset/admin/js/scripts.bundle.js" type="text/javascript"></script>
<script src="asset/vendor/js/propper.min.js" type="text/javascript"></script>

<script src="asset/admin/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="asset/admin/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

<script src="asset/js/jquery.blockUI.js"></script>

<script src="asset/alertify/alertify.min.js" type="text/javascript"></script>
<script src="asset/lib/sweetalert.js" type="text/javascript"></script>
<script src="asset/js/core.js" type="text/javascript"></script>
<script src="asset/js/utils.js" type="text/javascript"></script>
<script src="asset/js/export/jspdf.min.js" type="text/javascript"></script>
<script src="asset/js/export/jspdf.plugin.autotable.js" type="text/javascript"></script>
<script src="asset/js/activer-gopass-detail.js" type="text/javascript"></script>
<script src="asset/js/qr/jquery-qrcode.js" type="text/javascript"></script>
<script src="asset/js/qr/qr.js" type="text/javascript"></script>
</body>
</html>
