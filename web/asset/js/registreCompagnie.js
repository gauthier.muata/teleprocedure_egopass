/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var urlPhoto, FileName;
var len = 0;
var dataImage = new Array();
var dataImageVisualiser = new Array();

var url2, FileName2;
var fileExtensionPhoto = "";
var fileExtensionEmpreinte = "";

$(function () {


    LoadCompagnie();


    $('#btnAddCompagnie').on('click', function (e) {
        e.preventDefault();
        $('.addnewcompagnie').modal('show');
    });

    $('#LoadProfilPhoto').on('change', (function (e) {
        e.preventDefault();
        if (this.files && this.files [0]) {
            var myFile = this.files [0];
            var file = document.querySelector('input[type=file]').files[0];
            FileName = JSON.parse(JSON.stringify(myFile));
            var reader = new FileReader();
            var fileElement = document.getElementById("LoadProfilPhoto");

            FileName = fileElement.value;
            reader.addEventListener('load', function (e) {

                if (fileElement.value.lastIndexOf(".") > 0) {
                    fileExtensionPhoto = fileElement.value.substring(fileElement.value.lastIndexOf(".") + 1, fileElement.value.length);
                }

                var reader = new FileReader();
                reader.onloadend = function () {

                    base64 = reader.result;
                    extention = fileExtensionPhoto.toLowerCase();

                    if ((fileExtensionPhoto.toLowerCase() == "jpg") || (fileExtensionPhoto.toLowerCase() == "jpeg") || (fileExtensionPhoto.toLowerCase() == "png")) {
                        urlPhoto = 'background-image: url(' + base64 + ')';
                        paths = base64;

                        $('#Photo').attr('style', "background-size: 60%; margin:auto;width : 100%; height : 20em;background-repeat: no-repeat;background-position: center;  " + urlPhoto);
                    } else {
                        swal({text: "Veuillez sélectionner les fichiers dont l'extention est .png, .jpeg, .jpg", icon: "error"})
                        FileName = '';
                        urlPhoto = '';
                        return;
                    }


                };
                if (myFile) {
                    reader.readAsDataURL(myFile);
                }

            });
            reader.readAsBinaryString(myFile);
        }


    }));


    $('#btnSaveCompagnie').on('click', function (e) {
        e.preventDefault();
        dataPut = {};
        dataPut.methode = 'v1/savecompagnie';
        dataPut.dataType = 'JSON';
        dataPut.type = 'POST';

        dataparam = {};
        dataparam.intitule = $('#IntituleCompagnie').val();
        dataparam.sigle = $('#IntituleCompagnie').val();
        dataparam.pathPhoto = urlPhoto;
        dataparam.ExtPhoto = fileExtensionPhoto;
        dataparam.AgentCreat = userNif;
        dataPut.loadmessage = "Enregistrement en cours ..."
        sendDataCompagnie(dataPut, dataparam)
    });

});

function LoadCompagnie()
{
    dataPut = {};
    dataPut.methode = 'v1/getcompagnieList';
    dataPut.dataType = 'json';
    dataPut.loadmessage = 'Chargement en cours ...'
    dataPut.type = 'GET';
    sendDataCompagnie(dataPut, "")
}

function sendDataCompagnie(dataPut, dataparam) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: dataPut.loadmessage
    });

    $.ajax(
            {type: dataPut.type,
                url: baseUrl + dataPut.methode + '',
                dataType: dataPut.dataType,
                headers: headers,
                crossDomain: true,
                data: JSON.stringify(dataparam),
                success: function (response)
                {
                    setTimeout(function () {

                        callbackData(response, dataPut.methode);
                    }
                    , 5000);
                },
                complete: function () {

                },
                error: function (xhr, status, error) {
                    KTApp.unblockPage();
                    swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})
                }
            });
}

function callbackData(response, methode) {

    switch (methode) {
        case 'v1/getcompagnieList':

            var ListCompagnieDataList = JSON.parse(JSON.stringify(response));
            if (response == "-1") {
                swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})

            }
            printCompagnie(ListCompagnieDataList);
            break;

        case 'v1/savecompagnie':

            var returns = JSON.parse(JSON.stringify(response));
            if (response == "-1") {
                swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})

            } else {
                swal({text: "La compagnie a été créée avec succès !", icon: "info"})

            }
            LoadCompagnie();
            KTApp.unblockPage();
            break;
    }
}

function printCompagnie(ListCompagnieDataList) {

    var header = '<thead style="background-color:#5867dd;color:white"><tr><th style="width:15%;display:none">IdCompagnie</th>';
    header += '<th style="width:5%;color:white"><b>N°</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:10%;color:white"><b>LOGO</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:35%;color:white"><b>INTITULE DE LA COMPAGNIE</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:5%;;color:white"><b>ACTION</b></th>';
    header += '</tr> </thead>';

    var body = '<tbody>';

    for (var i = 0; i < ListCompagnieDataList.length; i++) {

        body += '<td style="display:none">' + ListCompagnieDataList[i].IdCompagnie + '</td>';
        body += '<td><b>' + (i + 1) + '</b></td>';
        body += '<td><b><div style="background-size: 100%; margin:auto;width : 80%; height : 4em;background-repeat: no-repeat;background-position: center;  background-image:' + ' url(' + ListCompagnieDataList[i].Logo + ')"></b></td>';
        body += '<td><b>' + ListCompagnieDataList[i].Intitule + '</b></td>';
        body += '<td><b><a href="#"><i class="align-middle fas fa-fw fa-edit"></i></a></b></td>'
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    $('#table_compagnie').html(tableContent);
    var dtCompagnie = $('#table_compagnie').DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 20,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    KTApp.unblockPage();
}

