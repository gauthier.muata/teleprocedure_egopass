/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var tempBienList = [],
        commandeList = [],
        detailVoucherList = [];

var host = window.location.origin;
var commandeId,
        stamp_,
        img,
        table,
        idCarte,
        operationType,
        etatNoVoucher = 1;
var btn_attribuer = $('#btn_attribuer');

$(function () {

    checkIfConnexionOn();
    setMenu();
    getCartesVoucher();
    setTableDataCartes('');

    btn_attribuer.on('click', function (e) {
        e.preventDefault();
        var form = this;
        var rows_selected = table.column(0).checkboxes.selected();

        $.each(rows_selected, function (index, rowId) {
            $(form).append(
                    $("<input>")
                    .attr("type", "hidden")
                    .attr("name", "id[]")
                    .val(rowId)
                    );
        });

        if (rows_selected.length == 0) {
            Swal.fire({
                title: "Affectation Gopass",
                text: "Veuillez cocher au moins une ligne avant de continuer.",
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            return;
        } else {
            var data = rows_selected.join(",").split(",");
            var message = 'Etes-vous sûr de vouloir affecter ces gopass à cette carte ?';
            if (operationType == 2) {
                message = 'Etes-vous sûr de vouloir désaffecter ces gopass de cette carte ?';
            }
            alertify.confirm(message, function () {
                attribuerGopass(JSON.stringify(data), operationType);
            });
        }
    });
});

function getCartesVoucher() {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + 'v1/getListCartesGopass/' + userNif + '',
        crossDomain: true,
        method: 'GET',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        } else if (data == "0") {
            setTableDataCartes('');
            return;
        }

        setTableDataCartes(data);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessageOnRequest();
    }).always(function () {
        KTApp.unblockPage();
    });
}

function setTableDataCartes(data) {

    var numero = 0;
    var tableContent = '';
    tableContent += '<thead style="background-color:#5867dd;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="color:white">#</th>';
    tableContent += '<th style="text-align:left; color:white">NUMERO</th>';
    tableContent += '<th style="text-align:left; color:white">ETAT</th>';
    tableContent += '<th style="text-align:left; color:white">OBSERVATION</th>';
    tableContent += '<th style="text-align:left; color:white" colspan="3">VOUCHER</th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    $.each(data, function (index, item) {
        numero += 1;
        var etat = '<td style="vertical-align:middle;color:red;" scope="col">EN ATTENTE DE VALIDATION</td>';
        var btnAffectGopass = '<td></td>';
        var btnDesaffectGopass = '<td></td>';
        var btnHistoriqueGopass = '<td></td>';

        if (item.etat == 1) {
            etat = '<td style="vertical-align:middle;color:green;" scope="col">VALIDE</td>';
            btnAffectGopass = '<td><button type="button" class="btn btn-secondary" ' +
                    'onclick="getAffectedGopass(\'' + item.id + '\')" ' +
                    'title="Voir les gopass non affect&eacute;s à cette carte">' +
                    '<i class="flaticon2-soft-icons"></i>Affecter</button></td>';
            btnDesaffectGopass = '<td><button type="button" class="btn btn-secondary" ' +
                    'onclick="getDeaffectedGopass(\'' + item.id + '\')" ' +
                    'title="Voir les vouchers affect&eacute;s à cette carte">' +
                    '<i class="flaticon2-soft-icons"></i>D&eacute;saffecter</button></td>';
            btnHistoriqueGopass = '<td><button type="button" class="btn btn-secondary" ' +
                    'onclick="getHistoriqueGopass(\'' + item.id + '\')" ' +
                    'title="Voir l\'historique des gopass affect&eacute;s à cette carte">' +
                    '<i class="flaticon2-soft-icons"></i>Historique</button></td>';
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;">' + numero + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + item.numero + '</td>';
        tableContent += etat;
        tableContent += '<td style="vertical-align:middle;">' + item.observation + '</td>';
        tableContent += btnHistoriqueGopass;
        tableContent += btnAffectGopass;
        tableContent += btnDesaffectGopass;
        tableContent += '</tr>';
    });

    tableContent += '</tbody>';
    $('#table-cartes').html(tableContent);
    $('#table-cartes').DataTable({
        "order": [[1, "asc"]],
        destroy: true,
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

}

function getAffectedGopass(id) {
    operationType = 1;
    idCarte = id;
    getDetailsGopass(1);
    btn_attribuer.removeClass('hidden');
    $('.title-voucher').html('Affectation des vouchers');
//    $('.div_v_usage').addClass('hidden');
}

function getDeaffectedGopass(id) {
    operationType = 2;
    idCarte = id;
    getDetailsGopass(2);
    btn_attribuer.removeClass('hidden');
    $('.title-voucher').html('D\351saffectation des vouchers');
}

function getHistoriqueGopass(id) {
    operationType = 3;
    idCarte = id;
    getDetailsGopass(3);
    btn_attribuer.addClass('hidden');
    $('.title-voucher').html('Historique des vouchers');
//    $('.div_v_usage').removeClass('hidden');
}

function getDetailsGopass(type) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + 'v1/getGopassCarte/' + userNif + '/' + idCarte + '/' + type,
        crossDomain: true,
        method: 'GET',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        KTApp.unblockPage();

        console.log(data)

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        } else if (data == "0") {
            if (etatNoVoucher == 1) {
                alertify.alert('Aucun gopass n\'est disponible pour faire cette opération');
            }
            etatNoVoucher = 1;
            setTableDataDetailGopass('');
            return;
        }
        detailVoucherList = data;
        setTableDataDetailGopass(data);
        $('.modalVoucher').modal('show');

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessageOnRequest();
    }).always(function () {
        KTApp.unblockPage();
    });
}

function setTableDataDetailGopass(data) {

    var numero = 0, usedGopassNber = 0, unusedGopassNber = 0;
    var tableContent = '';
    tableContent += '<thead style="background-color:#5867dd;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="color:white"></th>';
    tableContent += '<th style="text-align:left; color:white"><small>CODE GOPASS</small></th>';
    tableContent += '<th style="text-align:left; color:white"><small>ETAT</small></th>';
//    tableContent += '<th style="text-align:left; color:white">DATE CREATION</th>';
    if (operationType == 3) {
        tableContent += '<th style="text-align:left; color:white"><small>DATE UTILISATION</small></th>';
        tableContent += '<th style="text-align:left; color:white"><small>PROVENANCE</small></th>';
        tableContent += '<th style="text-align:left; color:white"><small>DESTINATION</small></th>';
    }
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    $.each(data, function (index, item) {
        numero += 1;
        var etat = '<td style="vertical-align:middle;color:green">BLANC</td>';
        var aeroProvenance = '<td style="vertical-align:middle;">-</td>';
        var aeroDestination = '<td style="vertical-align:middle;">-</td>';

        if (item.dateUtilisation != '-') {
            usedGopassNber++;
            etat = '<td style="vertical-align:middle;color:red"><small>UTILISE</small></td>';
            aeroDestination = '<td style="vertical-align:middle;"><small>' + item.aeroportDestination + '</small></td>';
        } else {
            unusedGopassNber++;
            if (item.etat === 2) {
                etat = '<td style="vertical-align:middle;color:blue"><small>BLANC</small></td>';
            } else if (item.etat === 1) {
                etat = '<td style="vertical-align:middle;color:blue"><small>ACTIVE</small></td>';
            }
            aeroProvenance = '<td style="vertical-align:middle;"><small>' + item.aeroportProvenance + '</small></td>';
        }

        tableContent += '<tr>';
        tableContent += '<td style="vertical-align:middle;">' + item.id + '</td>';
        tableContent += '<td style="vertical-align:middle; "><small>' + item.codeGoPass + '</mall></td>';
        tableContent += etat;
//        tableContent += '<td style="vertical-align:middle;">' + item.dateGeneration + '</td>';
        if (operationType == 3) {
            tableContent += '<td style="vertical-align:middle;"><small>' + item.dateUtilisation + '</small></td>';
            tableContent += aeroProvenance;
            tableContent += aeroDestination;
        }
        tableContent += '</tr>';
    });

    $('.info-used-voucher').html('<div class="list-group-item" style="color: red">' + usedGopassNber + '</div>');
    $('.info-unused-voucher').html('<div class="list-group-item" style="color: green">' + unusedGopassNber + '</div>');

    tableContent += '</tbody>';

    if (operationType == 3) {
        $('#tbl_det_gh').html(tableContent);
        $('#tbl_det_gh').removeClass('hidden');
        $('#tbl_det_gh').DataTable({
            "order": [[1, "asc"]],
            destroy: true,
            language: {
                processing: "Traitement en cours...",
                track: "Rechercher&nbsp;:",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donnée disponible",
                search: "Rechercher _INPUT_  ",
                paginate: {
                    first: "Premier",
                    previous: "Pr&eacute;c&eacute;dent",
                    next: "Suivant",
                    last: "Dernier"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            }
        });
    } else {
        $('#tbl_det_gh').addClass('hidden');
        $('#table_voucher').removeClass('hidden');
        $('#table_voucher').html(tableContent);
        table = $('#table_voucher').DataTable({
            "order": [[1, "asc"]],
            destroy: true,
            columnDefs: [
                {
                    targets: 0,
                    checkboxes: {
                        selectRow: true
                    }
                }
            ],
            select: {
                style: "multi"
            },
            language: {
                processing: "Traitement en cours...",
                track: "Rechercher&nbsp;:",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donnée disponible",
                search: "Rechercher _INPUT_  ",
                paginate: {
                    first: "Premier",
                    previous: "Pr&eacute;c&eacute;dent",
                    next: "Suivant",
                    last: "Dernier"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre décroissant"
                }
            }
        });
    }

}

function attribuerGopass(dataJson, type) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    var object = {
        "idCarte": idCarte,
        "voucherData": dataJson,
        "type": type
    };

    $.ajax({
        url: baseUrl + 'v1/attributeCarteGopass/',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        } else if (data == "0") {
            Swal.fire({
                title: "Attribution Gopass",
                text: "L'attribution des gopass à votre carte NFC n'a pas abouti. Veuillez reprendre l'op\351ration ou contacter l'administrateur.",
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            return;
        }
        var text = "Les vouchers s\351lectionn\351s ont \351t\351 affect\351es à votre carte avec succès";
        if (operationType == 2) {
            text = "Les gopass s\351lectionn\351s ont \351t\351 désaffect\351es de votre carte avec succès";
        }
        Swal.fire({
            title: "Attribution Gopass",
            text: text,
            type: "success",
            confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
        });

        etatNoVoucher = 0;
        getDetailsGopass(type);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessageOnRequest();
    }).always(function () {
        KTApp.unblockPage();
    });

}