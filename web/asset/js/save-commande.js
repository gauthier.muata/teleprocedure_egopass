/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {

    checkIfConnexionOn();
    setMenu();

    var reference = getUrlParameter('reference');
    if (reference == undefined) {
        $('.success-payment').addClass('alert-outline-danger');
        $('.succes_paiement').html("Erreur du paiement de la commande.");
        return;
    }

    $('.title-operation').html("");
    $('.alert-icon').addClass('hidden');
    $('.succes_paiement').html('<span style="font-size:16px">Le paiement de votre comande est en cours </span><img src="asset/img/loading.gif" alt=""/>');

    $.ajax({
        url: baseUrl + 'v1/setEtatPayCommande/' + reference,
        crossDomain: true,
        method: 'GET',
        contentType: 'application/json',
        headers: headers,
        dataType: "text"
    }).done(function (data) {

        $('.succes_paiement').html("");

        if (data === '-1') {
            $('.success-payment').addClass('alert-outline-danger');
            $('.succes_paiement').html('<span style="font-size:16px">Nous avons constat\351 une erreur lors du paiement de votre commande. Veuilllez contacter l\'administrateur.</span>');
            return;
        } else if (data.split(',')[0] === '0') {
            $('.success-payment').addClass('alert-outline-warning');
            $('.succes_paiement').html('<span style="font-size:16px">Le paiement de votre comande a \351chou\351. Veuilllez r\351essayer ou contacter l\'administrateur.</span>');
            return;
        } else if (data.split(',')[0] === '2') {
            $('.success-payment').addClass('alert-outline-warning');
            $('.succes_paiement').html('<span style="font-size:16px">Cette commande de r\351f\351rence <b class="dec_code">' + data.split(',')[1] + '</b> a &eacute;t&eacute; d&eacute;jà payée avec succès.</span>');
            return;
        }

        $('.alert-icon').removeClass('hidden');
        $('.succes_paiement').html('<span style="font-size:16px">Le paiement de votre commande de r\351f\351rence <b class="dec_code">' + data.split(',')[1] + '</b> a &eacute;t&eacute; effectu&eacute; avec succès</span>');


    }).fail(function (jqXHR, textStatus, errorThrown) {
        $('.success-payment').addClass('alert-outline-danger');
        $('.succes_paiement').html("Erreur du paiement de la commande.");
    });

});