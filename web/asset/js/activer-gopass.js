/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ListGoPassBlancDataList;

$(function () {

    LoadGoPassBlanc();

});

function LoadGoPassBlanc()
{
    dataPut = {};
    dataPut.methode = 'v1/getGoPassBlancs';
    dataPut.paramRequest = userNif;
    dataPut.dataType = 'json';
    dataPut.loadmessage = 'Chargement en cours ...'
    dataPut.type = 'GET';
    sendDataCompagnie(dataPut, "")
}

function sendDataCompagnie(dataPut, dataparam) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: dataPut.loadmessage
    });
    var urlMethode = "";
    if (dataPut.methode == 'v1/getGoPassBlancs')
    {
        urlMethode = dataPut.methode + '/' + dataPut.paramRequest;
    }
    else
    {
        urlMethode = dataPut.methode;
    }
    $.ajax(
            {type: dataPut.type,
                url: baseUrl + urlMethode,
                dataType: dataPut.dataType,
                headers: headers,
                crossDomain: true,
                data: JSON.stringify(dataparam),
                success: function (response)
                {
                    setTimeout(function () {

                        callbackData(response, dataPut.methode);
                    }
                    , 5000);
                },
                complete: function () {

                },
                error: function (xhr, status, error) {
                    KTApp.unblockPage();
                    swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})
                }
            });
}

function callbackData(response, methode) {

    switch (methode) {
        case 'v1/getGoPassBlancs':

            ListGoPassBlancDataList = JSON.parse(JSON.stringify(response));
            if (response == "-1") {
                swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})
            }
            printGoPassBlanc(ListGoPassBlancDataList);
            break;


    }
}

function printGoPassBlanc(ListGoPassBlancDataList) {

    var header = '<thead style="background-color:#5867dd;color:white"><tr><th style="width:5%;display:none">IdGoPass</th>';
    header += '<th style="width:5%;color:white"><b>N°</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:10%;color:white"><b>CODE GO-PASS</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:20%;color:white"><b>NATURE GO-PASS</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:20%;color:white"><b>TYPE GO-PASS</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:20%;color:white"><b>MONTANT</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:20%;color:white"><b>DATE GENERATION</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:15%;color:white"><b>ACTION</b></th>';
    header += '</tr> </thead>';
    var TotNat = 1;
    var TotInt = 1;
    var bodyNat = '<tbody>';
    var bodyInt = '<tbody>';
    for (var i = 0; i < ListGoPassBlancDataList.length; i++) {
        if (ListGoPassBlancDataList[i].intituleTarif == 'National') {
            bodyNat += '<tr><td style="display:none">' + ListGoPassBlancDataList[i].id + '</td>';
            bodyNat += '<td style="width:5%">' + TotNat + '</b></td>';
            bodyNat += '<td style="width:20%"> ' + ListGoPassBlancDataList[i].code + '</td>';
            bodyNat += '<td style="width:20%">' + ListGoPassBlancDataList[i].intituleTarif + '</td>';
            bodyNat += '<td style="width:20%">' + ListGoPassBlancDataList[i].type + '</td>';
            bodyNat += '<td style="width:20%">' + ListGoPassBlancDataList[i].montant + ' '+ListGoPassBlancDataList[i].devise + '</td>';
            bodyNat += '<td style="width:20%">' + ListGoPassBlancDataList[i].dateCreate + '</td>';
            bodyNat += '<td style="width:5%"><center><button type="button" class="btn btn-primary pull-right" onclick="ActiverGoPass(\'' + ListGoPassBlancDataList[i].code + '\',\'' + ListGoPassBlancDataList[i].intituleTarif + '\',\'' + ListGoPassBlancDataList[i].type + '\',\'' + ListGoPassBlancDataList[i].montant + ' '+ListGoPassBlancDataList[i].devise + '\')">Activer</button></center></td>'
            bodyNat += '</tr>';
            TotNat = TotNat + 1;
        }
        else if (ListGoPassBlancDataList[i].intituleTarif == 'International') {
            bodyInt += '<tr><td style="display:none">' + ListGoPassBlancDataList[i].id + '</td>';
            bodyInt += '<td style="width:5%">' + TotInt + '</b></td>';
            bodyInt += '<td style="width:20%">' + ListGoPassBlancDataList[i].code + '</td>';
            bodyInt += '<td style="width:20%">' + ListGoPassBlancDataList[i].intituleTarif + '</td>';
            bodyInt += '<td style="width:20%">' + ListGoPassBlancDataList[i].type + '</td>';
            bodyInt += '<td style="width:20%">' + ListGoPassBlancDataList[i].montant + ' '+ListGoPassBlancDataList[i].devise + '</td>';
            bodyInt += '<td style="width:20%">' + ListGoPassBlancDataList[i].dateCreate + '</td>';
            bodyInt += '<td style="width:5%"><center><button type="button" class="btn btn-primary pull-right" onclick="ActiverGoPass(\'' + ListGoPassBlancDataList[i].code + '\',\'' + ListGoPassBlancDataList[i].intituleTarif + '\',\'' + ListGoPassBlancDataList[i].type + '\',\'' + ListGoPassBlancDataList[i].montant + ' '+ListGoPassBlancDataList[i].devise + '\')">Activer</button></center></td>'
            bodyInt += '</tr>';
            TotInt = TotInt + 1;

        }

    }

    bodyNat += '</tbody>';
    bodyInt += '</tbody>';
    var tableContent = header + bodyNat;
    var tableContentInt = header + bodyInt;
    $('#table_go_pass_blanc_nationaux').html(tableContent);
    $('#table_go_pass_blanc_internationaux').html(tableContentInt);
    var dtGoPassBlanc = $('#table_go_pass_blanc_nationaux').DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 20,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    var dtGoPassBlancInt = $('#table_go_pass_blanc_internationaux').DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 20,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    KTApp.unblockPage();
}

function ActiverGoPass(GoPasssReference, NatureGopasse,TypeGopasse,Tarif)
{
    urlOpen = GoPasssReference + '@' + NatureGopasse+ '@' +TypeGopasse+ '@' +Tarif;
    var win = window.open('activer-gopass-detail?id=' + btoa(urlOpen), '_blank');
    win.focus();
//    window.location = 'activer-gopass-detail?id=' + btoa(urlOpen);
}

