var isDateFiltre = 1;

var div_date = $('.div_date'),
        btn_filtre = $('.btn_filtre'),
        btnFilter = $('.btnFilter'),
        dateDebut = $('#dateDebut'),
        dateFin = $('#dateFin');

$(function () {

    checkIfConnexionOn();
    setMenu();

    btn_filtre.on('click', function () {
        div_date.removeClass('hidden');
    });

    btnFilter.on('click', function () {
        isDateFiltre = 0;
        initDataDashboard(dateDebut.val(), dateFin.val());
    });

    initDataDashboard(getDateFormatYyMmDd(getDateToday()), getDateFormatYyMmDd(getDateToday()));
});

function hideDateDiv(value) {

    isDateFiltre = value;
    if (value != 0) {
        div_date.addClass('hidden');
        initDataDashboard(getDateFormatYyMmDd(getDateToday()), getDateFormatYyMmDd(getDateToday()));
    }

}

function initDataDashboard(dateDebut, dateFin) {

    if (dateDebut === '' || dateDebut === null) {
        alertify.alert('Veuillez d\'abord fournir la date de début.');
        return;
    }
    if (dateDebut == '' || dateDebut == null) {
        alertify.alert('Veuillez d\'abord fournir la date de fin.');
        return;
    }

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + 'v1/getDataDashBoardGopass/' + dateDebut + '/' + dateFin + '/' + userNif + '/' + isDateFiltre + '',
        crossDomain: true,
        method: 'GET',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        } else if (data == "0") {
            return;
        }

        console.log(data)

        setDataDashboard(data);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessageOnRequest();
    }).always(function () {
        KTApp.unblockPage();
    });
}

function setDataDashboard(data) {
    $.each(data, function (index, item) {
        item.nombreCmdCartes > 1 ? $('#nombreCmdCartes').html(item.nombreCmdCartes + ' cartes command&eacute;es') : $('#nombreCmdCartes').html(item.nombreCmdCartes + ' carte command&eacute;e');
//        $('#progress_carte').html('<div class="progress-bar bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>');
        var nombreCardAffected = (item.nombreCardAffected / item.nombreCmdCartes * 100);
        item.nombreCmdCartes != 0 ? isFloat(nombreCardAffected) ? nombreCardAffected = nombreCardAffected.toFixed(2) : nombreCardAffected : nombreCardAffected = 0;
        $('#nombreCardAffected').html(item.nombreCardAffected + ' (' + nombreCardAffected + '%)');
        $('#progress_carteAffected').html('<div class="progress-bar bg-success" role="progressbar" style="width: ' + nombreCardAffected + '%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>');
        var nombreCardNotAffected = (item.nombreCardNotAffected / item.nombreCmdCartes * 100);
        item.nombreCmdCartes != 0 ? isFloat(nombreCardNotAffected) ? nombreCardNotAffected = nombreCardNotAffected.toFixed(2) : nombreCardNotAffected : nombreCardNotAffected = 0;
        $('#nombreCardNotAffected').html(item.nombreCardNotAffected + ' (' + nombreCardNotAffected + '%)');
        $('#progress_carteNotAffected').html('<div class="progress-bar bg-primary" role="progressbar" style="width: ' + nombreCardNotAffected + '%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>');
        item.totalGopass > 1 ? $('#totalGopass').html(item.totalGopass) : $('#totalGopass').html(item.totalGopass);
//        $('#progress_vouchers').html('<div class="progress-bar bg-danger" role="progressbar" style="width: 100%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>');
        var totalGopassBlancs = (item.totalGopassBlancs / item.totalGopass * 100);
        item.totalGopass != 0 ? (isFloat(totalGopassBlancs) ? totalGopassBlancs = totalGopassBlancs.toFixed(2) : totalGopassBlancs) : totalGopassBlancs = 0;
        $('#totalGopassBlancs').html(item.totalGopassBlancs + ' (' + totalGopassBlancs + '%)');
        $('#progress_vouchersUsed').html('<div class="progress-bar bg-success" role="progressbar" style="width: ' + totalGopassBlancs + '%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>');
        var totalGopassActives = (item.totalGopassActives / item.totalGopass * 100);
        item.totalGopass != 0 ? isFloat(totalGopassActives) ? totalGopassActives = totalGopassActives.toFixed(2) : totalGopassActives : totalGopassActives = 0;
        $('#totalGopassActives').html(item.totalGopassActives + ' (' + totalGopassActives + '%)');
        $('#progress_vouchersUnused').html('<div class="progress-bar bg-primary" role="progressbar" style="width: ' + totalGopassActives + '%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>');
        var totalGopassUtilises = (item.totalGopassUtilises / item.totalGopass * 100);
        item.totalGopass != 0 ? isFloat(totalGopassUtilises) ? totalGopassUtilises = totalGopassUtilises.toFixed(2) : totalGopassUtilises : totalGopassUtilises = 0;
        $('#totalGopassUtilises').html(item.totalGopassUtilises + ' (' + totalGopassUtilises + '%)');
        $('#progress_vouchersUnused').html('<div class="progress-bar bg-primary" role="progressbar" style="width: ' + totalGopassUtilises + '%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>');
        var nombreVoucherAffected = item.nombreVoucherAffected / item.totalGopass * 100;
        item.totalGopass != 0 ? isFloat(nombreVoucherAffected) ? nombreVoucherAffected = nombreVoucherAffected.toFixed(2) : nombreVoucherAffected : nombreVoucherAffected = 0;
        $('#nombreVoucherAffected').html(item.nombreVoucherAffected + ' (' + nombreVoucherAffected + '%)');
        $('#progress_vouchersAffected').html('<div class="progress-bar bg-warning" role="progressbar" style="width: ' + nombreVoucherAffected + '%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>');
        var nombreVoucherNotAffected = item.nombreVoucherNotAffected / item.totalGopass * 100;
        item.totalGopass != 0 ? isFloat(nombreVoucherNotAffected) ? nombreVoucherNotAffected = nombreVoucherNotAffected.toFixed(2) : nombreVoucherNotAffected : nombreVoucherNotAffected = 0;
        $('#nombreVoucherNotAffected').html(item.nombreVoucherNotAffected + ' (' + nombreVoucherNotAffected + '%)');
        $('#progress_vouchersNotAffected').html('<div class="progress-bar bg-info" role="progressbar" style="width: ' + nombreVoucherNotAffected + '%;" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100"></div>');
    });
}