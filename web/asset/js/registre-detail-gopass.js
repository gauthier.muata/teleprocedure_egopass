var detailVoucherList = [],
        listCheckedVoucher = [];
var host = window.location.origin;

$(function () {
    checkIfConnexionOn();
    setMenu();
    $('#setUsedVoucher').attr('checked');
    $('#setUnusedVoucher').attr('checked');

    sendData('v1/getCommandesGoPass/' + userNif + '', 'GET', 'getCommandesGoPass');
});

function getDetails(id) {
    sendData('v1/getDetailsGoPassByCommandeId/' + id + '', 'GET', 'getDetailsGoPassByCommandeId');
}

function sendData(url, method, operation) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + url,
        crossDomain: true,
        method: method,
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        }

        switch (operation) {
            case 'getCommandesGoPass':
                setTableDataCommande(data);
                break;
            case 'getDetailsGoPassByCommandeId':
                detailVoucherList = data;
                if (data == '0') {
                    Swal.fire({
                        title: "eGoPass",
                        text: "Aucun d\351tail n'est disponible actuellement. Contactez l'administrateur pour la g\351n\351ration de vos codes gopass.",
                        type: "warning",
                        confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
                    });
                    return;
                }
                setTableDataDetailVoucher(data);
                break;
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessageOnRequest();
    }).always(function () {
        KTApp.unblockPage();
    });
}

function setTableDataCommande(data) {

    var numero = 0, nbre_voucher = 0;
    var tableContent = '';
    tableContent += '<thead style="background-color:#5867dd;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="color:white">#</th>';
    tableContent += '<th style="text-align:left; color:white">REFERENCE</th>';
    tableContent += '<th style="text-align:left; color:white">CATEGORIE</th>';
    tableContent += '<th style="text-align:left; color:white">TYPE</th>';
    tableContent += '<th style="text-align:left; color:white">QUANTITE</th>';
    tableContent += '<th style="text-align:left; color:white"></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    for (var i = 0; i < data.length; i++) {
        numero += 1;

        if (data[i].etat === 1) {
            nbre_voucher += data[i].nombre;
            tableContent += '<tr>';
            tableContent += '<td style="vertical-align:middle;">' + numero + '</td>';
            tableContent += '<td style="vertical-align:middle;">' + data[i].reference + '</td>';
            tableContent += '<td style="vertical-align:middle;">' + data[i].categorie + '</td>';
            tableContent += '<td style="vertical-align:middle;text-align:center">' + data[i].type + '</td>';
            tableContent += '<td style="vertical-align:middle;text-align:right">' + data[i].nombre + '</td>';
            tableContent += '<td><button type="button" class="btn btn-secondary" onclick="getDetails(\'' + data[i].id + '\')" title="Voir les détails"><i class="flaticon2-soft-icons"></i>Voir détails</button></td>';
            tableContent += '</tr>';
        }
    }
    tableContent += '</tbody>';

    $('.total-voucher').html('<div class="list-group-item" style="color: green">' + nbre_voucher + '</div>');

    $('#table_detail_voucher').html(tableContent);
    $('#table_detail_voucher').DataTable({
        "order": [[1, "asc"]],
        destroy: true,
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
//            emptyTable: "Aucune donnée disponible",
            emptyTable: "Aucun voucher disponible",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });

}

function setTableDataDetailVoucher(data) {

    var numero = 0, usedVoucherNber = 0, unusedVoucherNber = 0;
    var tableContent = '';
    tableContent += '<thead style="background-color:#5867dd;color:white">';
    tableContent += '<tr>';
//    tableContent += '<th style="color:white">#</th>';
    tableContent += '<th style="text-align:left; color:white"><small>CODE</small></th>';
    tableContent += '<th style="text-align:left; color:white"><small>ETAT</small></th>';
    tableContent += '<th style="text-align:left; color:white"><small>DATE ACTIVATION</small></th>';
    tableContent += '<th style="text-align:left; color:white"><small>DATE UTILISATION</small></th>';
    tableContent += '<th style="text-align:left; color:white"><small>TICKET</small></th>';
    tableContent += '<th style="text-align:left; color:white"><small>PROVENANCE</small></th>';
    tableContent += '<th style="text-align:left; color:white"><small>DESTINATION</small></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    $.each(data, function (index, item) {
        numero += 1;
        var etat = '<td style="vertical-align:middle;color:blue">BLANC</td>';
        var aeroProvenance = '<td style="vertical-align:middle;">-</td>';
        var aeroDestination = '<td style="vertical-align:middle;">-</td>';

        if (item.dateUtilisation !== '-') {
            usedVoucherNber++;
            etat = '<td style="vertical-align:middle;color:red"><small>UTILISE</small></td>';
            aeroDestination = '<td style="vertical-align:middle;"><small>' + item.aeroportDestination + '</small></td>';
        } else {
            unusedVoucherNber++;
            if (item.etat === 2) {
                etat = '<td style="vertical-align:middle;color:blue"><small>BLANC</small></td>';
            } else if (item.etat === 1) {
                etat = '<td style="vertical-align:middle;color:blue">ACTIVE</td>';
            }
            aeroProvenance = '<td style="vertical-align:middle;"><small>' + item.aeroportProvenance + '</small></td>';
        }
        tableContent += '<tr>';
//        tableContent += '<td style="vertical-align:middle;">' + numero + '</td>';
        tableContent += '<td style="vertical-align:middle; "><small>' + item.codeGopass + '</small></td>';
        tableContent += etat;
        tableContent += '<td style="vertical-align:middle;"><small>' + item.dateActivation + '</small></td>';
        tableContent += '<td style="vertical-align:middle;"><small>' + item.dateUtilisation + '</small></td>';
        tableContent += '<td style="vertical-align:middle;"><small>' + item.codeTicket + '</small></td>';
        tableContent += aeroProvenance;
        tableContent += aeroDestination;
        tableContent += '</tr>';
    });

    tableContent += '</tbody>';

    $('.div_info_voucher').removeClass('hidden');
    $('.info-used-voucher').html('<div class="list-group-item" style="color: red">' + usedVoucherNber + '</div>');
    $('.info-unused-voucher').html('<div class="list-group-item" style="color: green">' + unusedVoucherNber + '</div>');
    $('#table_dcv').html(tableContent);
    $('#table_dcv').DataTable({
        "order": [[1, "asc"]],
        destroy: true,
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
//    setVoucherUsed();
    $('.modalDetailsCommande').modal('show');

}

function getActivate(id) {
    alert(id);
}

function setVoucherUsed() {

    var j = 0;
    listCheckedVoucher.length = 0;

    var isUsed = $('#setUsedVoucher:checkbox:checked').length > 0;
    var isUnused = $('#setUnusedVoucher:checkbox:checked').length > 0;

    $.each(detailVoucherList, function (index, item) {
        if (isUsed && isUnused) {
            listCheckedVoucher[index] = detailVoucherList[index];
        } else if (isUsed && !isUnused) {
            if (item.dateUtilisation !== '-') {
                listCheckedVoucher[j] = detailVoucherList[index];
                j++;
            }
        } else if (!isUsed && !isUnused) {
            listCheckedVoucher = [];
        } else if (!isUsed && isUnused) {
            if (item.dateUtilisation === '-') {
                listCheckedVoucher[j] = detailVoucherList[index];
                j++;
            }
        }
    });

    setTableDataDetailVoucher(listCheckedVoucher);
}