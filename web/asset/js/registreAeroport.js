/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var urlPhoto, FileName;
var len = 0;
var dataImage = new Array();
var dataImageVisualiser = new Array();

var url2, FileName2;
var fileExtensionPhoto = "";
var fileExtensionEmpreinte = "";

$(function () {


    LoadAeroport();


    $('#btnAddCompagnie').on('click', function (e) {
        e.preventDefault();
        $('.addnewcompagnie').modal('show');
    });




    $('#btnSaveCompagnie').on('click', function (e) {
        e.preventDefault();
        dataPut = {};
        dataPut.methode = 'v1/savecompagnie';
        dataPut.dataType = 'JSON';
        dataPut.type = 'POST';

        dataparam = {};
        dataparam.intitule = $('#IntituleCompagnie').val();
        dataparam.sigle = $('#IntituleCompagnie').val();
        dataparam.pathPhoto = urlPhoto;
        dataparam.ExtPhoto = fileExtensionPhoto;
        dataparam.AgentCreat = userNif;
        dataPut.loadmessage = "Enregistrement en cours ..."
        sendDataCompagnie(dataPut, dataparam)
    });

});

function LoadAeroport()
{
    dataPut = {};
    dataPut.methode = 'v1/getaeroportList';
    dataPut.dataType = 'json';
    dataPut.loadmessage = 'Chargement en cours ...'
    dataPut.type = 'GET';
    sendDataCompagnie(dataPut, "")
}

function sendDataCompagnie(dataPut, dataparam) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: dataPut.loadmessage
    });

    $.ajax(
            {type: dataPut.type,
                url: baseUrl + dataPut.methode + '',
                dataType: dataPut.dataType,
                headers: headers,
                crossDomain: true,
                data: JSON.stringify(dataparam),
                success: function (response)
                {
                    setTimeout(function () {

                        callbackData(response, dataPut.methode);
                    }
                    , 5000);
                },
                complete: function () {

                },
                error: function (xhr, status, error) {
                    KTApp.unblockPage();
                    swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})
                }
            });
}

function callbackData(response, methode) {

    switch (methode) {
        case 'v1/getaeroportList':

            var ListAeroportDataList = JSON.parse(JSON.stringify(response));
            if (response == "-1") {
                swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})

            }
            printAeroport(ListAeroportDataList);
            break;

        case 'v1/savecompagnie':

            var returns = JSON.parse(JSON.stringify(response));
            if (response == "-1") {
                swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})

            } else {
                swal({text: "La compagnie a été créée avec succès !", icon: "info"})

            }
            LoadCompagnie();
            KTApp.unblockPage();
            break;
    }
}

function printAeroport(ListAeroportDataList) {

    var header = '<thead style="background-color:#5867dd;color:white"><tr><th style="width:15%;display:none">IdAeroport</th>';
    header += '<th style="width:5%;color:white"><b>N°</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:35%;color:white"><b>INTITULE</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:35%;color:white"><b>VILLE</b></th>';
    header += '<th class="d-none d-md-table-cell" style="width:5%;;color:white"><b>ACTION</b></th>';
    header += '</tr> </thead>';

    var body = '<tbody>';

    for (var i = 0; i < ListAeroportDataList.length; i++) {

        body += '<td style="display:none">' + ListAeroportDataList[i].IdAeroport + '</td>';
        body += '<td><b>' + (i + 1) + '</b></td>';
        body += '<td>CODE IATA : <b>' + ListAeroportDataList[i].Code + '<br><p style="color:orange">' + ListAeroportDataList[i].Intitule + '</p></b></td>';
        if (ListAeroportDataList[i].ProvinceIntitule != "")
        {
            body += '<td><b style="color:green">' + ListAeroportDataList[i].VilleIntitule + '</b><br>Province : ' + ListAeroportDataList[i].ProvinceIntitule + '<br> ' + ListAeroportDataList[i].PaysIntitule + ' (' + ListAeroportDataList[i].ContinentIntitule + ')</b></td>';
        }
        else if (ListAeroportDataList[i].ProvinceIntitule == "")
        {
            body += '<td><b style="color:green">' + ListAeroportDataList[i].VilleIntitule + '</b>' + '<br>' + ListAeroportDataList[i].PaysIntitule + ' (' + ListAeroportDataList[i].ContinentIntitule + ')</b></td>';
        }
        body += '<td><b><a href="#"><i class="align-middle fas fa-fw fa-edit"></i></a></b></td>'
        body += '</tr>';
    }

    body += '</tbody>';
    var tableContent = header + body;
    $('#table_aeroport').html(tableContent);
    var dtCompagnie = $('#table_aeroport').DataTable({
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible pour le critère sélectionné",
            search: "Recherche _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        },
        info: false,
        destroy: true,
        searching: true,
        paging: true,
        lengthChange: false,
        tracking: false,
        ordering: false,
        pageLength: 20,
        lengthMenu: [[7, 10, 25, 50, -1], [7, 10, 25, 50, "Tout"]],
        select: {
            style: 'os',
            blurable: true
        },
        datalength: 3
    });
    KTApp.unblockPage();
}

