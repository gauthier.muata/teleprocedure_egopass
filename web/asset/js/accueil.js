/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {

    initDataDashboard();
    
});

function initDataDashboard() {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + 'v1/getAccueilGoPass',
        crossDomain: true,
        method: 'GET',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        } else if (data == "0") {
            return;
        }

        console.log(data)

        setDataDashboard(data);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessageOnRequest();
    }).always(function () {
        KTApp.unblockPage();
    });
}

function setDataDashboard(data) {
    $.each(data, function (index, item) {
        $('.nbreCmdGopass').html(item.totalCommande);
        $('.nbreCmdCarte').html(item.totalCommandeCarte);
        $('.nbreAbonne').html(item.totalAbonne);
        $('.nbreGopass').html(item.totalGopass);
    });
}
