/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var ListCompagnieDataList
var ListAeroportDataList
var Reference;
var TypeGoPasse;
var Tarif;
var Type;
var AeroportSimilar = 0;
var tempGoPass = []
var code_provenance = '';
var code_destination = '';
var urlPhoto = '';
var urlPhotoPdf = '';
var base64Image;
$(function () {

    LoadCompagnie();
    LoadAeroport();

    Reference = atob(getUrlParameter('id')).split('@')[0];
    TypeGoPasse = atob(getUrlParameter('id')).split('@')[1];
    Type = atob(getUrlParameter('id')).split('@')[2];
    Tarif = atob(getUrlParameter('id')).split('@')[3];
    $('#RefGopass').html('Go-Passe Référence : <b style="Color:red">' + Reference + '</b> <b style ="float:right"><b style="Color:green">' + TypeGoPasse)

    $('#cmb_compagnie').on('change', function (e)
    {
        e.preventDefault();
        for (var i = 0; i < ListCompagnieDataList.length; i++) {
            if (ListCompagnieDataList[i].IdCompagnie == $('#cmb_compagnie').val()) {
                urlPhoto = 'background-image:' + ' url(' + ListCompagnieDataList[i].Logo + ')';
                urlPhotoPdf = ListCompagnieDataList[i].Logo
                $('#Logo').attr('style', "background-size: 50%; margin:auto;width : 80%; height : 5em;background-repeat: no-repeat;background-position: center;  " + urlPhoto);

            }
        }
    });

    $('#cmb_aeroport_provenance').on('change', function (e)
    {
        e.preventDefault();
        if ($('#cmb_aeroport_destination').val() == $('#cmb_aeroport_provenance').val()) {
            swal({text: "L'aéroport de destination ne peut pas être identique à l'aeéoport de provenance", icon: "warning"})
            AeroportSimilar = 1;
            $('#DetNational').html('');
            return;
        }
        for (var i = 0; i < ListAeroportDataList.length; i++) {
            if (ListAeroportDataList[i].IdAeroport == $('#cmb_aeroport_provenance').val()) {
                if (ListAeroportDataList[i].ProvinceIntitule != "")
                {
                    $('#DetNational').html('Ville : <b style="color:green">' + ListAeroportDataList[i].VilleIntitule + '</b><br>Province : ' + ListAeroportDataList[i].ProvinceIntitule + ' -- ' + ListAeroportDataList[i].PaysIntitule + '<br>' + ListAeroportDataList[i].ContinentIntitule + '</b>');
                    code_provenance = ListAeroportDataList[i].Code
                }
                else if (ListAeroportDataList[i].ProvinceIntitule == "")
                {
                    $('#DetNational').html('Ville : <b style="color:green">' + ListAeroportDataList[i].VilleIntitule + '</b>' + '<br>' + ListAeroportDataList[i].PaysIntitule + '<br>' + ListAeroportDataList[i].ContinentIntitule + '</b>');
                    code_provenance = ListAeroportDataList[i].Code
                }
                AeroportSimilar = 0;
            }
        }
    });

    $('#cmb_aeroport_destination').on('change', function (e)
    {
        e.preventDefault();
        if ($('#cmb_aeroport_destination').val() == $('#cmb_aeroport_provenance').val()) {
            swal({text: "L'aéroport de destination ne peut pas être identique à l'aeéoport de provenance", icon: "warning"})
            AeroportSimilar = 1;
            $('#DetInternational').html('');
            return;
        }
        for (var i = 0; i < ListAeroportDataList.length; i++) {
            if (ListAeroportDataList[i].IdAeroport == $('#cmb_aeroport_destination').val()) {
                if (ListAeroportDataList[i].ProvinceIntitule != "")
                {
                    $('#DetInternational').html('Ville : <b style="color:green">' + ListAeroportDataList[i].VilleIntitule + '</b><br>Province : ' + ListAeroportDataList[i].ProvinceIntitule + ' -- ' + ListAeroportDataList[i].PaysIntitule + '<br>' + ListAeroportDataList[i].ContinentIntitule + '</b>');
                    code_destination = ListAeroportDataList[i].Code
                }
                else if (ListAeroportDataList[i].ProvinceIntitule == "")
                {
                    $('#DetInternational').html('Ville  : <b style="color:green">' + ListAeroportDataList[i].VilleIntitule + '</b>' + '<br>Pays : ' + ListAeroportDataList[i].PaysIntitule + ' <br>' + ListAeroportDataList[i].ContinentIntitule + '</b>');
                    code_destination = ListAeroportDataList[i].Code
                }
                AeroportSimilar = 0;
            }
        }
    });

    $('#btnActiveGoPass').on('click', function (e) {
        e.preventDefault();
        if ($('#cmb_aeroport_provenance').val() == null) {
            swal({text: "Veuillez sélectionner l'aéroport de provenance", icon: "warning"})
            return;
        }
        if ($('#cmb_aeroport_destination').val() == null) {
            swal({text: "Veuillez sélectionner l'aéroport de destination", icon: "warning"})
            return;
        }
        if (AeroportSimilar == '1') {
            swal({text: "L'aéroport de destination ne peut pas être identique à l'aeéoport de provenance", icon: "warning"})
            return;
        }
        if ($('#cmb_compagnie').val() == null) {
            swal({text: "Veuillez sélectionner une compagnie", icon: "warning"})
            return;
        }
        if ($('#Prenom').val() == '') {
            swal({text: "Veuillez saisir le prenom, il est obligatoire", icon: "warning"})
            return;
        }
        if ($('#Nom').val() == '') {
            swal({text: "Veuillez saisir le nom, il est obligatoire", icon: "warning"})
            return;
        }
        if ($('#cmb_Sexe').val() == null) {
            swal({text: "Veuillez sélectionner le sexe ", icon: "warning"})
            return;
        }

        dataPut = {};
        dataPut.methode = 'v1/activegopass';
        dataPut.dataType = 'JSON';
        dataPut.type = 'POST';

        dataparam = {};
        dataparam.CodeGoPass = Reference;
        dataparam.Passager = $('#Prenom').val() + ' ' + $('#Nom').val() + ' ' + $('#Postnom').val();
        dataparam.SexePassager = $('#cmb_Sexe').val();
        dataparam.FkCompagnie = $('#cmb_compagnie').val();
        dataparam.AgentCreat = userNif;
        dataparam.DateVol = $('#DateVol').val() + ' ' + $('#HeureVol').val() + ':00';
        dataparam.NumeroVol = $('#NumeroVol').val();
        dataparam.FkAeroportProvenance = $('#cmb_aeroport_provenance').val();
        dataparam.FkAeroportDestination = $('#cmb_aeroport_destination').val();
        dataPut.loadmessage = "Activation en cours ..."
        sendDataActivationGoPass(dataPut, dataparam)
    });

    $('#btnprint').on('click', function (e) {
        e.preventDefault();
        printGoPass();
    });

    $('#SendWhatsapp').on('click', function (e) {
        e.preventDefault();
        document.getElementById('TxtNumero').style.display = "none";
        document.getElementById('SendWhatsapp').style.display = "none";
        document.getElementById('DivPhoto').style.display = "none";
        var textMessage = 'Bonjour, Voici le code de votre go-pass ' + TypeGoPasse + ' : *' + Reference + '* pour le vol de *' + $('#cmb_compagnie option:selected').text().toUpperCase() + '* à destination de l\'' + $('#cmb_aeroport_destination option:selected').text();
        ;
        var win = window.open('https://api.whatsapp.com/send?text=' + textMessage + '&phone=' + $('#TxtNumero').val(), '_blank');
        win.focus();

    });

    $('#SendMail').on('click', function (e) {
        e.preventDefault();
        document.getElementById('DivPhoto2').style.display = "none";
        var Paragraph1 = 'Bonjour ' + $('#Prenom').val() + ' ' + $('#Nom').val() + ' ' + $('#Postnom').val() + ', Votre Go-Pass ' + TypeGoPasse + ' a été activé avec succès, voici son code' + ' : <b>' + Reference + '</b> pour le vol de ' + $('#cmb_compagnie option:selected').text().toUpperCase() + ' à destination de l\'' + $('#cmb_aeroport_destination option:selected').text();
        var Paragraph2 = 'Nous vous remercions de votre confiance'
        dataPut = {};
        dataPut.methode = 'v1/sendMail';
        dataPut.dataType = 'JSON';
        dataPut.type = 'POST';

        dataparam = {};
        dataparam.Paragraph1 = Paragraph1;
        dataparam.Paragraph2 = Paragraph2;
        dataparam.MailDestinataire = $('#AdresseMail').val();
        dataparam.ObjectMessage = "GO-PASS N° " + Reference;
        dataparam.HeaderMessage = "GO-PASS " + $('#cmb_aeroport_provenance option:selected').text().toUpperCase();

        dataPut.loadmessage = "Envoi en cours ..."

        sendDataActivationGoPass(dataPut, dataparam)


    });

    $("#modalprintGopass").on("hidden.bs.modal", function () {
        window.close();
    });

});

function LoadCompagnie()
{
    dataPut = {};
    dataPut.methode = 'v1/getcompagnieList';
    dataPut.dataType = 'json';
    dataPut.loadmessage = 'Chargement en cours ...'
    dataPut.type = 'GET';
    sendDataActivationGoPass(dataPut, "")
}

function LoadAeroport()
{
    dataPut = {};
    dataPut.methode = 'v1/getaeroportList';
    dataPut.dataType = 'json';
    dataPut.loadmessage = 'Chargement en cours ...'
    dataPut.type = 'GET';
    sendDataActivationGoPass(dataPut, "")
}

function sendDataActivationGoPass(dataPut, dataparam) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: dataPut.loadmessage
    });

    $.ajax({type: dataPut.type,
        url: baseUrl + dataPut.methode + '',
        dataType: dataPut.dataType,
        headers: headers,
        crossDomain: true,
        data: JSON.stringify(dataparam),
        success: function (response)
        {
            setTimeout(function () {

                callbackData(response, dataPut.methode);
            }
            , 5000);
        },
        complete: function () {

        },
        error: function (xhr, status, error) {
            KTApp.unblockPage();
            swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})
        }
    });
}

function callbackData(response, methode) {

    switch (methode) {
        case 'v1/getcompagnieList':

            ListCompagnieDataList = JSON.parse(JSON.stringify(response));
            if (response == "-1") {
                swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})

            }
            printCompagnie(ListCompagnieDataList);
            break;

        case 'v1/getaeroportList':

            ListAeroportDataList = JSON.parse(JSON.stringify(response));
            if (response == "-1") {
                swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})

            }
            printAeroport(ListAeroportDataList);
            break;
        case 'v1/activegopass':

            switch (JSON.stringify(response)) {
                case '1':
                    swal({
                        text: "Go-Pass activé avec succès !",
                        icon: "success"
                    }).then(function () {
                        KTApp.unblockPage();
                        var urlOpen = 'http://192.168.43.34:8080/teleprocedure_egopass/gopass-tracking?id=';
                        $('#test').qrcode({width: 120, height: 120, text: urlOpen + btoa('' + Reference + '@')})
                        $('.modalprintGopass').modal('show');

                        base64Image = $('#test > img').attr('src');

                    });

                    break;
                case '0':
                    swal({
                        text: "Ce Go-Pass n'existe pas dans le système !",
                        icon: "error"
                    }).then(function () {
                        KTApp.unblockPage();
                        window.close();
                    });
                    break;
                case '2':
//                    swal({
//                        text: "Ce Go-Pass a déjà été affecté à un vol. Impossible de l'activer de nouveau ! ",
//                        icon: "warning"
//                    }).then(function () {
//                        KTApp.unblockPage();
//                        window.close();
//                    });
//                    break;

                    swal({
                        text: "Go-Pass activé avec succès !",
                        icon: "success"
                    }).then(function () {
                        KTApp.unblockPage();
                        var urlOpen = 'http://192.168.43.34:8080/teleprocedure_egopass/gopass-tracking?id=';
                        $('#test').qrcode({width: 120, height: 120, text: urlOpen + btoa('' + Reference + '@')})
                        
                        var dataUrl = qrcode.canvas(urlOpen + btoa('' + Reference + '@')).toDataURL('image/jpeg');
                        
                        $('.modalprintGopass').modal('show');

                        var image = new Image();
                        var canvas = document.createElement('canvas');
                        image.src = canvas.toDataURL("img/png");
                        base64Image =dataUrl
                    });
            }

            KTApp.unblockPage();
            break;
    }
    KTApp.unblockPage();
}

function printCompagnie(ListCompagnieDataList) {
    var data = '<option value="0" selected disabled>S&eacute;lectionner une compagnie</option>'
    for (var i = 0; i < ListCompagnieDataList.length; i++) {
        data += '<option value="' + ListCompagnieDataList[i].IdCompagnie + '">' + ListCompagnieDataList[i].Intitule + '</td>';
    }
    $('#cmb_compagnie').html(data);
    KTApp.unblockPage();
}

function printAeroport(ListAeroportDataList) {
    var dataNational = '<option value="0" selected disabled>S&eacute;lectionner un aéroport</option>'
    var dataInternational = '<option value="0" selected disabled>S&eacute;lectionner un aéroport</option>'
    for (var i = 0; i < ListAeroportDataList.length; i++) {
        if (ListAeroportDataList[i].ProvinceCode != "") {
            dataNational += '<option value="' + ListAeroportDataList[i].IdAeroport + '">' + ListAeroportDataList[i].Intitule + ' (' + ListAeroportDataList[i].Code + ')</option>';
        }
        else if (ListAeroportDataList[i].ProvinceCode == "") {
            dataInternational += '<option value="' + ListAeroportDataList[i].IdAeroport + '">' + ListAeroportDataList[i].Intitule + ' (' + ListAeroportDataList[i].Code + ')</option>';
        }
    }
    $('#cmb_aeroport_provenance').html(dataNational);
    if (TypeGoPasse == 'International') {
        $('#cmb_aeroport_destination').html(dataInternational);
    }
    else if (TypeGoPasse == 'National') {
        $('#cmb_aeroport_destination').html(dataNational);
    }

    KTApp.unblockPage();
}


function printGoPass() {
    var registreGoPass = new Object();
    tempGoPass = [];
    registreGoPass.Passager = $('#Prenom').val() + ' ' + $('#Nom').val() + ' ' + $('#Postnom').val();
    registreGoPass.Sexe = $('#cmb_Sexe').val();
    registreGoPass.Tarif = Tarif;
    registreGoPass.Type = Type;
    registreGoPass.Date = $('#DateVol').val() + ' à ' + $('#HeureVol').val();
    tempGoPass.push(registreGoPass);
    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('p', 'pt', 'A4');
    var docTitle, position, hauteur = 155;

    var columns = [], columnStyles;
    var columns2 = [], columnStyles2;
    var carEspace = '';
    hauteur = 160;


    hauteur = 180;
    position = 40;
    carEspace = '                                                        ';
    docTitle = '\n ' + carEspace + $('#cmb_aeroport_provenance option:selected').text().toUpperCase();



    columnsHeaderUsd = [
        {title: "IDENTITE PASSAGER", dataKey: "Passager"},
        {title: "SEXE", dataKey: "Sexe"},
        {title: "TYPE", dataKey: "Type"},
        {title: "DATE VOL", dataKey: "Date"},
        {title: "VALEUR", dataKey: "Tarif"}];

    columnStylesHeaderUsd = {
        Passager: {columnWidth: 150, overflow: 'linebreak', fontSize: 7, lineWidth: 1, lineColor: [0, 0, 0]},
        Sexe: {columnWidth: 90, fontSize: 7, overflow: 'linebreak', lineWidth: 1, lineColor: [0, 0, 0]},
        Type: {columnWidth: 90, overflow: 'linebreak', fontSize: 7, lineWidth: 1, lineColor: [0, 0, 0]},
        Date: {columnWidth: 90, overflow: 'linebreak', fontSize: 7, lineWidth: 1, lineColor: [0, 0, 0]},
        Tarif: {columnWidth: 100, overflow: 'linebreak', fontSize: 7, lineWidth: 1, lineColor: [0, 0, 0]}


    };

    columnsCdf = [
        {title: 'I.DE.F GO-PASS ' + TypeGoPasse.toUpperCase() + ' |  REFERENCE : ' + Reference, dataKey: "SNum"}];

    columnStylesCdf = {
        SNum: {columnWidth: 520, overflow: 'linebreak', fontSize: 7}
    };




    var rows2 = tempGoPass;
    var rows = [];
    var pageContent = function (data) {

        var bureau = $('.cmbBureau option:selected').text();
//        setDocParams(doc, docTitle, position, day, month, year, hh, mm, ss, data, bureau);
        var lenBureau = bureau.toUpperCase().length;
        var textRepublique = '';
        var lenRepub = textRepublique.length;
        var lenDebut1 = 0, lenDebut2 = 0, lenPosition1 = 0, lenPosition2 = 0;
        if (lenBureau > lenRepub) {
            lenPosition1 = lenBureau - lenRepub;
            if (lenPosition1 % 2 == 0) {
                lenDebut1 = lenPosition1 / 2;
            } else {
                lenDebut1 = (lenPosition1 + 1) / 2;
            }
        } else {
            lenPosition2 = lenRepub - lenBureau;
            if (lenPosition2 % 2 == 0) {
                lenDebut2 = lenPosition2 / 2 + 7;
            } else {
                lenDebut2 = (lenPosition2 + 1) / 2 + 7;
            }
        }

        doc.setFontType("bold");
        doc.setFontSize(5);
        doc.text(textRepublique, 40 + (lenPosition1 * 2) + lenDebut1, 25);
        doc.setFontSize(9);
        doc.text("", 73 + (lenPosition1 * 2) + lenDebut1, 40);
        doc.setFontSize(10);
        doc.text(bureau.toUpperCase(), 40 + (lenPosition2 * 2) + lenDebut2, 55);
        doc.text(docTitle, position, 110);
        doc.addImage(docImage, 'PNG', 85 + lenDebut1 + (lenPosition1 * 2), 20, 185, 70);
        doc.addImage(AvionIcon, 'PNG', 275 + lenDebut1 + (lenPosition1 * 2), 250, 60, 50);
        doc.addImage(base64Image, 'PNG', 250 + lenDebut1 + (lenPosition1 * 2), 370, 120, 50);
        doc.text(docTitle, position, 110);
        doc.setFontSize(6);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-PEAGE', 390, 25);
        doc.setFontSize(7);
        doc.text("AEROPORT DE DEPART", 180, 260);
        doc.setFontSize(25);
        doc.text(code_provenance, 200, 285);
        doc.setFontSize(7);
        doc.text("AEROPORT DE DESTINATION", 355, 260);
        doc.setFontSize(25);
        doc.text(code_destination, 355, 285);
        doc.setFontSize(6);
        var sizeLen = $('#cmb_aeroport_provenance option:selected').text().length;
        if (sizeLen < 30)
        {
            doc.text($('#cmb_aeroport_provenance option:selected').text().toUpperCase(), 160, 300);
        }
        else if (sizeLen > 29)
        {
            doc.text($('#cmb_aeroport_provenance option:selected').text().toUpperCase(), 125, 300);
        }
        doc.setFontSize(6);
        doc.text($('#cmb_aeroport_destination option:selected').text().toUpperCase(), 355, 300);
        doc.setFontSize(10);
        doc.text($('#cmb_compagnie option:selected').text().toUpperCase() + '  |  N° VOL : ' + $('#NumeroVol').val().toUpperCase(), 200, 350);


    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columnsCdf, rows, {
        addPageContent: pageContent,
        margin: {top: 139},
        theme: 'grid',
        headerStyles: {
            fillColor: [140, 140, 140],
            valign: 'middle',
            halign: 'center',
            fontSize: 10},
        columnStyles: columnStylesCdf,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 7 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });



    var rows = tempGoPass;
    doc.autoTable(columns2, rows, {
        addPageContent: pageContent,
        margin: {top: 454},
        theme: 'grid',
        headerStyles: {
            fillColor: [140, 140, 140],
            fontSize: 6},
        columnStyles: columnStyles2,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 26 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    doc.autoTable(columnsHeaderUsd, rows2, {
        addPageContent: null,
        margin: {top: 478},
        theme: 'grid',
        headerStyles: {
            fillColor: [77, 77, 77],
            color: [0, 0, 0],
            fontSize: 6},
        columnStyles: columnStylesHeaderUsd,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 7 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });


    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(6);
    doc.setFontStyle('bold');

//    var totCdf = formatNumberOnlyNew(TotalMontantCdf);
//    var totUsd = formatNumberOnlyNew(TotalMontantUsd);
//
//    var PenCdf = formatNumberOnlyNew(PenaliteMontantCdf);
//    var PenUsd = formatNumberOnlyNew(PenaliteMontantUsd);

//    doc.text("TOTAL NOTE  :  " + formatNumberOnlyNew(TotalDec), 265, finalY + 35);
//    doc.text("TOTAL CDF  :  " + totCdf, 420, finalY + 35);
//    doc.text("TOTAL USD :  " + totUsd, 555, finalY + 35);
//    doc.text("TOTAL NOTE PENALITE  :  " + formatNumberOnlyNew(TotalPen), 265, finalY + 50);
//    doc.text("PENALITE CDF  :  " + PenCdf, 420, finalY + 50);
//    doc.text("PENALITE USD :  " + PenUsd, 555, finalY + 50);
//    doc.text(getNameChefBureau(sessionStorage.getItem('BureauId')), 540, finalY + 135);
//    doc.text(getNameChefTaxateur(sessionStorage.getItem('BureauId'), sessionStorage.getItem('TaxeId')), 40, finalY + 135);

//    }
    window.open(doc.output('bloburl'), '_blank');
}
