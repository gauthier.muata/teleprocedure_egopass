/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var cmb_axe = $('#cmb_axe'),
        cmb_tarif = $('#cmb_tarif'),
        cmb_typeGopass = $('#cmb_typeGopass'),
        lbl_axe = $('#lbl_axe'),
        lbl_montant = $('#lbl_montant'),
        input_quantite = $('#input_quantite'),
        btnAddPanier = $('#btnAddPanier'),
        btnAddCommande = $('#btnAddCommande');
var montantTaxe = 0,
        initialTaxe = 0,
        deviseTaxe,
        montantDevideTaxe,
        remise = 0;
var listCategorie = [],
        listeTarif = [],
        dataCommande = [];

$(function () {

    setMenu();
    checkIfConnexionOn();

    sendData('v1/getListTarifsGoPass', 'GET', 'getListTarifsGoPass', '');

    cmb_tarif.on('change', function (event) {
        $('.montant_taxe').html('<div class="list-group-item">0</div>');
        setMontantTaxe(cmb_tarif.val());
    });

    cmb_typeGopass.select2();

    $('.reload').click(function (e) {
        window.location.reload();
    });

    btnAddCommande.click(function (e) {
        saveCommandeGoPass();
    });

    $('#input_quantite').on('keypress', function (event) {
        var regex = new RegExp("^[0-9]+$");
        var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
        if (!regex.test(key)) {
            event.preventDefault();
            return false;
        }
    });

    $('#input_quantite').on('change', function (event) {
        setDataPanierTaxePeage();
    });

    $('#btnAddPanier').on('click', function (event) {
        setDataPanierTaxePeage();
    });

});

function sendData(url, method, operation, data) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + url,
        crossDomain: true,
        method: method,
        contentType: 'application/json',
        data: data,
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        }

        switch (operation) {
            case 'getListTarifsGoPass':
                if (data === "0") {
                    Swal.fire({
                        title: "Commande GoPass",
                        text: "Aucune cat&eacute;gorie de GoPass n'a \351t\351 trouv\351e",
                        type: "warning",
                        confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
                    });
                    return;
                }

                listCategorie = data;
                setCategorieGoPass();
                break;
            case 'saveCommandeGoPass':
                if (data == "0") {
                    Swal.fire({
                        title: "Commande GoPass",
                        text: "Votre commande n'a pas a abouti. Veuillez réessayer.",
                        type: "warning",
                        confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
                    });
                    return;
                } else {

                    var text = "Votre commande de référence <span style=\"color:red; font-weight:bold\">" + data.reference + "</span> a été enregistrée avec succès.</a> Voulez-vous proc\351der au paiement ?";

                    alertify.confirm(text, function () {
                        sendData('v1/getCommandesGoPassByReference/' + data.reference, 'GET', 'getCommandesGoPassByReference', '');
                    });
                }
                break;
            case 'getCommandesGoPassByReference':

                $.each(data, function (index, item) {
                    var objet = {
                        "id": item.id,
                        "commandeList": data
                    };
                    payementData(objet);
                });
                break;
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showErrorMessageOnRequest();
    }).always(function () {
        KTApp.unblockPage();
    });

}

function setTarif(codeCategorie) {

    $.each(listCategorie, function (index, item) {
        if (item.idTarif == codeCategorie) {
            listeTarif = item.listTarif;
            $('.montant_taxe').html('<div class="list-group-item">' + listeTarif[0].taux + ' ' + listeTarif[0].devise + '</div>');
        }
    });

}

function setCategorieGoPass() {

    var option = '<option value="0" selected disabled>S&eacute;lectionner une cat&eacute;gorie v&eacute;hicule</option>';
    $.each(listCategorie, function (index, item) {
        option += '<option value="' + item.idTarif + '">' + item.intituleTarif + '</option>';
    });
    cmb_tarif.html(option);
    cmb_tarif.select2();

}

function setMontantTaxe(codeCategorie) {

    $.each(listCategorie, function (index, item) {
        if (item.idTarif == codeCategorie) {
            montantTaxe = item.listTarif[0].taux;
            deviseTaxe = item.listTarif[0].devise;
            montantDevideTaxe = montantTaxe + ' ' + deviseTaxe;
            $('.montant_taxe').html('<div class="list-group-item">' + item.listTarif[0].taux + ' ' + item.listTarif[0].devise + '</div>');
        }
    });

}

function setDataPanierTaxePeage() {

    if (cmb_tarif.val() == '' || cmb_tarif.val() == null) {
        alertify.alert('Veuillez d\'abord ajouter un tarif.');
        return;
    }
    if (cmb_typeGopass.val() == '' || cmb_typeGopass.val() == null) {
        alertify.alert('Veuillez d\'abord s\351lectionner le type de GoPass.');
        return;
    }
    if (montantTaxe == 0) {
        alertify.alert('Veuillez d\'abord séléctionner une cat&eacute;gorie dont le taux a été fixé.');
        return;
    }
    if (input_quantite.val() == '') {
        alertify.alert('Veuillez d\'abord ajouter une quantité.');
        return;
    }

    var object = {};

    object.typeId = $('#cmb_typeGopass').val();
    object.typeName = $('#cmb_typeGopass option:selected').text();
    object.tarifCode = cmb_tarif.val();
    object.tarifName = $('#cmb_tarif option:selected').text();
    object.montant = montantTaxe;
    object.total = parseFloat(montantTaxe) * parseInt(input_quantite.val());
    object.devise = deviseTaxe;
    object.montantDevideTaxe = montantDevideTaxe;
    object.quantite = input_quantite.val();
    object.nif = userNif;
    object.abCode = AB_GOPASS;
    object.remise = remise;

    if (dataCommande.length !== 0) {
        for (var i = 0; i < dataCommande.length; i++) {
            if (dataCommande[i].typeId === cmb_typeGopass.val() && dataCommande[i].tarifCode === cmb_tarif.val()) {
                dataCommande.splice(i, 1);
            } else {
                if (dataCommande[i].devise != deviseTaxe) {
                    Swal.fire({
                        title: "Commande GoPass",
                        text: "Vous ne pouvez pas mettre dans le panier deux lignes de commande de devises diff\351rentes. Nous vous sugg\351rons de faire une nouvelle commande s'il vous plait.",
                        type: "warning",
                        confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
                    });
                    return;
                }
            }
        }
    }
    dataCommande.push(object);
    setTableDataCommande(dataCommande);

}

function setTableDataCommande(data) {

    var numero = 0, total = 0, montant_cdf = 0, montant_usd = 0, devise_cdf = '', devise_usd = '';
    var tableContent = '';
    tableContent += '<thead style="background-color:#5867dd;color:white">';
    tableContent += '<tr>';
    tableContent += '<th style="color:white">#</th>';
    tableContent += '<th style="text-align:left; color:white">CATEGORIE GOPASS</th>';
    tableContent += '<th style="text-align:left; color:white">TYPE</th>';
    tableContent += '<th style="text-align:left; color:white">MONTANT</th>';
    tableContent += '<th style="text-align:left; color:white">NOMBRE GOPASS</th>';
    tableContent += '<th style="text-align:left; color:white">TOTAL PARTIEL</th>';
    tableContent += '<th></th>';
    tableContent += '</tr>';
    tableContent += '</thead>';
    tableContent += '<tbody>';

    $.each(data, function (index, item) {
        numero += 1;
        total += item.montant * item.quantite;
        if (item.devise === "CDF") {
            montant_cdf += item.montant * item.quantite;
            devise_cdf = 'CDF';
        } else {
            montant_usd += item.montant * item.quantite;
            devise_usd = 'USD';
        }
        tableContent += '<tr>';
        tableContent += '<td>' + numero + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + item.tarifName + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + item.typeName + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + item.montantDevideTaxe + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + item.quantite + '</td>';
        tableContent += '<td style="vertical-align:middle;">' + (item.montant * item.quantite) + ' ' + item.devise + '</td>';
        tableContent += '<td><button type="button" class="btn btn-danger btn-elevate btn-circle btn-icon " onclick="deleteTaxe(\'' + item.typeId + '\',\'' + item.tarifCode + '\')"><i class="flaticon2-cross"></i></button></td>';
        tableContent += '</tr>';
    });

    if (montant_cdf != 0 && montant_usd != 0) {
        $('.total_montant').html('<div class="list-group-item" style="color:blue; font-weight:bold; font-size: 15px">' + devise_usd + ' ' + montant_usd + ' et ' + devise_cdf + ' ' + montant_cdf + '</div>');
    } else {
        $('.total_montant').html('<div class="list-group-item" style="color:blue; font-weight:bold; font-size: 15px">' + total + ' ' + deviseTaxe + '</div>');
    }

    tableContent += '</tbody>';

    tableContent += '<tfoot>';
    tableContent += '<tr>';
    tableContent += '<th colspan="5" class="text-right label-general pr-3" style="color:#fd397a"> NET A PAYER</th>';
    tableContent += '<th colspan="2"><span class="general" style="color:green;font-size:14px"></span> </th>';
    tableContent += '</tr>';
    tableContent += '</tfoot>';

    $('#panierCommndeTaxe').html(tableContent);
    $('#panierCommndeTaxe').DataTable({
        "order": [[1, "asc"]],
        destroy: true,
        language: {
            processing: "Traitement en cours...",
            track: "Rechercher&nbsp;:",
            lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
            info: "Affichage de _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
            infoEmpty: "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
            infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
            infoPostFix: "",
            loadingRecords: "Chargement en cours...",
            zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
            emptyTable: "Aucune donnée disponible",
            search: "Rechercher _INPUT_  ",
            paginate: {
                first: "Premier",
                previous: "Pr&eacute;c&eacute;dent",
                next: "Suivant",
                last: "Dernier"
            },
            aria: {
                sortAscending: ": activer pour trier la colonne par ordre croissant",
                sortDescending: ": activer pour trier la colonne par ordre décroissant"
            }
        }
    });
    $('.general').html(formatNumber(total, deviseTaxe));

}

function deleteTaxe(typeId, codeTarif) {

    alertify.confirm('Etes-vous sûr de vouloir supprimer cette ligne de commande ?', function () {

        for (var i = 0; i < dataCommande.length; i++) {
            if (dataCommande[i].typeId === typeId && dataCommande[i].tarifCode === codeTarif) {
                dataCommande.splice(i, 1);
                setTableDataCommande(dataCommande);
            }
        }
    });

}

function saveCommandeGoPass() {

    if (dataCommande.length == 0) {
        alertify.alert('Veuillez d\'abord ajouter au moins une ligne de commande dans le panier avant de faire cette action.');
        return;
    }

    var object = {
        'listCommande': JSON.stringify(dataCommande)
    };

    alertify.confirm('Etes-vous sûr de vouloir enregistrer cette commande ?', function () {
        sendData('v1/saveCommandeGoPass', 'POST', 'saveCommandeGoPass', JSON.stringify(object));
    });

}