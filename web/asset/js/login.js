/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var loginform,
        listRetraitDeclaration,
        listBordereau,
        email,
        codeEmail,
        codeUser,
        userData,
        registredeclarationtable,
        code_validation,
        defaultPw;

var cmb_rang = $('#cmb_rang'),
        formLogin = $('#loginform'),
        dvMessageErreur = $('#dvMessageErreur'),
        password = $('#password'),
        confirmpassword = $('#confirmpassword'),
        inputUsername = $('.text-user-primary'),
        inputPassword = $('.text-user-pw-primary'),
        new_password = $('#new_password'),
        confirme_password = $('#confirme_password'),
        resetPassword = $('#resetPassword'),
        btnLogin = $('.btn-primary'),
        btnChangePw = $('#btnChangePw'),
        motpassoublier = $('#motpassoublier'),
        userlogo = $('#userlogo'),
        check_if_assujetti = $('#check_if_assujetti'),
        div_mail = $('.div_mail'),
        div_phone = $('.div_phone');

$(function () {

    if (getNTD() != '') {
        inputUsername.val(getNTD());
    }

    btnLogin.click(function (e) {
        e.preventDefault();

        if (inputUsername.val() === '' || inputUsername.val() == 'null') {
            alertify.alert('Veuillez fournir votre numero de teledeclaration.');
            inputUsername.focus();
            return;
        }

        if (inputPassword.val() === '' || inputPassword.val() == 'null') {
            alertify.alert('Veuillez fournir votre mot de passe.');
            inputPassword.focus();
            return;
        }
        setLoginUser(inputUsername.val(), inputPassword.val());
    });

    motpassoublier.click(function (e) {
        e.preventDefault();
    });

    btnChangePw.on('click', function () {
        if (inputUsername.val() === '' || inputUsername.val() == 'null') {
            alertify.alert('Veuillez fournir votre numero de teledeclaration.');
            inputUsername.focus();
            return;
        }

        if (new_password.val() === '' || new_password.val() == 'null') {
            alertify.alert('Veuillez fournir votre nouvau mot de passe.');
            inputPassword.focus();
            return;
        }

        if (confirme_password.val() == '' || confirme_password.val() == 'null') {
            alertify.alert('Veuillez fournir votre mot de passe de confirmation.');
            inputPassword.focus();
            return;
        }

        if (new_password.val() != confirme_password.val()) {
            alertify.alert('Le nouveau mot de passe et celui de confirmation ne sont pas identiques');
            confirmpassword.focus();
            return;
        }
        if (new_password.val() == defaultPw) {
            alertify.alert('Le nouveau mot de passe doit être différent de l\'ancien.');
            new_password.focus();
            return;
        }

        setNewPassword(inputUsername.val(), confirme_password.val());
    });

    inputUsername.keypress(function (e) {
        if (e.keyCode == 13) {
            btnLogin.trigger('click');
        }
    });

    inputPassword.keypress(function (e) {
        if (e.keyCode == 13) {
            btnLogin.trigger('click');
        }
    });

});

function setLoginUser(username, password) {

    defaultPw = password;

    var object = {
        'ntd': username,
        'password': password,
        'operation': '1'
    };

    $.ajax({
        url: baseUrl + 'v2/getAssujetti',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        if (data == '-1') {
            $.unblockUI();
            showResponseError();
            return;
        }
//        KTApp.unblockPage();
        var result = JSON.parse(JSON.stringify(data));
        if (result.code == 100) {
            if (result.etat == 1) {
                setUserData(JSON.stringify(data));
                sessionStorage.setItem('userData', JSON.stringify(result));
                setNTD('');
                window.location = 'dashboard';
            } else if (result.etat == 2) {
                $('.alert-login').html('Vous êtes déjà un assujetti à la régie et nous vous redirigeons vers une autre page pour vous connecter. Merci.');
                $('.alert-login').removeClass('hidden');
                $('.link_pw').addClass('hidden');
                $('.div_password').addClass('hidden');
                $('.div_new_password').addClass('hidden');
                $('.div_confirm_password').addClass('hidden');
                $('.btnlogin').addClass('hidden');
                $('.btn_change_password').addClass('hidden');
                $('.div_username').addClass('hidden');

                setTimeout(function () {
                    window.location = 'demande_validation';
                }, 3500);

            } else if (result.etat == 4) {
                $('.alert-login').html('Veuillez modifier le mot de passe de votre compte avant d\'acceder dans le système.');
                $('.alert-login').removeClass('hidden');
                $('.link_pw').addClass('hidden');
                $('.div_password').addClass('hidden');
                $('.div_new_password').removeClass('hidden');
                $('.div_confirm_password').removeClass('hidden');
                $('.btnlogin').addClass('hidden');
                $('.btn_change_password').removeClass('hidden');
                inputUsername.attr('disabled', 'disabled');
            } else if (result.etat == 3) {
                $('.alert-login').html('Votre compte en t&eacute;l&eacute;-d&eacute;claration est en attente de validation et vous ne pouvez pas vous connecter dans le syst&egrave;me dans cet &eacute;tat.');
                $('.alert-login').removeClass('hidden');
                $('.link_pw').addClass('hidden');
                $('.link_check_assujetti').addClass('hidden');
                $('.btnlogin').addClass('hidden');
                div_mail.addClass('hidden');
                div_phone.addClass('hidden');
            }
        } else {
            $('.alert-login').addClass('hidden');
            $('.link_pw').removeClass('hidden');
            $('.btnlogin').removeClass('hidden');
            alertify.alert('Votre login et/ou le mot passe sont incorrects');
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
    });

}

function initPassword() {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    var object = {
        'email': email,
        'password': password,
        'idUser': codeUser
    };

    $.ajax({
        url: baseUrl + '/v1/updatePassword',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: {
            'accept': 'application/json',
            'Access-Control-Allow-Origin': '*',
            'id': 1340
        },
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {
        KTApp.unblockPage();
        $('#resetPassword .css-loader').removeClass('hidden');
        if (data === 1) {
            alertify.alert('Modification effectuée avec succès.<br/> Connectez-vous avec votre nouveau mot de passe.');
            setTimeout(function () {
                window.location = 'login';
            }, 2000);

        } else if (data === -6) {
            alertify.alert('Votre mot de passe actuel et celui existant sont les mêmes. Prière de saisir un nouveau mot de passe.');
        } else {
            showResponseError();
        }
        $('#resetPassword .css-loader').addClass('hidden');
    }).fail(function (jqXHR, textStatus, errorThrown) {
        KTApp.unblockPage();
        showResponseError();
    });

}

function setNewPassword(userName, password) {

    var object = {
        'ntd': userName,
        'password': password
    };

    $.ajax({
        url: baseUrl + 'v1/setNewPassword',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        } else if (data == "0") {
            alertify.alert('Echec de la réinitialisation de votre mot de passe. Veuillez ressayer.');
            return;
        }

        alertify.alert('Votre mot de passe a été réinitialisé avec succès. Vous serez rédirigé sur votre tableau de bord');

        setTimeout(function () {
            setLoginUser(userName, password);
        }
        , 4000);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
    });
}