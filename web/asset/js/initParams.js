
var btnInitPassword = $('#btnInitPassword'),
        btnResetPassword = $('#btnResetPassword'),
        newPasswordassword = $('#newPasswordassword'),
        confirmPassword = $('#confirmPassword'),
        codeInit = $('#codeInit');

$(function () {

    var id = getUrlParameter('id');
    if (id !== undefined) {
        $('.resetPassword').addClass('hidden');
        $('.divResetnewPassword').removeClass('hidden');

        btnResetPassword.click(function (e) {
            e.preventDefault();

            if (codeInit.val() === '') {
                alertify.alert("Veuillez saisir le code d\'initialisation.");
                return;
            }

            if (newPasswordassword.val() === '') {
                alertify.alert("Veuillez saisir votre nouveau mot de passe.");
                return;
            }

            if (confirmPassword.val() === '') {
                alertify.alert("Veuillez confirmer votre nouveau mot de passe.");
                return;
            }

            if (newPasswordassword.val().length < 6) {
                alertify.alert('Le nouveau mot de passe doit avoir au moins six (6) caractères.');
                newPasswordassword.val('');
                confirmPassword.val('');
                newPasswordassword.focus();
                return;
            }

            if (confirmPassword.val() !== newPasswordassword.val()) {
                alertify.alert("Les deux mots de passe saisis ne correspondent pas.");
                return;
            }
            var object = new Object();
            object.code = id;
            object.codeInit = codeInit.val();
            object.passWord = newPasswordassword.val();
            sendData('v1/initPassword', 'POST', 'initPassword', JSON.stringify(object));

        });

        return;
    }

    btnInitPassword.click(function (e) {
        e.preventDefault();

        if ($('#inputEmail').val() === '') {
            $('.mail-error').attr('style', 'color:red');
            $('.mail-error').removeClass('hidden');
            return;
        }

        $('.mail-error').addClass('hidden');

        if (!validateEmail($('#inputEmail').val())) {
            alertify.alert("Veuillez saisir une adresse mail valide.");
            return;
        }

        sendData('v1/checkMailValid/' + $('#inputEmail').val(), 'GET', 'checkMailValid', '');

    });

});

function sendData(url, method, operation, data) {

    $.ajax({
        url: baseUrl + url,
        crossDomain: true,
        method: method,
        contentType: 'application/json',
        data: data,
        headers: headers,
        dataType: "JSON"
    }).done(function (data) {

        KTApp.unblockPage();
        if (data === "-1") {
            showErrorMessageOnRequest();
            return;
        }

        switch (operation) {
            case 'checkMailValid':
                switch (data.code) {
                    case 100:
                    case 200:
                        $('.divInputMail').addClass('hidden');
                        $('.divBtnInit').addClass('hidden');
                        $('.divMessageText').removeClass('hidden');
                        $('.divMessageText').html(data.message);
                        $('.divMessageText').addClass(data.alert);
                        break;
                }
                break;
            case 'initPassword':
                switch (data.code) {
                    case 100:
                    case 500:
                        $('.resetPassword').removeClass('hidden');
                        $('.divResetnewPassword').addClass('hidden');
                        $('.divInputMail').addClass('hidden');
                        $('.divBtnInit').addClass('hidden');
                        $('.divMessageText').removeClass('hidden');
                        $('.divMessageText').html(data.message);
                        $('.divMessageText').addClass(data.alert);
                        break;
                    case 200:
                    case 300:
                    case 400:
                        alertify.alert(data.message);
                        break;
                }
                break;
        }
    });

}