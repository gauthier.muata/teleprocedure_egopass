/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var Reference;
var TypeGoPasse;
var Tarif;
var Type;
var AeroportSimilar = 0;
var tempGoPass = []

var ListGoPassDetails;
$(function () {


    Reference = atob(getUrlParameter('id')).split('@')[0];
   
    if (Reference != "")
    {
        dataPut = {};
        dataPut.methode = 'v1/getGoPassByCode';
        dataPut.paramRequest = Reference;
        dataPut.dataType = 'json';
        dataPut.loadmessage = 'Recherche en cours ...'
        dataPut.type = 'GET';
        sendDataTrackingGoPass(dataPut, "");
    }

    $('#btnCheckGoPass').on('click', function (e) {
        e.preventDefault();
        dataPut = {};
        dataPut.methode = 'v1/getGoPassByCode';
        dataPut.paramRequest = $('#CodeGoPass').val();
        dataPut.dataType = 'json';
        dataPut.loadmessage = 'Recherche en cours ...'
        dataPut.type = 'GET';
        sendDataTrackingGoPass(dataPut, "")
    });
    $('#btnPrintGoPass').on('click', function (e) {
        e.preventDefault();
        printGoPass(ListGoPassDetails);
    });

});


function sendDataTrackingGoPass(dataPut, dataparam) {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: dataPut.loadmessage
    });
    var urlMethode = "";
    if (dataPut.methode == 'v1/getGoPassByCode')
    {
        urlMethode = dataPut.methode + '/' + dataPut.paramRequest;
    }
    else
    {
        urlMethode = dataPut.methode;
    }
    $.ajax(
            {type: dataPut.type,
                url: baseUrl + urlMethode,
                dataType: dataPut.dataType,
                headers: headers,
                crossDomain: true,
                data: JSON.stringify(dataparam),
                success: function (response)
                {
                    setTimeout(function () {

                        callbackData(response, dataPut.methode);
                    }
                    , 5000);
                },
                complete: function () {

                },
                error: function (xhr, status, error) {
                    KTApp.unblockPage();
                    swal({text: "Une erreur est survenue, prière de contacter notre équipe de support technique", icon: "error"})
                }
            });
}

function callbackData(response, methode) {

    switch (methode) {

        case 'v1/getGoPassByCode':
            ListGoPassDetails = JSON.parse(JSON.stringify(response));
            switch (JSON.stringify(response)) {
                case '0':
                    swal({
                        text: "Ce Go-Pass n'est pas authentique",
                        icon: "warning"
                    }).then(function () {
                        $('#DetailsGoPassInfo').html('');
                        KTApp.unblockPage();
                    });

                    break;
                case '-1':
                    swal({
                        text: "Une erreur est survenue, prière de contacter notre équipe de support technique",
                        icon: "error"
                    }).then(function () {
                        KTApp.unblockPage();
                        window.close();
                    });
                    break;
                default :
                    switch (JSON.stringify(ListGoPassDetails[0].Etat)) {
                        case '1':
                            swal({
                                text: "Ce Go-Pass n'a pas encore été utilisé, il est encore valide",
                                icon: "info"
                            }).then(function () {
                                $('#DetailsGoPassInfo').html('Code Go-Pass  : <b style="color:green"><br>' + ListGoPassDetails[0].code + ' | ' + ListGoPassDetails[0].intituleTarif + '</b><br>' + '<br>Aéroport de départ : <b><br>' + ListGoPassDetails[0].AeroportDepart + '</b><br><br>Aéroport de destination :<b><br>' + ListGoPassDetails[0].AeroportDestination + '</b><br><br>Date du Vol :<b><br>' + ListGoPassDetails[0].DateVol + '</b><br><br><b style="color : red">Ce Go-Pass n\'est pas encore utilisé</b>');
                                printGoPass(ListGoPassDetails);
                                KTApp.unblockPage();
                            });
                            break;
                        case '2':
                            swal({
                                text: "Ce Go-Pass n'est pas encore activé, il est toujours blanc",
                                icon: "info"
                            }).then(function () {
                                $('#DetailsGoPassInfo').html('');
                                KTApp.unblockPage();
                            });
                            break;
                    }
            }
            KTApp.unblockPage();
            break;
    }
    KTApp.unblockPage();
}

function printGoPass(ListGoPassDetails) {
    var registreGoPass = new Object();
    tempGoPass = [];
    registreGoPass.Passager = ListGoPassDetails[0].Passager;
    registreGoPass.Sexe = ListGoPassDetails[0].Sexe;
    registreGoPass.Tarif = ListGoPassDetails[0].intituleTarif;
    registreGoPass.Type = ListGoPassDetails[0].type;
    registreGoPass.Date = ListGoPassDetails[0].DateVol;
    tempGoPass.push(registreGoPass);
    var date = new Date;
    var day = date.getDate();
    var month = date.getMonth() + 1;
    var year = date.getFullYear();
    var hh = date.getHours();
    var mm = date.getMinutes();
    var ss = date.getSeconds();
    var doc = new jsPDF('p', 'pt', 'A4');
    var docTitle, position, hauteur = 155;

    var columns = [], columnStyles;
    var columns2 = [], columnStyles2;
    var carEspace = '';
    hauteur = 160;


    hauteur = 180;
    position = 40;
    carEspace = '                                                        ';
    docTitle = '\n ' + carEspace + ListGoPassDetails[0].AeroportDepart.toUpperCase();



    columnsHeaderUsd = [
        {title: "IDENTITE PASSAGER", dataKey: "Passager"},
        {title: "SEXE", dataKey: "Sexe"},
        {title: "TYPE", dataKey: "Type"},
        {title: "DATE VOL", dataKey: "Date"},
        {title: "VALEUR", dataKey: "Tarif"}];

    columnStylesHeaderUsd = {
        Passager: {columnWidth: 150, overflow: 'linebreak', fontSize: 7, lineWidth: 1, lineColor: [0, 0, 0]},
        Sexe: {columnWidth: 90, fontSize: 7, overflow: 'linebreak', lineWidth: 1, lineColor: [0, 0, 0]},
        Type: {columnWidth: 90, overflow: 'linebreak', fontSize: 7, lineWidth: 1, lineColor: [0, 0, 0]},
        Date: {columnWidth: 90, overflow: 'linebreak', fontSize: 7, lineWidth: 1, lineColor: [0, 0, 0]},
        Tarif: {columnWidth: 100, overflow: 'linebreak', fontSize: 7, lineWidth: 1, lineColor: [0, 0, 0]}


    };

    columnsCdf = [
        {title: 'I.DE.F GO-PASS ' + ListGoPassDetails[0].intituleTarif.toUpperCase() + ' |  REFERENCE : ' + ListGoPassDetails[0].code, dataKey: "SNum"}];

    columnStylesCdf = {
        SNum: {columnWidth: 520, overflow: 'linebreak', fontSize: 7}
    };




    var rows2 = tempGoPass;
    var rows = [];
    var pageContent = function (data) {

        var bureau = $('.cmbBureau option:selected').text();
//        setDocParams(doc, docTitle, position, day, month, year, hh, mm, ss, data, bureau);
        var lenBureau = bureau.toUpperCase().length;
        var textRepublique = '';
        var lenRepub = textRepublique.length;
        var lenDebut1 = 0, lenDebut2 = 0, lenPosition1 = 0, lenPosition2 = 0;
        if (lenBureau > lenRepub) {
            lenPosition1 = lenBureau - lenRepub;
            if (lenPosition1 % 2 == 0) {
                lenDebut1 = lenPosition1 / 2;
            } else {
                lenDebut1 = (lenPosition1 + 1) / 2;
            }
        } else {
            lenPosition2 = lenRepub - lenBureau;
            if (lenPosition2 % 2 == 0) {
                lenDebut2 = lenPosition2 / 2 + 7;
            } else {
                lenDebut2 = (lenPosition2 + 1) / 2 + 7;
            }
        }

        doc.setFontType("bold");
        doc.setFontSize(5);
        doc.text(textRepublique, 40 + (lenPosition1 * 2) + lenDebut1, 25);
        doc.setFontSize(9);
        doc.text("", 73 + (lenPosition1 * 2) + lenDebut1, 40);
        doc.setFontSize(10);
        doc.text(bureau.toUpperCase(), 40 + (lenPosition2 * 2) + lenDebut2, 55);
        doc.text(docTitle, position, 110);
        doc.addImage(docImage, 'PNG', 85 + lenDebut1 + (lenPosition1 * 2), 20, 185, 70);
        doc.addImage(AvionIcon, 'PNG', 275 + lenDebut1 + (lenPosition1 * 2), 250, 60, 50);
//        doc.addImage(urlPhotoPdf, 'PNG', 250 + lenDebut1 + (lenPosition1 * 2), 370, 120, 50);
        doc.text(docTitle, position, 110);
        doc.setFontSize(6);
        doc.text("Imprimé le " + day + '/' + month + '/' + year + ' à ' + hh + ':' + mm + ':' + ss + ' avec E-PEAGE', 390, 25);
        doc.setFontSize(7);
        doc.text("AEROPORT DE DEPART", 180, 260);
        doc.setFontSize(25);
        doc.text(ListGoPassDetails[0].code_provenance.toUpperCase(), 200, 285);
        doc.setFontSize(7);
        doc.text("AEROPORT DE DESTINATION", 355, 260);
        doc.setFontSize(25);
        doc.text(ListGoPassDetails[0].code_destination.toUpperCase(), 355, 285);
        doc.setFontSize(6);
        var sizeLen = ListGoPassDetails[0].AeroportDepart.length;
        if (sizeLen < 30)
        {
            doc.text(ListGoPassDetails[0].AeroportDepart.toUpperCase(), 160, 300);
        }
        else if (sizeLen > 29)
        {
            doc.text(ListGoPassDetails[0].AeroportDepart.toUpperCase(), 125, 300);
        }
        doc.setFontSize(6);
        doc.text(ListGoPassDetails[0].AeroportDestination.toUpperCase(), 355, 300);
        doc.setFontSize(10);
        doc.text(ListGoPassDetails[0].Compagnie.toUpperCase() + '  |  N° VOL : ' + ListGoPassDetails[0].NumeroVol.toUpperCase(), 200, 350);


    };

    if (rows == undefined) {
        rows = [];
    }

    doc.autoTable(columnsCdf, rows, {
        addPageContent: pageContent,
        margin: {top: 139},
        theme: 'grid',
        headerStyles: {
            fillColor: [140, 140, 140],
            valign: 'middle',
            halign: 'center',
            fontSize: 10},
        columnStyles: columnStylesCdf,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 7 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });



    var rows = tempGoPass;
    doc.autoTable(columns2, rows, {
        addPageContent: pageContent,
        margin: {top: 454},
        theme: 'grid',
        headerStyles: {
            fillColor: [140, 140, 140],
            fontSize: 6},
        columnStyles: columnStyles2,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 26 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });

    doc.autoTable(columnsHeaderUsd, rows2, {
        addPageContent: null,
        margin: {top: 478},
        theme: 'grid',
        headerStyles: {
            fillColor: [77, 77, 77],
            color: [0, 0, 0],
            fontSize: 6},
        columnStyles: columnStylesHeaderUsd,
        drawRow: function (row, data) {
            if (row.index > 0 && row.index % 7 === 0) {
                doc.autoTableAddPage();
            }
        },
        drawCell: function (cell, data) {
        }
    });


    var finalY = doc.autoTable.previous.finalY;
    doc.setFontSize(6);
    doc.setFontStyle('bold');

//    var totCdf = formatNumberOnlyNew(TotalMontantCdf);
//    var totUsd = formatNumberOnlyNew(TotalMontantUsd);
//
//    var PenCdf = formatNumberOnlyNew(PenaliteMontantCdf);
//    var PenUsd = formatNumberOnlyNew(PenaliteMontantUsd);

//    doc.text("TOTAL NOTE  :  " + formatNumberOnlyNew(TotalDec), 265, finalY + 35);
//    doc.text("TOTAL CDF  :  " + totCdf, 420, finalY + 35);
//    doc.text("TOTAL USD :  " + totUsd, 555, finalY + 35);
//    doc.text("TOTAL NOTE PENALITE  :  " + formatNumberOnlyNew(TotalPen), 265, finalY + 50);
//    doc.text("PENALITE CDF  :  " + PenCdf, 420, finalY + 50);
//    doc.text("PENALITE USD :  " + PenUsd, 555, finalY + 50);
//    doc.text(getNameChefBureau(sessionStorage.getItem('BureauId')), 540, finalY + 135);
//    doc.text(getNameChefTaxateur(sessionStorage.getItem('BureauId'), sessionStorage.getItem('TaxeId')), 40, finalY + 135);

//    }
    window.open(doc.output('bloburl'), '_blank');
}
