<%-- 
    Document   : modalDetailsCommande
    Created on : 27 mars 2021, 00:27:30
    Author     : Juslin TSHIAMUA
--%>

<div class="modal fade bd-example-modal-lg addnewcompagnie" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edition d'une nouvelle compagnie</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--label-right">


                    <div class="row">
                        <div  id="DivPhoto" class="icon col-xs-6 col-md-6" > 
                            <div class="card-header">Chargement du Logo de la compagnie</div>

                            <div class="card no-shadow">
                                <img src="asset/img/photo.png" id="ImageEmpreinte" style="width:150px;height:130px"/>
                                <div class="card-body">
                                    <input type="file" id="LoadProfilPhoto" class="btn btn-primary btn-outline btn-sm">
                                    <p> Charger le logo de la compagnie</p>
                                </div>
                            </div>
                        </div>
                        <div class="icon col-xs-6 col-md-6" > 
                            <div class="card card-pills">
                                <div class="card-header">LOGO

                                </div>

                                <div class="card-body block-el" id="Photo">

                                </div>
                            </div>
                        </div>
                    </div>



                    <div class="row">

                        <div class="icon col-xs-6 col-md-6" > 
                            <div class="form-group">
                                <br>
                                <label for="userName">Intitul� de la compagnie*</label>
                                <input type="text" class="form-control required" name="userName" id="IntituleCompagnie" placeholder="Ex: Turkish Airlines"  >
                            </div> 
                        </div>
                        <div class="icon col-xs-6 col-md-6" > 
                            <div class="form-group">
                                <br>
                                <label for="userName">Sigle</label>
                                <input type="text" class="form-control required" name="userName" id="Signle" placeholder="Ex: TK"  >

                            </div> 
                        </div>
                         
                    </div>



                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btnSaveCompagnie" >
                    <i class="fa fa-check-circle"></i> Enregistrer
                </button>
                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
            </div>
        </div>
    </div>
</div>
