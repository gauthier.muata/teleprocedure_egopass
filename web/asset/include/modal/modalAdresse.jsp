<%-- 
    Document   : modalAdresse
    Created on : 8 mars 2021, 09:06:22
    Author     : Juslin Tshiamua
--%>

<div class="modal fade" id="modalAddAdresse" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered" style="width: 865px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h5 class="modal-title" id="exampleModalLongTitle">Registre des adresses</h5>
            </div>
            <div class="modal-body" id="divAdresses">
                <div class="navbar-form" role="search">
                    <div class="input-group">
                        <input type="text" class="form-control" id="inputLibelle" 
                               placeholder="Veuillez saisir l'avenue">
                        <div class="input-group-btn">
                            <button class="btn btn-success" style="background-color: #337ab7;border-color: #004085" id="btnRechercherAdresse">
                                <i class="fa fa-search-plus"></i>
                                Rechecher
                            </button>
                        </div>
                    </div>
                    <img src="asset/img/loading.gif" class="center-block" alt="" id="loading_img"/>
                    <table width="100%" class="table table-bordered table-hover" id="tableAdresse"></table>
                </div>
            </div>
            <div class="modal-footer">
                <div class="navbar-form" >
                    <label id="labelSelectedAdresse" style="float:left;font-weight: normal"></label>
                    <div class="input-group" >
                        <input type="text" class="form-control" id="inputNumber" placeholder="Num�ro" size="5">

                        <div class="input-group-btn">
                            <button class="btn btn-success" id="btnValiderAdresse" style="background-color: #337ab7;border-color: #004085"><i class="fa fa-check-circle"></i>
                                Valider
                            </button>
                            <button class="btn btn-default" id="btnFermerAdresse" data-dismiss="modal">
                                Fermer
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
