<%-- 
    Document   : modalDetailsCommande
    Created on : 27 mars 2021, 00:27:30
    Author     : Juslin TSHIAMUA
--%>

<div class="modal fade bd-example-modal-lg modalDetailsCommande" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><i class="flaticon2-soft-icons"></i>&nbsp;D&eacute;tails commande</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--label-right">
                    <div class="alert alert-secondary" role="alert">
                        <div class="alert-text" style="font-weight: bold">
                            <div class="form-group row div_info_voucher hidden" style="margin-bottom: 0rem;">
                                <label class="col-lg-3 col-form-label"><input id="setUsedVoucher" checked onclick="setVoucherUsed()" value="1" type="checkbox">&nbsp;Total vouchers utilis&eacute;s</label>
                                <div class="col-lg-2 info-used-voucher">
                                    <div class="list-group-item" style="color: red">0</div>
                                </div>
                                <label class="col-lg-4 col-form-label"><input id="setUnusedVoucher" checked onclick="setVoucherUsed()" value="2" type="checkbox">&nbsp;Total vouchers non utilis&eacute;s</label>
                                <div class="col-lg-2 info-unused-voucher">
                                    <div class="list-group-item" style="color: green">0</div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="kt-portlet__corps">
                        <div class="kt-wizard-v3__content panier" data-ktwizard-type="step-content" data-ktwizard-state="current">
                            <div class="kt-form__section kt-form__section--first">
                                <table id="table_dcv" class="table table-striped table-bordered" style="width:100%"></table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btnSelectAssujeti" style="display: none">
                    <i class="fa fa-check-circle"></i> &nbsp;S&eacute;lectionner
                </button>
                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
            </div>
        </div>
    </div>
</div>
