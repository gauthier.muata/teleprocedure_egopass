<%-- 
    Document   : modalVouchers
    Created on : 8 avr. 2021, 15:59:50
    Author     : Juslin TSHIAMUA
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<div class="modal fade bd-example-modal-lg modalVoucher" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"><i class="flaticon2-soft-icons title-voucher"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--label-right">
                    <div class="kt-portlet__corps">
                        <div class="kt-wizard-v3__content panier" data-ktwizard-type="step-content" data-ktwizard-state="current">
                            <div class="kt-form__section kt-form__section--first">
                                <table id="table_voucher" class="table table-striped table-bordered" style="width:100%"></table>
                                <table id="tbl_det_gh" class="table table-striped table-bordered" style="width:100%"></table>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <!--div class="form-group row div_v_usage hidden">
                    <label class="col-lg-4 col-form-label">Vouchers utilis&eacute;s</label>
                    <div class="col-lg-2 info-used-voucher">
                        <div class="list-group-item" style="color: red">0</div>
                    </div>
                    <label class="col-lg-4 col-form-label">Vouchers non utilis&eacute;s</label>
                    <div class="col-lg-2 info-unused-voucher">
                        <div class="list-group-item" style="color: green">0</div>
                    </div>
                </div-->
                <button class="btn btn-primary" id="btn_attribuer">
                    <i class="fa fa-check-circle"></i> Valider
                </button>
                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
            </div>
        </div>
    </div>
</div>
