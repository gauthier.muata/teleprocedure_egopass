<div class="modal fade" id="modalPaiementMpata" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document" style="width: 40%;margin-top:100px">
        <div class="modal-content"  >
            <div class="modal-header" style="margin-top:10px; margin-bottom: 5px">
                <div class="col col-md-12" style="margin-bottom:5px">
                    <img src="asset/admin/img/mpata.png" style="width: 92px; height: 92px;" alt="mpata">
                    <span style="text-align: left; font-weight: bold; font-size: 18px" class="modal-title" key="cashPayTitle">Paiement eMPata</span>
                </div>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group" style="margin-bottom:10px">
                            <div class="col col-md-12" style="margin-bottom:10px">
                                <label for="input_numeroCompte">Num�ro t�l�phone compte</label>
                                <input id="input_numeroCompte" name="input_numeroCompte" type="text" class="form-control">
                            </div>
                            <div class="col col-md-12" style="margin-bottom:10px">
                                <label for="input_montant" style="font-weight: bold;">Montant � payer</label>
                                <input id="input_montant" name="text" type="text" class="form-control" disabled>
                            </div>
                            <div class="col col-md-12" style="margin-bottom:10px">
                                <label for="inputPin" style="font-weight: bold;">Code PIN</label>
                                <input id="inputPin" name="text" type="password" class="form-control">
                            </div>
                        </div>
                    </div>
                </div>

                <center id="loading_img" style="display: none;">
                    <img src="asset/admin/img/200.gif" alt="" width="120" height="120" />
                    <h5>Veuillez patienter...</h5>
                </center> 

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" id="cancelPayMpata" data-dismiss="modal" >Fermer</button>
                <button type="button" class="btn btn-primary" id="btnPayerMpata" ><i class=""></i>Payer</button>   
            </div>
        </div>
    </div>
</div>
