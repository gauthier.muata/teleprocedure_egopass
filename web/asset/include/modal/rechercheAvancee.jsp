<%-- 
    Document   : rechercheAvancee
    Created on : 19 avr. 2021, 12:37:42
    Author     : Juslin TSHIAMUA
--%>

<div class="modal fade bd-example-modal-lg rechercheAvancee" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title kt-font-bold">Recherche avanc&eacute;e</i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--label-right">
                    <div class="form-group form-group-last">
                        <div class="alert alert-secondary" role="alert">
                            <div class="alert-icon"><i class="fa fa-search"></i></div>
                            <div class="alert-text" style="font-weight: bold; font-size: 16px">
                                Filtrer le registre de commandes
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="cmb_etat" class="col-lg-3 col-form-label">Etat commande</label>
                        <div class="col-lg-8">
                            <select name="cmb_etat" id="cmb_etat" class="form-control"></select>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dateDebut" class="col-lg-3 col-form-label">Date debut</label>
                        <div class="col-lg-8">
                            <input type="date" class="form-control" id="dateDebut">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="dateFin" class="col-lg-3 col-form-label">Date fin</label>
                        <div class="col-lg-8">
                            <input type="date" class="form-control" id="dateFin">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-brand btAdvancedSearch"><i class="fa fa-search"></i>Rechercher</button>
                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
            </div>
        </div>
    </div>
</div>
