<%-- 
    Document   : modalDetailsCommande
    Created on : 27 mars 2021, 00:27:30
    Author     : Juslin TSHIAMUA
--%>

<div class="modal fade bd-example-modal-lg modalprintGopass" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Partage du go-pass</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="kt-form kt-form--label-right">

                    <div class="row">
                        <div  id="DivPhoto" class="col-xs-6 col-md-6" > 
                            <div class="card-header shadow">Partagez le go-pass sur whatsapp</div>

                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-9" > 

                                            <label for="userName">Num�ro whatsap</label>
                                            <input type="text" class="form-control required" name="userName" id="TxtNumero" placeholder="Ex: +243822602347"  >
                                        </div>
                                        <div class="col-lg-3" > 

                                            <label for="userName">.</label>
                                            <br>
                                            <button id="SendWhatsapp" class="btn btn-default" >Envoyer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div  id="DivPhoto2" class="col-xs-6 col-md-6" > 
                            <div class="card-header shadow">Envoyer le go-pass par mail</div>

                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-9" > 

                                            <label for="userName">Adresse Mail</label>
                                            <input type="email"   class="form-control required" name="userName" id="AdresseMail" placeholder="Ex: christian.luketa@hologram.cd"  >
                                        </div>
                                        <div class="col-lg-3" > 

                                            <label for="userName">.</label>
                                            <br>
                                            <button id="SendMail" class="btn btn-default"  >Envoyer</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div  class="col-xs-6 col-md-6" > 
                            <br>
                            <div class="card-header shadow">Code QR du Go-Pass</div>
                            <div class="card shadow">
                                <div class="card-body">
                                    <div class="row">
                                        <center>
                                            <div id="test">

                                            </div>
                                        </center>

                                    </div>
                                </div>
                            </div>
                        </div>
                         
                    </div>






                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary" id="btnprint" >
                    <i class="fa fa-check-circle"></i> Je pr�f�re imprimer le go-pass
                </button>
                <button class="btn btn-default" data-dismiss="modal" >Fermer</button>
            </div>
        </div>
    </div>
</div>
