<%-- 
    Document   : menu_rapide
    Created on : 27 mars 2021, 12:55:28
    Author     : Juslin TSHIAMUA
--%>

<div class="kt-portlet__head-toolbar">
    <div class="kt-portlet__head-wrapper">
        <div class="dropdown dropdown-inline">
            <button type="button" class="btn btn-default btn-bold btn-upper btn-font-sm" data-toggle="dropdown" aria-haspopup="true">
                <i class="flaticon2-soft-icons"></i>Cr&eacute;ation nouveaux biens      
            </button>
            <div class="dropdown-menu dropdown-menu-right">
                <ul class="kt-nav">
                    <li class="kt-nav__section kt-nav__section--first">
                        <span class="kt-nav__section-text">Actions rapides</span>
                    </li>
                    <li class="kt-nav__item">
                        <a href="bien-automobile" class="kt-nav__link b_auto">
                            <i class="kt-nav__link-icon flaticon-truck"></i>
                            <span class="kt-nav__link-text">Bien automobile</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="bien-immobilier" class="kt-nav__link b_immo">
                            <i class="kt-nav__link-icon flaticon-home-2"></i>
                            <span class="kt-nav__link-text">Bien immobilier</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="bien-concession-miniere" class="kt-nav__link b_com">
                            <i class="kt-nav__link-icon flaticon-coins"></i>
                            <span class="kt-nav__link-text">Bien concession-miniere</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="bien-panneau-publicitaire" class="kt-nav__link b_ppub">
                            <i class="kt-nav__link-icon flaticon-tabs"></i>
                            <span class="kt-nav__link-text">Bien panneau publicitaire</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="location-bien" class="kt-nav__link b_locat">
                            <i class="kt-nav__link-icon flaticon-interface-9"></i>
                            <span class="kt-nav__link-text">Location bien</span>
                        </a>
                    </li>
                    <li class="kt-nav__item">
                        <a href="permutation-bien" class="kt-nav__link b_perm">
                            <i class="kt-nav__link-icon flaticon-suitcase"></i>
                            <span class="kt-nav__link-text">Permutation bien</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>