/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var documentContainer;
var btnPrint;

$(function () {

    documentContainer = $('#document_container');
    
    btnPrint = $('#btnPrint');
    
    var content = getDocumentContent();
    documentContainer.html(content);

    btnPrint.click(function (e) {
        e.preventDefault();
        documentContainer.printThis({
        });
    });

});

