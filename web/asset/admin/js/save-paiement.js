/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(function () {

    var reference = getUrlParameter('reference');

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + 'v2/declaration/' + reference,
        crossDomain: true,
        method: 'GET',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {
        KTApp.unblockPage();
        data = JSON.parse(JSON.stringify(data));

        if (data.numero === reference) {
            console.log(data);
            var bien = data.details[0];
            var sendData = {
                'documentType': 'DEC',
                'bankRefNumber': reference,
                'paymentDate': new Date().toISOString().slice(0, 10),
                'amountPaid': bien.montant,
                'currency': bien.devise,
                'documentNumber': reference,
                'bankAccount': bien.compte
            };
            savePaiement(JSON.stringify(sendData));
        } else {
            showResponseError();
        }
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
        KTApp.unblockPage();
    });

});

function savePaiement(dec_data) {

    $.ajax({
        url: baseUrl + 'bank/v1/payment',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        headers: headers,
        dataType: "JSON",
        data: dec_data
    }).done(function (data, textStatus, jqXHR) {
        KTApp.unblockPage();
        data = JSON.parse(JSON.stringify(data));

        
        
    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
        KTApp.unblockPage();
    });
}