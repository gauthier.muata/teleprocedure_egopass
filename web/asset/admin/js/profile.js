/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var listUser = new Object();
var userDatas,
        codeUser,
        passwordChange,
        lastPasswords,
        lastPasswordBdd,
        etatPassword,
        email,
        checkPassword = false;

var modalUpdatePassword = $('#modalUpdatePassword'),
        btnUpdatePassword = $('#btnUpdatePassword'),
        usernameprofile = $('#usernameprofile'),
        userId = $('#userId'),
        inputNumeroTeleDeclaration = $('#inputNumeroTeleDeclaration'),
        inputAdresseEmail = $('#inputAdresseEmail'),
        inputNumeroTelePhone = $('#inputNumeroTelePhone'),
        inputNomComplet = $('#inputNomComplet'),
        inputNumeroIdentFiscal = $('#inputNumeroIdentFiscal'),
        idMessagePwd = $('#idMessagePwd'),
        newPasswordConfirm = $('#newPasswordConfirm'),
        newPassword = $('#newPassword'),
        lastPassword = $('#lastPassword'),
        btnUpdateInfos = $('.btnUpdateInfos'),
        divUpdateInfo = $('.divUpdateInfo'),
        editInfos = $('.editInfos');

$(function () {

    setUserInfos();

    if (etatPassword == '0') {
        idMessagePwd.attr('style', 'display: inline');
    }

    btnUpdatePassword.click(function (e) {

        e.preventDefault();

        if (newPassword.val() === '' || newPasswordConfirm.val() === '' || lastPassword.val() === '') {
            alertify.alert('Veuillez remplir tous les champs.');
            return;
        }

        if (newPassword.val() !== newPasswordConfirm.val()) {
            alertify.alert('Le mot de passe de confirmation doit être identique au nouveau mot de passe');
            return;
        }


        if (lastPassword.val() === newPassword.val()) {
            alertify.alert('L\'actuel et le nouveau mot de passe sont identiques.');
            return;
        }

        if (newPassword.val().length < 6) {
            alertify.alert('Le nouveau mot de passe doit avoir 6 caractères minimum');
            return;
        }

        passwordChange = newPasswordConfirm.val();
        lastPasswordBdd = lastPassword.val();

        alertify.confirm('Etes-vous sûr de vouloir confirmer cette modification ?', function () {
            updateUserPassword();
        });

    });

    btnUpdateInfos.on('click', function (e) {
        alertify.confirm('Etes-vous sûr de vouloir mettre à jour ces données ?', function () {
            updateInfos();
        });
    });

    editInfos.on('click', function (e) {
        inputNumeroTelePhone.removeAttr("disabled");
        inputAdresseEmail.removeAttr("disabled");
        divUpdateInfo.removeClass('hidden');
        inputNumeroTelePhone.focus();
    });

//    $(window).click(function () {

//    });

});

function setUserInfos() {

    checkIfConnexionOn();
    setMenu();

    userDatas = JSON.parse(getUserData());
    listUser = JSON.parse(getUserData());

    inputNumeroTeleDeclaration.val(userDatas.ntd);
    usernameprofile.html(userDatas.fullname);
    userId.html(userDatas.fullname);
    inputNomComplet.val(userDatas.fullname);
    inputNumeroTelePhone.val(userDatas.phonenumber);
    inputAdresseEmail.val(userDatas.email);
//        inputNumeroTeleDeclaration.val(userDatas.userName);
    inputNumeroIdentFiscal.val(userDatas.nif);
    codeUser = userDatas.nif;
    lastPasswords = userDatas.password;
    etatPassword = userDatas.etatPassword;
    email = userDatas.email;

}

function updateUserPassword() {

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    var object = {
        'email': email,
        'password': newPassword.val(),
        'idUser': codeUser
    };

    $.ajax({
        url: baseUrl + '/v1/updatePassword',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {
        KTApp.unblockPage();

        if (data === 1) {
            alertify.alert('Modification effectuée avec succès.<br/> Connectez-vous avec votre nouveau mot de passe.');
            setTimeout(function () {
                window.location = 'login';
            }, 2000);

        } else if (data === -6) {
            alertify.alert('Votre mot de passe actuel et celui existant sont les mêmes. Prière de saisir un nouveau mot de passe.');
        } else {
            showResponseError();
        }

    }).fail(function (jqXHR, textStatus, errorThrown) {
        KTApp.unblockPage();
        showResponseError();
    });

}

function updateInfos() {
    
    if (!validateEmail(inputAdresseEmail.val())) {
        alertify.alert('L\adresse mail renseignée n\'est pas valide. Veuillez la ressaisir.');
        return;
    }

    var object = {
        "phonenumber": inputNumeroTelePhone.val(),
        "email": inputAdresseEmail.val(),
        "nif": userNif
    };

    KTApp.blockPage({
        overlayColor: "#000000",
        type: "v2",
        state: "primary",
        message: "Traitement en cours ..."
    });

    $.ajax({
        url: baseUrl + 'v1/updateUserInfos',
        crossDomain: true,
        method: 'POST',
        contentType: 'application/json',
        data: JSON.stringify(object),
        headers: headers,
        dataType: "JSON"
    }).done(function (data, textStatus, jqXHR) {

        KTApp.unblockPage();

        if (data == "-1") {
            showErrorMessageOnRequest();
            return;
        } else if (data == "0") {
            Swal.fire({
                title: "Profil",
                text: "L'enregistrement de vos informations n'a pas abouti.",
                type: "warning",
                confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
            });
            return;
        }
        Swal.fire({
            title: "Profil",
            text: "Vos informations ont été enregistrées avec succès.",
            type: "success",
            confirmButtonClass: "btn btn-secondary kt-btn kt-btn--wide"
        });
        inputNumeroTelePhone.attr('disabled', true);
        inputAdresseEmail.attr('disabled', true);
        divUpdateInfo.addClass('hidden');
        listUser.phonenumber = inputNumeroTelePhone.val();
        listUser.email = inputAdresseEmail.val();
        setUserData(JSON.stringify(listUser));

        setTimeout(function () {
            window.location.reload();
        }, 3000);

    }).fail(function (jqXHR, textStatus, errorThrown) {
        showResponseError();
        KTApp.unblockPage();
    });
}