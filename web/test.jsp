<%-- 
    Document   : test
    Created on : 1 avr. 2021, 12:09:35
    Author     : Administrateur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<head>
    <title></title>
</head>
<body>
    <script type="text/javascript">
        window.onload = function () {
            //Build an array containing Customer records.
            var customers = new Array();
            customers.push([1, "John Hammond", "United States"]);
            customers.push([2, "Mudassar Khan", "India"]);
            customers.push([3, "Suzanne Mathews", "France"]);
            customers.push([4, "Robert Schidner", "Russia"]);
 
            //Add the data rows.
            for (var i = 0; i < customers.length; i++) {
                //Get the reference of the HTML Table.
                var table = document.getElementById("tblCustomers");
 
                //Add Row.
                row = table.insertRow(-1);
 
                //Add CheckBox cell at First position.
                var cell = row.insertCell(0);
                var chk = document.createElement("INPUT");
                chk.type = "checkbox";
                chk.setAttribute("onclick", "CheckUncheckHeader()");
                cell.appendChild(chk);
                cell.innerHTML += customers[i][0];
 
                //Add Name cell.
                cell = row.insertCell(-1);
                cell.innerHTML = customers[i][1];
 
                //Add Country cell.
                cell = row.insertCell(-1);
                cell.innerHTML = customers[i][2];
            }
        };
 
        function CheckUncheckAll(chkAll) {
            //Fetch all rows of the Table.
            var rows = document.getElementById("tblCustomers").rows;
 
            //Execute loop on all rows excluding the Header row.
            for (var i = 1; i < rows.length; i++) {
                rows[i].getElementsByTagName("INPUT")[0].checked = chkAll.checked;
            }
        };
 
        function CheckUncheckHeader() {
           //Determine the reference CheckBox in Header row.
            var chkAll = document.getElementById("chkAll");
 
            //By default set to Checked.
            chkAll.checked = true;
 
            //Fetch all rows of the Table.
            var rows = document.getElementById("tblCustomers").rows;
 
            //Execute loop on all rows excluding the Header row.
            for (var i = 1; i < rows.length; i++) {
                if (!rows[i].getElementsByTagName("INPUT")[0].checked) {
                    chkAll.checked = false;
                    break;
                }
            }
        };
    </script>
    <table id="tblCustomers" cellpadding="0" cellspacing="0" border="1">
        <tr>
            <th><input type="checkbox" id="chkAll" onclick="CheckUncheckAll(this)" />
                CustomerId
            </th>
            <th>Name</th>
            <th>Country</th>
        </tr>
    </table>
</body>
</html>