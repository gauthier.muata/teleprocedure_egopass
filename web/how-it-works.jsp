<%-- 
    Document   : how-it-works
    Created on : 30 sept. 2020, 09:56:00
    Author     : enoch.bukasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>E-Recettes:: DGRK</title>
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">

        <!-- Favicons -->
        <link href="asset/img/favicon.png" rel="icon">
        <link href="asset/img/apple-touch-icon.png" rel="apple-touch-icon">

        <!-- Google Fonts -->
        <link href="asset/css/family.css" rel="stylesheet" type="text/css"/>

        <!-- Bootstrap CSS File -->
        <link href="asset/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>

        <!-- Libraries CSS Files -->
        <link href="asset/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/prettyphoto/css/prettyphoto.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/hover/hoverex-all.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/jetmenu/jetmenu.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/owl-carousel/owl-carousel.css" rel="stylesheet" type="text/css"/>
        <link href="asset/lib/intl-tel-input/build/css/intlTelInput.css" rel="stylesheet" type="text/css"/>

        <!-- Main Stylesheet File -->
        <link href="asset/front/style.css" rel="stylesheet" type="text/css"/>
        <link href="asset/front/blue.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <%@include file="asset/include/header.html" %>

        <section class="post-wrapper-top">
            <div class="container">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <ul class="breadcrumb">
                        <li><a href="">Accueil</a></li>
                        <li><a href=""></a>Comment ça marche</li>
                    </ul>
                    <h2>Comment ça marche</h2>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <!-- search -->
                    <div class="search-bar">
                    </div>
                    <!-- / end div .search-bar -->
                </div>
            </div>
        </section>
        <!-- end post-wrapper-top -->

        <section class="section1">
            <div class="container clearfix">
                <div class="content col-lg-12 col-md-12 col-sm-12 clearfix">
                    <div class="general-title text-center">
                        <h3>LISTE DES GUIDE D'UTILISATION</h3>
                        <hr>
                    </div>
                    <div class="clearfix"></div>

                    <div class="col-lg-4 col-md-4">
                        <div class="servicebox text-center">
                            <div class="service-icon">
                                <div class="dm-icon-effect-1 effect-slide-bottom in" data-effect="slide-bottom" style="transition: all 0.7s ease-in-out 0s;">
                                    <a href="#" class=""> <i class="active dm-icon fa fa-user fa-3x"></i> </a>
                                </div>
                                <div class="servicetitle">
                                    <h4>Inscription</h4>
                                    <hr>
                                </div>
                                <p>Ici vous pouvez visualiser le guide d'utilisation pour l'inscription à la plateforme de télé déclaration. <br>
                                    <a class="link-guide" href="http://localhost:8080/Teledeclaration_java/asset/files/Guide_inscription.pdf" target="_blank">Cliquez ici </a>
                                </p>
                            </div>
                            <!-- service-icon -->
                        </div>
                        <!-- servicebox -->
                    </div>
                    <!-- large-3 -->

                    <div class="col-lg-4 col-md-4">
                        <div class="servicebox text-center">
                            <div class="service-icon">
                                <div class="dm-icon-effect-1 effect-slide-bottom in" data-effect="slide-bottom" style="transition: all 0.7s ease-in-out 0s;">
                                    <a href="#" class=""> <i class="active dm-icon fa fa-cogs fa-3x"></i> </a>
                                </div>
                                <div class="servicetitle">
                                    <h4>Connexion</h4>
                                    <hr>
                                </div>
                                <p>Ici vous pouvez visualiser le guide d'utilisation pour la connexion à la plateforme de télé déclaration
                                    <br>
                                    <a class="link-guide" href="http://localhost:8080/Teledeclaration_java/asset/files/Guide_connexion.pdf" target="_blank">Cliquez ici </a></p>
                            </div>
                            <!-- service-icon -->
                        </div>
                        <!-- servicebox -->
                    </div>
                    <!-- large-3 -->

                    <div class="col-lg-4 col-md-4">
                        <div class="servicebox text-center">
                            <div class="service-icon">
                                <div class="dm-icon-effect-1 effect-slide-bottom in" data-effect="slide-bottom" style="transition: all 0.7s ease-in-out 0s;">
                                    <a href="#" class=""> <i class="active dm-icon fa fa-car fa-3x"></i> </a>
                                </div>
                                <div class="servicetitle">
                                    <h4>Vignette</h4>
                                    <hr>
                                </div>
                                <p>Ici vous pouvez visualiser le guide d'utilisation pour la déclaration à la vignette dans la plateforme de télé déclaration.<br>
                                    <a class="link-guide" href="#" target="_blank">Cliquez ici </a></p>
                            </div>
                            <!-- service-icon -->
                        </div>
                        <!-- servicebox -->
                    </div>

                    <div class="clearfix"></div>
                    <div class="divider"></div>

                    <div class="col-lg-4 col-md-4">
                        <div class="servicebox text-center">
                            <div class="service-icon">
                                <div class="dm-icon-effect-1 effect-slide-bottom in" data-effect="slide-bottom" style="transition: all 0.7s ease-in-out 0s;">
                                    <a href="#" class=""> <i class="active dm-icon fa fa-home fa-3x"></i> </a>
                                </div>
                                <div class="servicetitle">
                                    <h4>Impôt Foncier</h4>
                                    <hr>
                                </div>
                                <p>Ici vous pouvez visualiser le guide d'utilisation pour la déclaration à l'impôt foncier dans la plateforme de télé déclaration.<br>
                                    <a class="link-guide" href="http://localhost:8080/Teledeclaration_java/asset/files/Guide_IF.pdf" target="_blank">Cliquez ici </a></p>
                            </div>
                            <!-- service-icon -->
                        </div>
                        <!-- servicebox -->
                    </div>
                    <!-- large-3 -->

                    <div class="col-lg-4 col-md-4">
                        <div class="servicebox text-center">
                            <div class="service-icon">
                                <div class="dm-icon-effect-1 effect-slide-bottom in" data-effect="slide-bottom" style="transition: all 0.7s ease-in-out 0s;">
                                    <a href="#" class=""> <i class="active dm-icon fa fa-home fa-3x"></i> </a>
                                </div>
                                <div class="servicetitle">
                                    <h4>Impôt sur le revenu locatif</h4>
                                    <hr>
                                </div>
                                <p>Ici vous pouvez visualiser le guide d'utilisation pour la déclaration à l'impôt sur le revenu locatif dans la plateforme de télé déclaration.<br>
                                    <a class="link-guide" href="http://localhost:8080/Teledeclaration_java/asset/files/Guide_IRL.pdf" target="_blank">Cliquez ici </a></p>
                            </div>
                            <!-- service-icon -->
                        </div>
                        <!-- servicebox -->
                    </div>
                    <!-- large-3 -->

                    <div class="col-lg-4 col-md-4">
                        <div class="servicebox text-center">
                            <div class="service-icon">
                                <div class="dm-icon-effect-1 effect-slide-bottom in" data-effect="slide-bottom" style="transition: all 0.7s ease-in-out 0s;">
                                    <a href="#" class=""> <i class="active dm-icon fa fa-home fa-3x"></i> </a>
                                </div>
                                <div class="servicetitle">
                                    <h4>Retenue Locative</h4>
                                    <hr>
                                </div>
                                <p>Ici vous pouvez visualiser le guide d'utilisation pour la déclaration sur la revenu locative dans la plateforme de télé déclaration.<br>
                                    <a class="link-guide" href="http://localhost:8080/Teledeclaration_java/asset/files/Guide_RL.pdf" target="_blank">Cliquez ici </a></p>
                            </div>
                            <!-- service-icon -->
                        </div>
                        <!-- servicebox -->
                    </div>

                </div>
                <!-- end content -->
            </div>
            <!-- end container -->
        </section>

        <!-- end footer -->
        <footer class="footer">
            <div class="container">
                <div class="widget col-lg-4 col-md-4 col-sm-12">
                    <h4 class="title">A propos de nous</h4>
                    <p>E-RECETTES est la plateforme de télédéclaration de la province de Haut Katanga.</p>
                </div>
                <!-- end widget -->
                <!-- end widget -->
                <div class="widget col-lg-4 col-md-4 col-sm-12">
                    <h4 class="title">Voir Adresses</h4>
                    <ul class="contact_details">
                        <li><i class="fa fa-envelope-o"> </i> <a href="mailto:info@dgrk-rdc.com">info@dgrk-rdc.com</a> </li>
                        <li><i class="fa fa-phone-square"> </i> <a href="tel:+243810000180">(+243) 810000180</a> </li>
                        <li>
                            <i class="fa fa-home"></i> 
                            18, avenue Kitona, Immeuble Bali, réf. Hotel Royal, Gombe, Kinshasa
                        </li>
                    </ul>
                    <!-- contact_details -->
                </div>
                <!-- end widget -->
                <div class="widget col-lg-4 col-md-4 col-sm-12">
                    <h4 class="title">Liens</h4>
                    <ul>
                        <li><a href="https://www.dgrk-rdc.com/" target="_blank">Direction Générale des Recettes de Kinshasa</a></li>
                        <li><a href="http://minfinrdc.com/minfin/" target="_blank">Ministère provincial des Finances de Kinshasa</a></li>
                    </ul>
                </div>
                <!-- end widget -->
            </div>
            <!-- end container -->

            <div class="copyrights">
                <div class="container">
                    <div class="col-lg-6 col-md-6 col-sm-12 columns footer-left">
                        <p>Copyright © 2020 -  tout droits reservés.</p>
                    </div>
                    <!-- end widget -->
                    <div class="col-lg-6 col-md-6 col-sm-12 columns text-right">
                        <div class="footer-menu right">
                            <div>
                                Designed by <www class="hologram cd"></www> <a href="https://www.hologram.cd/">Hologram.cd</a>
                            </div>
                        </div>
                    </div>
                    <!-- end large-6 -->
                </div>
                <!-- end container -->
            </div>
            <!-- end copyrights -->
        </footer>
        <!-- end footer -->

    </body>
    <script src="asset/lib/jquery/jquery.min.js"></script>
    <script src="asset/lib/bootstrap/js/bootstrap.min.js"></script>
    <script src="asset/js/validate.js"></script>
    <script src="asset/js/prettyphoto.js"></script>
    <script src="asset/js/isotope.min.js"></script>
    <script src="asset/js/hoverdir.js"></script>
    <script src="asset/js/hoverex.min.js"></script>
    <script src="asset/js/unveil-effects.js"></script>
    <script src="asset/js/owl-carousel.js"></script>
    <script src="asset/js/jetmenu.js"></script>
    <script src="asset/js/animate-enhanced.min.js"></script>
    <script src="asset/js/jigowatt.js"></script>
    <script src="asset/js/easypiechart.min.js"></script>   
    <script src="asset/js/jquery.gmap.js"></script> 
    <script src="asset/js/front-main.js" type="text/javascript"></script>
    <script src="asset/js/intlTelInput.js" type="text/javascript"></script>
    <script src="asset/js/core.js" type="text/javascript"></script> 
    <script src="asset/js/front-main.js" type="text/javascript"></script>
    <script src="asset/js/jquery.mb.YTPlayer.js" type="text/javascript"></script>
    <script src="asset/js/modernizr.js" type="text/javascript"></script>   
    <!-- Template Main Javascript File -->    
    <script src="asset/js/main.js"></script>
</html>
