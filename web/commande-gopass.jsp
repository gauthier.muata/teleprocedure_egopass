<%-- 
    Document   : commande-gopass
    Created on : 10 aout 2021, 08:58:45
    Author     : juslin.tshiamua
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Commande GoPass 
        </title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--begin::Fonts -->
        <link href="asset/css/font.css" rel="stylesheet" type="text/css"/>
        <link href="asset/admin/css/fullcalendar.bundle.css" rel="stylesheet" type="text/css">
        
        <link href="asset/admin/css/plugins.bundle.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/style.bundle.css" rel="stylesheet" type="text/css">

        <link href="asset/admin/css/light.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/light-menu.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy-aside.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/custom.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/paiement.css" rel="stylesheet" type="text/css"/>
        <!--end::Layout Skins -->

        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>

        <style type="text/css">
            /* Chart.js */
            /*
    * DOM element rendering detection
    * https://davidwalsh.name/detect-node-insertion
    */

            @keyframes chartjs-render-animation {
                from {
                    opacity: 0.99;
                }
                to {
                    opacity: 1;
                }
            }

            .chartjs-render-monitor {
                animation: chartjs-render-animation 0.001s;
            }
            /*
    * DOM element resizing detection
    * https://github.com/marcj/css-element-queries
    */

            .chartjs-size-monitor,
            .chartjs-size-monitor-expand,
            .chartjs-size-monitor-shrink {
                position: absolute;
                direction: ltr;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                overflow: hidden;
                pointer-events: none;
                visibility: hidden;
                z-index: -1;
            }

            .chartjs-size-monitor-expand > div {
                position: absolute;
                width: 1000000px;
                height: 1000000px;
                left: 0;
                top: 0;
            }

            .chartjs-size-monitor-shrink > div {
                position: absolute;
                width: 200%;
                height: 200%;
                left: 0;
                top: 0;
            }
        </style>
    </head>
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed" style="">
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                <h4 class="text-light">E-Recettes</h4> 
            </div>
            <div class="kt-header-mobile__toolbar">

                <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
            </div>
        </div>

        <div class="kt-grid kt-grid--hor kt-grid--root">

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>

                <%@include file="asset/include/menu.html" %>

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    <%@include file="asset/include/headerV2.html" %>

                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid "></div>
                        </div>


                        <!-- Begin Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <%@include file="asset/include/messageUpdatePassword.html"%>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="kt-portlet" id="kt_blockui_1_content">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">Commande GoPass</h3>
                                            </div>
                                        </div>
                                        <!--begin::Form-->
                                        <form class="kt-form kt-form--label-right" id="form-commande-gopass">
                                            <div class="kt-portlet__corps">
                                                <div class="form-group form-group-last">
                                                    <div class="alert alert-secondary" role="alert">
                                                        <div class="alert-icon"><i class="flaticon2-lorry kt-font-brand"></i></div>
                                                        <div class="alert-text">
                                                            Vous pouvez ici faire des commandes en provision de vos GoPass et payer en ligne.
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="level" id="level" value="-1">
                                                <div class="form-group row">
                                                    <label for="cmb_tarif" class="col-lg-3 col-form-label">Cat&eacute;gorie de GoPass</label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" name="tarifs" id="cmb_tarif">
                                                            <option value="0">S&eacute;lectionner une cat&eacute;gorie</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="cmb_typeGopass" class="col-lg-3 col-form-label">Type GoPass</label>
                                                    <div class="col-lg-8">
                                                        <select class="form-control" name="Type GoPass" id="cmb_typeGopass">
                                                            <option value="0" selected disabled>S&eacute;lectionner un type</option>
                                                            <option value="1">C</option>
                                                            <option value="2">Y</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="montant" class="col-lg-3 col-form-label">Montant</label>
                                                    <div class="col-lg-3 montant_taxe">
                                                        <div class="list-group-item">0</div>
                                                    </div>
                                                    <label for="input_quantite" class="col-lg-2 col-form-label">Nombre de GoPass</label>
                                                    <div class="col-lg-3">
                                                        <input class="form-control" type="text" name="quantit&e&eacute;" id="input_quantite" >
                                                    </div>
                                                </div>
                                                <div class="kt-portlet__corps">
                                                    <div class="form-group row">
                                                        <div class="col-lg-12">
                                                            <button type="button" class="btn btn-primary pull-right" id="btnAddPanier"><i class="flaticon2-shopping-cart-1"></i>Ajouter au panier</button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-wizard-v3__content panier" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                    <div style="margin-left: 20px; margin-right: 20px; margin-bottom: 10px;">
                                                        <table id="panierCommndeTaxe" class="table table-striped table-bordered" style="width:100%"></table>
                                                    </div>
                                                </div>
                                                <div class="form-group row hidden">
                                                    <label for="total_montant" class="col-lg-3 col-form-label" style="color: red; font-weight: bold">Net &agrave; payer</label>
                                                    <div class="col-lg-8 total_montant">
                                                        <div class="list-group-item">0</div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="kt-portlet__foot">
                                                <div class="kt-form__actions">
                                                    <div class="row">
                                                        <div class="col-2">
                                                        </div>
                                                        <div class="col-10">
                                                            <button type="button" class="btn btn-primary" id="btnAddCommande"><i class="flaticon2-add-1"></i>Enregistrer</button>
                                                            <button type="button" class="btn btn-secondary reload">Annuler</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!--end::Form-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Content -->
                    </div>

                </div>
            </div>
        </div>

        <%@include file="asset/include/footerV2.html" %>
        <%@include file="asset/include/modal/modalMoyensPaiement.jsp" %>

        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "metal": "#c4c5d6",
                        "light": "#ffffff",
                        "accent": "#00c5dc",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995",
                        "focus": "#9816f4"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>
        <script src="asset/admin/js/plugins.bundle.js" type="text/javascript"></script>
        <script src="asset/admin/js/scripts.bundle.js" type="text/javascript"></script>

        <!--end::Page Vendors -->
        <script src="asset/admin/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="asset/admin/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

        <script src="asset/js/jquery.blockUI.js"></script>
        <script src="asset/alertify/alertify.min.js" type="text/javascript"></script>

        <script src="asset/js/core.js" type="text/javascript"></script>
        <script src="asset/js/utils.js" type="text/javascript"></script>
        <script src="asset/js/commande/commande_gopass.js" type="text/javascript"></script>
        <script src="asset/js/paymentManager.js" type="text/javascript"></script>

    </body>

</html>
