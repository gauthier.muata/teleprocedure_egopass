<%-- 
    Document   : declarations
    Created on : 3 nov. 2020, 14:13:45
    Author     : enoch.bukasa
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Paiement</title>
        <meta name="description" content="Latest updates and statistic charts">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <!--begin::Fonts -->
        <link href="asset/css/font.css" rel="stylesheet" type="text/css"/>
        <!--end::Fonts -->

        <!--begin::Global Theme Styles(used by all pages) -->
        <link href="asset/admin/css/fullcalendar.bundle.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/plugins.bundle.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/style.bundle.css" rel="stylesheet" type="text/css">
        <!--end::Global Theme Styles -->

        <!--begin::Layout Skins(used by all pages) -->

        <link href="asset/admin/css/light.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/light-menu.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/navy-aside.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/custom.css" rel="stylesheet" type="text/css">
        <link href="asset/admin/css/wizard-v3.css" rel="stylesheet" type="text/css"/>
        <link href="asset/admin/css/dataTables.bootstrap4.min.css" rel="stylesheet" type="text/css">
        <link href="asset/plugins/fancybox/css/jquery.fancybox.min.css" rel="stylesheet" type="text/css">
        <!--end::Layout Skins -->

        <link href="asset/alertify/alertify.min.css" rel="stylesheet" type="text/css"/>
        <!--end::Layout Skins -->

        <style type="text/css">
            /* Chart.js */
            /*
    * DOM element rendering detection
    * https://davidwalsh.name/detect-node-insertion
    */

            @keyframes chartjs-render-animation {
                from {
                    opacity: 0.99;
                }
                to {
                    opacity: 1;
                }
            }

            .chartjs-render-monitor {
                animation: chartjs-render-animation 0.001s;
            }
            /*
    * DOM element resizing detection
    * https://github.com/marcj/css-element-queries
    */

            .chartjs-size-monitor,
            .chartjs-size-monitor-expand,
            .chartjs-size-monitor-shrink {
                position: absolute;
                direction: ltr;
                left: 0;
                top: 0;
                right: 0;
                bottom: 0;
                overflow: hidden;
                pointer-events: none;
                visibility: hidden;
                z-index: -1;
            }

            .chartjs-size-monitor-expand > div {
                position: absolute;
                width: 1000000px;
                height: 1000000px;
                left: 0;
                top: 0;
            }

            .chartjs-size-monitor-shrink > div {
                position: absolute;
                width: 200%;
                height: 200%;
                left: 0;
                top: 0;
            }
        </style>
    </head>
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed" style="">
        <div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
            <div class="kt-header-mobile__logo">
                <h4 class="text-light">E-Recettes</h4> 
            </div>
            <div class="kt-header-mobile__toolbar">

                <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left" id="kt_aside_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>

                <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
            </div>
        </div>

        <div class="kt-grid kt-grid--hor kt-grid--root">

            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
                <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>

                <%@include file="asset/include/menu.html" %>

                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

                    <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " style="justify-content: flex-end;">

                        <!-- begin:: Header Menu -->
                        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>

                        <!-- end:: Header Menu -->
                        <!-- begin:: Header Topbar -->
                        <div class="kt-header__topbar">
                            <!--begin: User Bar -->

                            <div class="kt-header__topbar-item kt-header__topbar-item--user">

                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">

                                    <div class="kt-header__topbar-user kt-rounded-">

                                        <span class="kt-header__topbar-welcome kt-hidden-mobile">Salut,</span>
                                        <span class="kt-header__topbar-username kt-hidden-mobile" id="userId"></span>
                                        <img alt="Pic" src="asset/admin/img/avatar.jpg" class="kt-rounded-">
                                        <span class="kt-badge kt-badge--username kt-badge--lg kt-badge--brand kt-hidden kt-badge--bold">S</span>
                                    </div>
                                </div>
                                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-sm">
                                    <div class="kt-user-card kt-margin-b-40 kt-margin-b-30-tablet-and-mobile" style="background-image: url(/admin/img/head_bg_sm.jpg)">
                                        <div class="kt-user-card__wrapper">
                                            <div class="kt-user-card__pic">

                                                <img alt="Pic" src="asset/admin/img/avatar.jpg" class="kt-rounded-">
                                            </div>
                                            <div class="kt-user-card__details">
                                                <div class="kt-user-card__name" style="font-size: 1rem;"></div>
                                                <span class="email-user hidden"></span>
                                            </div>
                                        </div>
                                    </div>

                                    <ul class="kt-nav kt-margin-b-10">
                                        <li class="kt-nav__item">
                                            <a href="profile" class="kt-nav__link">
                                                <span class="kt-nav__link-icon"><i class="flaticon2-calendar-3"></i></span>
                                                <span class="kt-nav__link-text">Mon profil</span>
                                            </a>
                                        </li>
                                        <li class="kt-nav__separator kt-nav__separator--fit"></li>

                                        <li class="kt-nav__custom kt-space-between">
                                            <a href="login" class="btn btn-label-brand btn-upper btn-sm btn-bold">Se déconnecter</a>
                                            <i class="flaticon2-information kt-label-font-color-2" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more..."></i>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                            <!--end: User Bar -->

                        </div>
                        <!-- end:: Header Topbar -->
                    </div>

                    <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container  kt-container--fluid "></div>
                        </div>

                        <!-- Begin Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <%@include file="asset/include/messageUpdatePassword.html"%>

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="kt-portlet" id="kt_blockui_1_content">
                                        <div class="kt-portlet__head">
                                            <div class="kt-portlet__head-label">
                                                <h3 class="kt-portlet__head-title">Registres - <span style="color:red">Paiements</span></h3>
                                            </div>
                                        </div>
                                        <!--begin::Form-->
                                        <form class="kt-form kt-form--label-right" id="form-repertoire-bien">
                                            <div class="kt-portlet__corps">
                                                <div class="form-group form-group-last">
                                                    <div class="alert alert-secondary" role="alert">
                                                        <div class="alert-icon"><i class="flaticon-clipboard kt-font-brand"></i></div>
                                                        <div class="alert-text" style="font-weight: bold">
                                                            R&eacute;pertoire des paiements
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="kt-wizard-v3__content" data-ktwizard-type="step-content" data-ktwizard-state="current">
                                                    <div class="kt-form__section kt-form__section--first">
                                                        <table id="tablePaiement" class="table table-striped table-bordered" style="width:100%"></table>
                                                    </div>
                                                </div>
                                            </div>

                                        </form>
                                        <!--end::Form-->
                                    </div>
                                </div>
                            </div>  
                        </div>
                        <!-- End Content -->
                    </div>
                </div>
            </div>
        </div>

        <%@include file="asset/include/footerV2.html" %>

        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "metal": "#c4c5d6",
                        "light": "#ffffff",
                        "accent": "#00c5dc",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995",
                        "focus": "#9816f4"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>
        <script src="asset/admin/js/jquery-3.5.1.js" type="text/javascript"></script>
        <script src="asset/admin/js/plugins.bundle.js" type="text/javascript"></script>
        <script src="asset/admin/js/scripts.bundle.js" type="text/javascript"></script>
        <!--end::Global Theme Bundle -->

        <!--begin::Page Vendors(used by this page) -->
        <script src="asset/admin/js/fullcalendar.bundle.js" type="text/javascript"></script>
        <!--end::Page Vendors -->

        <script src="asset/admin/js/jquery.dataTables.min.js" type="text/javascript"></script>
        <script src="asset/admin/js/dataTables.bootstrap4.min.js" type="text/javascript"></script>

        <script src="asset/js/core.js" type="text/javascript"></script>
        <script src="asset/js/jquery.blockUI.js"></script>
        <script src="asset/alertify/alertify.min.js" type="text/javascript"></script>

        <script src="asset/js/utils.js" type="text/javascript"></script>
        <script src="asset/js/paiement.js" type="text/javascript"></script>

    </body>
</html>
