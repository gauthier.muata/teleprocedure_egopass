<%-- 
    Document   : accueil
    Created on : 6 août 2021, 16:39:20
    Author     : Administrateur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="keywords">
        <meta content="" name="description">
        <title>Accueil</title>

        <link href="asset/front/style.css" rel="stylesheet" type="text/css"/>
        <link href="asset/css/classic.css" rel="stylesheet" type="text/css"/>
        <style>
            body {
                /*background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAaCAYAAACpSkzOAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAxMC8yOS8xMiKqq3kAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzVxteM2AAABHklEQVRIib2Vyw6EIAxFW5idr///Qx9sfG3pLEyJ3tAwi5EmBqRo7vHawiEEERHS6x7MTMxMVv6+z3tPMUYSkfTM/R0fEaG2bbMv+Gc4nZzn+dN4HAcREa3r+hi3bcuu68jLskhVIlW073tWaYlQ9+F9IpqmSfq+fwskhdO/AwmUTJXrOuaRQNeRkOd5lq7rXmS5InmERKoER/QMvUAPlZDHcZRhGN4CSeGY+aHMqgcks5RrHv/eeh455x5KrMq2yHQdibDO6ncG/KZWL7M8xDyS1/MIO0NJqdULLS81X6/X6aR0nqBSJcPeZnlZrzN477NKURn2Nus8sjzmEII0TfMiyxUuxphVWjpJkbx0btUnshRihVv70Bv8ItXq6Asoi/ZiCbU6YgAAAABJRU5ErkJggg==);*/
            }
            .dark_1{
                background-color: #000
            }
            .main-height{
                min-height:5vh
            }
        </style>
    </head>
    <body>
        <%@include file="asset/include/header.html" %>

        <main class="main d-flex w-100 main-height" id="intro">
            <div class="container d-flex flex-column">
                <div class="row h-10">
                    <div class="col-sm-10 col-md-8 col-lg-6 mx-auto d-table h-10">
                        <div class="d-table-cell align-middle">

                            <div class="text-center mt-3">
                                <h1 class="h2">Portail eGoPass</h1>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </main>
        <main class="main d-flex w-100 main-height">
            <div class="container d-flex flex-column">
                <div class="row h-10">
                    <div class="col-sm-12 col-md-12 col-lg-12 mx-auto d-table h-10">
                        <div class="d-table-cell align-middle">

                            <div class="text-center mt-2">
                                <h1 class="h2 text-dark">Bienvenue dans le portail de T&eacute;l&eacute;-proc&eacute;dure&nbsp;eGoPass</h1>
                                <hr />
                            </div>

                            <div class="text-center m-0">
                                <h3 class="h3 text-dark"><small>Cr&eacute;er votre compte à partir de votre bureau, faites vos commandes de GoPass en ligne et payez aussi en ligne.</small></h3>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-6 col-xl d-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="media">
                                    <div class="d-inline-block mt-2 mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-cart feather-lg text-primary"><circle cx="9" cy="21" r="1"></circle><circle cx="20" cy="21" r="1"></circle><path d="M1 1h4l2.68 13.39a2 2 0 0 0 2 1.61h9.72a2 2 0 0 0 2-1.61L23 6H6"></path></svg>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="mb-2">2.562</h3>
                                        <div class="mb-0">Commandes GoPass</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl d-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="media">
                                    <div class="d-inline-block mt-2 mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-activity feather-lg text-warning"><polyline points="22 12 18 12 15 21 9 3 6 12 2 12"></polyline></svg>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="mb-2">17.212</h3>
                                        <div class="mb-0">Abonn&eacute;s</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl d-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="media">
                                    <div class="d-inline-block mt-2 mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign feather-lg text-success"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="mb-2">$ 24.300</h3>
                                        <div class="mb-0">Total Earnings</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl d-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="media">
                                    <div class="d-inline-block mt-2 mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-shopping-bag feather-lg text-danger"><path d="M6 2L3 6v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2-2V6l-3-4z"></path><line x1="3" y1="6" x2="21" y2="6"></line><path d="M16 10a4 4 0 0 1-8 0"></path></svg>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="mb-2">43</h3>
                                        <div class="mb-0">Pending Orders</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-xl d-none d-xxl-flex">
                        <div class="card flex-fill">
                            <div class="card-body py-4">
                                <div class="media">
                                    <div class="d-inline-block mt-2 mr-3">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-dollar-sign feather-lg text-info"><line x1="12" y1="1" x2="12" y2="23"></line><path d="M17 5H9.5a3.5 3.5 0 0 0 0 7h5a3.5 3.5 0 0 1 0 7H6"></path></svg>
                                    </div>
                                    <div class="media-body">
                                        <h3 class="mb-2">$ 18.700</h3>
                                        <div class="mb-0">Total Revenue</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-lg-8 d-flex">
                        <div class="card flex-fill w-100">
                            <div class="card-header">
                                <span class="badge badge-primary float-right">Monthly</span>
                                <h5 class="card-title mb-0">Total Revenue</h5>
                            </div>
                            <div class="card-body">
                                <div class="chart chart-lg"><div class="chartjs-size-monitor"><div class="chartjs-size-monitor-expand"><div class=""></div></div><div class="chartjs-size-monitor-shrink"><div class=""></div></div></div>
                                    <canvas id="chartjs-dashboard-line" width="272" height="350" style="display: block; height: 350px; width: 272px;" class="chartjs-render-monitor"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4 d-flex">
                        <div class="card flex-fill w-100">
                            <div class="card-header">
                                <span class="badge badge-info float-right">Today</span>
                                <h5 class="card-title mb-0">Proc&eacute;dure à suivre</h5>
                            </div>
                            <div class="card-body">
                                <div class="media">
                                    <img src="asset/admin/img/avatar.jpg" width="36" height="36" class="rounded-circle mr-2" alt="Ashley Briggs">
                                    <div class="media-body">
                                        <small class="float-right text-navy">1</small>
                                        <strong>Faites votre <a href="register">inscription</a> en ligne</strong>
                                        <br>Cr&eacute;er votre compte et connectez-vous
                                    </div>
                                </div>
                                <hr>
                                <div class="media">
                                    <img src="asset/admin/img/avatar.jpg" width="36" height="36" class="rounded-circle mr-2" alt="Chris Wood">
                                    <div class="media-body">
                                        <small class="float-right text-navy">2</small>
                                        <strong>Passez vos commandes en ligne</strong>
                                        <br>Commandez vos GoPass et vos cartes NFC
                                    </div>
                                </div>

                                <hr>
                                <div class="media">
                                    <img src="asset/admin/img/avatar.jpg" width="36" height="36" class="rounded-circle mr-2" alt="Stacie Hall">
                                    <div class="media-body">
                                        <small class="float-right text-navy">3</small>
                                        <strong>Payez vos commandes en ligne</strong>
                                        <br>Faites vos paiements en toute s&eacute;curit&eacute; 
                                    </div>
                                </div>

                                <hr>
                                <a href="#" class="btn btn-primary btn-block">Load more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>			
    </body>
</html>
